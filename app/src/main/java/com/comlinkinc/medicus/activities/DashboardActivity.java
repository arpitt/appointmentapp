package com.comlinkinc.medicus.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.MainActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.dialog.DialogMembersMoreOptions;
import com.comlinkinc.medicus.fragments.ChatFragment;
import com.comlinkinc.medicus.fragments.ChatMembersFragment;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.fragments.PatientTestsFragment;
import com.comlinkinc.medicus.fragments.ProfileFragment;
import com.comlinkinc.medicus.fragments.ReportsFragment;
import com.comlinkinc.medicus.fragments.ResetPasswordFragment;
import com.comlinkinc.medicus.fragments.SubmittedQuestionnaireFragment;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.listener.ConnectListener;
import com.rocketchat.common.network.ReconnectionStrategy;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.callback.LoginListener;
import com.rocketchat.core.model.SubscriptionObject;
import com.rocketchat.core.model.TokenObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ConnectListener, LoginListener, View.OnClickListener, GetSubscriptionListener {

    FragmentManager fragmentManager;
    ProgressDialog progressDialog;
    boolean isSearchViewOpened = false;
    ConstraintLayout cLMessageCount;
    TextView tVMessageCount;
    String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG, Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    private NavigationView navigationView;
    private Context mContext;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    List<SubscriptionObject> subscribedMembers;
    private ImageView itemVideoCall, itemMoreOptions, itemSearchMessage;
    private TextView toolbar_title;
    private static final String TAG = "DashboardActivity";
    private int unreadMessagesCount = 0;
    private boolean showMessageCounter = true;
    private Handler handler = new Handler();
    private final static int LOADING_DURATION = 2000;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = DashboardActivity.this;
        getPermissions();
        getIds();
        updateProfileHeader();
        replaceFragments(PatientTestsFragment.class, null, getResources().getString(R.string.lbl_lab_tests), false);
        handler.postDelayed(new Runnable(){
            public void run(){
                if (Constants.client != null) {
                    unreadMessagesCount = 0;
                    Constants.client.getSubscriptions(DashboardActivity.this);
                }
                handler.postDelayed(this, LOADING_DURATION);
            }
        }, LOADING_DURATION);
    }

    private void getIds() {
        fragmentManager = getSupportFragmentManager();
        toolbar = findViewById(R.id.app_bar);
        toolbar_title = toolbar.findViewById(R.id.toolbar_title);
        cLMessageCount = toolbar.findViewById(R.id.messages);
        tVMessageCount = toolbar.findViewById(R.id.count);
        cLMessageCount.setOnClickListener(this);

        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_lab_tests);

        ImageView btnHamburger = toolbar.findViewById(R.id.img_left);
        btnHamburger.setOnClickListener(this);

        itemVideoCall = toolbar.findViewById(R.id.img_video_call);
        itemVideoCall.setVisibility(View.GONE);
        itemVideoCall.setOnClickListener(this);

        itemMoreOptions = toolbar.findViewById(R.id.img_more_options);
        itemMoreOptions.setVisibility(View.GONE);
        itemMoreOptions.setOnClickListener(this);

        itemSearchMessage = toolbar.findViewById(R.id.img_search_message);
        itemSearchMessage.setVisibility(View.GONE);
        itemSearchMessage.setOnClickListener(this);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(v -> drawer.openDrawer(GravityCompat.START));
        if (Constants.client == null) {
            Constants.client = new RocketChatAPI(Constants.ROCKET_CHAT_URL);
            Constants.client.setReconnectionStrategy(new ReconnectionStrategy(5, 1000));
            Constants.client.connect(this);
        }
    }

    public void updateProfileHeader() {
        View header = navigationView.getHeaderView(0);
        CircleImageView cVProfile = header.findViewById(R.id.profile_image);
        Glide.with(mContext).load(Constants.userDetails.getPpPath()).placeholder(R.drawable.user_default_thumb).into(cVProfile);
        TextView tVName = header.findViewById(R.id.txt_title_name_nav);
        tVName.setText(Constants.userDetails.getName());
        TextView tVEmail = header.findViewById(R.id.txt_title_email_nav);
        tVEmail.setText(Constants.userDetails.getEmail());
    }

    private void getPermissions() {
        Dexter.withContext(mContext).withPermissions(PERMISSIONS).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                multiplePermissionsReport.areAllPermissionsGranted(); //TODO - Do something
//                    Toast.makeText(mContext, "Thank you!", Toast.LENGTH_SHORT).show();
                if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                    CommonUtils.showAlertDialog(mContext, "Permission Denied", "You've denied one of the permissions required permanently. You can enable it if you wish to from your phone's settings.");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).onSameThread().check();
    }

    public void replaceFragments(Class fragmentClass, Bundle bundle, String title, boolean wantToShowAnimation) {
        try {
            Fragment fragment = (Fragment) fragmentClass.newInstance();
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentManager = getSupportFragmentManager();
            if (wantToShowAnimation) {
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.frame_container, fragment)
                        .commit();
            } else {
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment)
                        .commit();
            }
            toolbar_title.setText(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(getString(R.string.are_you_sure_quit));
        builder.setPositiveButton(getString(R.string.yes_please), (dialog, which) -> finish());
        builder.setNegativeButton(getString(R.string.no_stay_here), (dialog, which) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void setToolbarBarTitle(String title) {
        toolbar_title.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        drawer.closeDrawer(GravityCompat.START, true);
        hideKeyboard();
        showSearchMsgButton(false);
        showMoreOptionsButton(false);
        showMessageCounter(true);
        switch (item.getItemId()) {
            case R.id.nav_my_profile:
                replaceFragments(ProfileFragment.class, null, getResources().getString(R.string.lbl_profile), true);
                break;
            case R.id.nav_reports:
                replaceFragments(ReportsFragment.class, null, getResources().getString(R.string.lbl_reports), true);
                break;
            case R.id.nav_my_questionnaire:
                replaceFragments(SubmittedQuestionnaireFragment.class, null, getResources().getString(R.string.lbl_questionnaire), true);
                break;
            case R.id.nav_lab_tests:
                replaceFragments(PatientTestsFragment.class, null, getResources().getString(R.string.lbl_lab_tests), true);
                break;
            case R.id.nav_chat_with_doc:
                goToChat();
                break;
            case R.id.nav_reset_password:
                replaceFragments(ResetPasswordFragment.class, null, getResources().getString(R.string.lbl_reset_password), true);
                break;
            case R.id.nav_logout:
                logoutDialog();
                break;
            default:
                showMessageCounter(true);
                Toast.makeText(mContext, "Under construction!", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    private void goToChat() {
        showMessageCounter(false);
        if (subscribedMembers != null) {
            if (subscribedMembers.size() == 0) {
                //TODO - Do something
            } else if (subscribedMembers.size() == 1) {
                Bundle bundle = new Bundle();
                bundle.putString("roomId", subscribedMembers.get(0).getRoomId());
                replaceFragments(ChatFragment.class, bundle, subscribedMembers.get(0).getRoomName(), true);
            } else {
                replaceFragments(ChatMembersFragment.class, null, "Members", true);
            }
        } else {
            //TODO - Do something
        }
    }

    public void showSearchMsgButton(boolean status) {
        if (itemSearchMessage != null) {
            if (status) {
                itemSearchMessage.setVisibility(View.VISIBLE);
            } else {
                itemSearchMessage.setVisibility(View.GONE);
            }
        }
    }

    public void showMoreOptionsButton(boolean status) {
        if (itemMoreOptions != null) {
            if (status) {
                itemMoreOptions.setVisibility(View.VISIBLE);
            } else {
                itemMoreOptions.setVisibility(View.GONE);
            }
        }
    }

    public void logoutDialog() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);
        alertDialog.setTitle(getString(R.string.header_logout));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getString(R.string.want_to_logout));
        alertDialog.setIcon(R.drawable.ic_logout_black);
        alertDialog.setPositiveButton(getString(R.string.yes_please), (dialog, which) -> {
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_EMAIL, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_EMAIL, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_PASSWORD, "");
            Intent intent = new Intent(mContext, SplashScreenActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });

        alertDialog.setNegativeButton(getString(R.string.no_stay_here), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.messages:
                navigationView.setCheckedItem(R.id.nav_chat_with_doc);
                goToChat();
                break;
            case R.id.img_left:
                drawer.openDrawer(GravityCompat.START, true);
                break;
            case R.id.img_more_options:
                DialogMembersMoreOptions dialogMembersMoreOptions = new DialogMembersMoreOptions(DashboardActivity.this);
                Window window = dialogMembersMoreOptions.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP | Gravity.RIGHT;
                wlp.width = RelativeLayout.LayoutParams.MATCH_PARENT;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                dialogMembersMoreOptions.show();
                break;
            case R.id.img_search_message:
                ChatFragment frg = (ChatFragment) getSupportFragmentManager().findFragmentByTag("ChatFragment");
                if (isSearchViewOpened) {
                    isSearchViewOpened = false;
                    frg.hideShowSearchView(false);
                    itemSearchMessage.setImageDrawable(getResources().getDrawable(R.drawable.vector_search_white));
                } else {
                    isSearchViewOpened = true;
                    frg.hideShowSearchView(true);
                    itemSearchMessage.setImageDrawable(getResources().getDrawable(R.drawable.vector_close_white));
                }
                break;
        }
    }

    public void showMessageCounter(boolean status) {
        showMessageCounter = status;
        if (showMessageCounter && unreadMessagesCount > 0) {
            cLMessageCount.setVisibility(View.VISIBLE);
        } else {
            cLMessageCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onGetSubscriptions(List<SubscriptionObject> subscriptions, ErrorObject error) {
        subscribedMembers = new ArrayList<>();
        for (int i = 0; i < subscriptions.size(); i++) {
            if (("" + subscriptions.get(i).getRoomType()).equalsIgnoreCase("ONE_TO_ONE")) {
                unreadMessagesCount += subscriptions.get(i).getUnread();
                subscribedMembers.add(subscriptions.get(i));
            }
        }

        if (subscribedMembers != null) {
            Collections.reverse(subscribedMembers);
        }
        if (showMessageCounter && unreadMessagesCount > 0) {
            runOnUiThread(() -> {
                tVMessageCount.setText(String.valueOf(unreadMessagesCount));
                cLMessageCount.setVisibility(View.VISIBLE);
            });
        } else {
            runOnUiThread(() -> cLMessageCount.setVisibility(View.GONE));
        }
    }

    @Override
    public void onConnect(String sessionID) {
        if (Constants.client != null)
            Constants.client.loginUsingToken(Constants.chatAuthToken, this);
    }

    @Override
    public void onDisconnect(boolean closedByServer) {
        Constants.client.reconnect();
    }

    @Override
    public void onConnectError(Exception e) {
        Log.e(TAG, "onConnectError: " + e.getMessage());
    }

    @Override
    public void onLogin(TokenObject token, ErrorObject error) {

    }
}
