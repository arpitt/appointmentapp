package com.comlinkinc.medicus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.adapters.PatientDetailsAdapter;
import com.comlinkinc.medicus.dialog.DialogMembersMoreOptions;
import com.comlinkinc.medicus.utils.Connectivity;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.MainActivity;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.factory.ChatRoomFactory;
import com.rocketchat.core.model.SubscriptionObject;

import java.util.List;

public class PatientDetailsActivity extends AppCompatActivity implements View.OnClickListener, GetSubscriptionListener {

    public Toolbar toolbar;
    ViewPager viewPager;
    PatientDetailsAdapter adapter;
    TextView labTest, labReport, questions, chat;
    ImageView back_button, imgMore;
    Intent in;
    ImageView itemVideoCall;
    String pid, email, roomName = "", roomId = "";
    private static final String TAG = "PatientDetailsActivity";
    private List<SubscriptionObject> subscriptions = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        in = getIntent();
        pid = in.getStringExtra("pid");
        email = in.getStringExtra("email");
        findViewByIds();
    }

    private void findViewByIds() {
        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.viewpager);
        setSupportActionBar(toolbar);
        itemVideoCall = toolbar.findViewById(R.id.videocall);
        imgMore = toolbar.findViewById(R.id.img_more_options);
        labTest = findViewById(R.id.labtest);
        back_button = findViewById(R.id.back_button);
        labReport = findViewById(R.id.labreport);
        questions = findViewById(R.id.questions);
        chat = findViewById(R.id.chat);
        labReport.setOnClickListener(this);
        labTest.setOnClickListener(this);
        questions.setOnClickListener(this);
        chat.setOnClickListener(this);
        back_button.setOnClickListener(this);
        imgMore.setOnClickListener(this);

        if (Connectivity.isNetworkConnected(MyApplication.getInstance())) {
            if (Constants.client != null) {
                Constants.client.getSubscriptions(this);
            }
        }
        labTest.setBackgroundResource(R.drawable.left_tab_image);
        adapter = new PatientDetailsAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, pid, email, roomName, roomId);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    labTest.setBackgroundResource(R.drawable.left_tab_image);
                    labReport.setBackgroundResource(R.drawable.tab_image_holo);
                    questions.setBackgroundResource(R.drawable.tab_image_holo);
                    chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                    itemVideoCall.setVisibility(View.GONE);
                    imgMore.setVisibility(View.GONE);
                }
                if (position == 1) {
                    labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                    labReport.setBackgroundResource(R.drawable.tab_image);
                    questions.setBackgroundResource(R.drawable.tab_image_holo);
                    chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                    itemVideoCall.setVisibility(View.GONE);
                    imgMore.setVisibility(View.GONE);
                }
                if (position == 2) {
                    labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                    labReport.setBackgroundResource(R.drawable.tab_image_holo);
                    questions.setBackgroundResource(R.drawable.tab_image);
                    chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                    itemVideoCall.setVisibility(View.GONE);
                    imgMore.setVisibility(View.GONE);
                }
                if (position == 3) {
                    labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                    labReport.setBackgroundResource(R.drawable.tab_image_holo);
                    questions.setBackgroundResource(R.drawable.tab_image_holo);
                    chat.setBackgroundResource(R.drawable.right_tab_image);
                    itemVideoCall.setVisibility(View.VISIBLE);
                    imgMore.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.labtest:
                labTest.setBackgroundResource(R.drawable.left_tab_image);
                labReport.setBackgroundResource(R.drawable.tab_image_holo);
                questions.setBackgroundResource(R.drawable.tab_image_holo);
                chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                viewPager.setCurrentItem(0);
                itemVideoCall.setVisibility(View.GONE);
                imgMore.setVisibility(View.GONE);
                break;

            case R.id.labreport:
                labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                labReport.setBackgroundResource(R.drawable.tab_image);
                questions.setBackgroundResource(R.drawable.tab_image_holo);
                chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                viewPager.setCurrentItem(1);
                itemVideoCall.setVisibility(View.GONE);
                imgMore.setVisibility(View.GONE);
                break;

            case R.id.questions:
                labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                labReport.setBackgroundResource(R.drawable.tab_image_holo);
                questions.setBackgroundResource(R.drawable.tab_image);
                chat.setBackgroundResource(R.drawable.right_tab_image_holo);
                viewPager.setCurrentItem(2);
                itemVideoCall.setVisibility(View.GONE);
                imgMore.setVisibility(View.GONE);
                break;

            case R.id.chat:
                labTest.setBackgroundResource(R.drawable.left_tab_image_holo);
                labReport.setBackgroundResource(R.drawable.tab_image_holo);
                questions.setBackgroundResource(R.drawable.tab_image_holo);
                chat.setBackgroundResource(R.drawable.right_tab_image);
                viewPager.setCurrentItem(3);
                itemVideoCall.setVisibility(View.VISIBLE);
                imgMore.setVisibility(View.VISIBLE);
                break;

            case R.id.img_more_options:
                DialogMembersMoreOptions dialogMembersMoreOptions = new DialogMembersMoreOptions(this);
                Window window = dialogMembersMoreOptions.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP | Gravity.RIGHT;
                wlp.width = RelativeLayout.LayoutParams.MATCH_PARENT;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                dialogMembersMoreOptions.show();
                break;
        }
    }


    @Override
    public void onGetSubscriptions(List<SubscriptionObject> subscriptions, ErrorObject error) {
        runOnUiThread(() -> {
            if (subscriptions != null) {
                this.subscriptions = subscriptions;
                String e1 = email.replace("@", "_");
                for (int i = 0; i < subscriptions.size(); i++) {
                    if (("" + subscriptions.get(i).getRoomType()).equalsIgnoreCase("ONE_TO_ONE")) {
                        SubscriptionObject obj = subscriptions.get(i);
                        if (e1.equals(obj.getRoomName())) {
                            roomName = obj.getRoomName();
                            roomId = obj.getRoomId();
                            adapter = new PatientDetailsAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, pid, email, roomName, roomId);
                            viewPager.setAdapter(adapter);
                        }
                    }
                }
            } else {
                Constants.client.getSubscriptions(this);
                Log.e(TAG, "onGetSubscriptions: Could not get subscriptions.");
            }
        });
    }
}
