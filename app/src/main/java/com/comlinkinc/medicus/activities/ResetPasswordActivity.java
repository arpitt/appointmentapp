package com.comlinkinc.medicus.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.interfaces.PasswordInterface;
import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private EditText eTOldPassword, eTNewPassword, eTNewPasswordConfirm;
    private ProgressBar pBLoading;
    private AlertDialog alert = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getIds();
    }

    private void getIds() {
        mContext = getApplicationContext();
        eTOldPassword = findViewById(R.id.edttxt_old_pswd);
        eTNewPassword = findViewById(R.id.edttxt_new_pswd);
        eTNewPasswordConfirm = findViewById(R.id.edttxt_confirm_pswd);
        pBLoading = findViewById(R.id.progress_reset_pswd);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        setSupportActionBar(toolbar);

        Button btnResetPassword = findViewById(R.id.btn_reset_pswd);
        btnResetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_button) {
            finish();
        } else if (v.getId() == R.id.btn_reset_pswd) {
            boolean areAllFieldsCorrect = true;
            String oldPassword = eTOldPassword.getText().toString().trim();
            String newPassword = eTNewPassword.getText().toString().trim();
            String newPasswordConfirm = eTNewPasswordConfirm.getText().toString().trim();
            if (oldPassword.isEmpty()) {
                Toast.makeText(mContext, "Please enter your current password!", Toast.LENGTH_SHORT).show();
                areAllFieldsCorrect = false;
            }
            if (newPassword.isEmpty()) {
                Toast.makeText(mContext, "Please enter the new password!", Toast.LENGTH_SHORT).show();
                areAllFieldsCorrect = false;
            }
            if (newPasswordConfirm.isEmpty()) {
                Toast.makeText(mContext, "Please confirm your new password!", Toast.LENGTH_SHORT).show();
                areAllFieldsCorrect = false;
            }
            if (!newPassword.equals(newPasswordConfirm)) {
                Toast.makeText(mContext, "Both passwords don't match!", Toast.LENGTH_SHORT).show();
                areAllFieldsCorrect = false;
            }

            if (areAllFieldsCorrect) {
                changePassword(oldPassword, newPassword);
            }
        }
    }
    private void changePassword(String oldPassword, String newPassword) {
        pBLoading.setVisibility(View.VISIBLE);
        PasswordInterface passwordInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PasswordInterface.class);
        passwordInterface.changePassword(Constants.XAuthToken, Constants.XUserId, oldPassword, newPassword).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                pBLoading.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        dialogPasswordReset("Hooray!", response.body().getMessage());
                    } else {
                        dialogPasswordReset("Oops!", response.body().getMessage());
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                pBLoading.setVisibility(View.GONE);
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }


    private void dialogPasswordReset(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", (dialog, which) -> {
            if (alert != null)
                alert.dismiss();
        });
        alert = builder.create();
        alert.show();
    }
}
