package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;

public class QuestionnaireCompletedActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnarie_completed);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = QuestionnaireCompletedActivity.this;

        getIds();
    }

    private void getIds() {
        Button btnProceed = findViewById(R.id.btn_proceed);
        btnProceed.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_proceed) {
            Intent mainIntent = new Intent(mContext, DashboardActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(mainIntent);
        }
    }
}
