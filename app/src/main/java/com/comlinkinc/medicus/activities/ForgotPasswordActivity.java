package com.comlinkinc.medicus.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.PasswordInterface;
import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.Connectivity;
import com.comlinkinc.medicus.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private EditText edttxtEmail;
    private ProgressBar progressResetPswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pswd);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = ForgotPasswordActivity.this;

        getIds();

    }

    private void getIds() {
        edttxtEmail = findViewById(R.id.edttxt_reset_pswd);
        if (Constants.userDetails != null && Constants.userDetails.getEmail() != null)
            edttxtEmail.setText(Constants.userDetails.getEmail());

        progressResetPswd = findViewById(R.id.progress_reset_pswd);

        TextView btnRegister = findViewById(R.id.txt_register);
        btnRegister.setOnClickListener(this);

        TextView btnLogin = findViewById(R.id.txt_login);
        btnLogin.setOnClickListener(this);

        Button btnSendEmail = findViewById(R.id.btn_send_email);
        btnSendEmail.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_email:
                if (Connectivity.isNetworkConnected(MyApplication.getInstance())) {
                    String email = edttxtEmail.getText().toString();
                    if (!email.trim().equals("")) {
                        progressResetPswd.setVisibility(View.VISIBLE);
                        resetPassword(email);
                    } else {
                        edttxtEmail.setError(getString(R.string.enter_email_address));
                    }
                } else {
                    progressResetPswd.setVisibility(View.GONE);
                    Toast.makeText(mContext, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.txt_register:
                Intent registerIntent = new Intent(mContext, RegistrationActivity.class);
                registerIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(registerIntent);
                break;
            case R.id.txt_login:
                Intent loginIntent = new Intent(mContext, LoginActivity.class);
                loginIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(loginIntent);
                break;
        }
    }


    private void resetPassword(String email) {
        PasswordInterface passwordInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PasswordInterface.class);
        passwordInterface.forgotPassword(email, "0").enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        startActivity(new Intent(mContext, EmailVerificationActivity.class).putExtra("Message", response.body().getMessage()).putExtra("isForgotten", true));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        Toast.makeText(mContext, "Could not send the email again. Please contact support.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext);
            }
        });
    }
}
