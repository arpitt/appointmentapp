package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.DoctorInterface;
import com.comlinkinc.medicus.interfaces.LoginInterface;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.doctorDetails.DoctorProfileResponse;
import com.comlinkinc.medicus.responses.login.LoginResponse;
import com.comlinkinc.medicus.responses.patientDetails.PatientProfileResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.FCMTokenUpdateInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.fcm.FCMTokenResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.data.model.UserObject;
import com.rocketchat.common.listener.ConnectListener;
import com.rocketchat.common.network.ReconnectionStrategy;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.callback.LoginListener;
import com.rocketchat.core.model.TokenObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ConnectListener, LoginListener {
    private Context mContext;
    private EditText eTEmail, eTPassword;
    private ProgressBar progressLogin;
    private boolean isRememberMeIsChecked;
    private static final String TAG = "LoginActivity";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = LoginActivity.this;
        getIds();
    }

    private void getIds() {
        eTEmail = findViewById(R.id.edttxt_email);
        eTPassword = findViewById(R.id.eTPassword);

        TextView txtForgotPassword = findViewById(R.id.txt_forgot_pswd);
        txtForgotPassword.setOnClickListener(this);

        TextView txt_register = findViewById(R.id.txt_register);
        txt_register.setOnClickListener(this);

        CheckBox cbRememberMe = findViewById(R.id.cb_remember_me);
        cbRememberMe.setOnCheckedChangeListener((buttonView, isChecked) -> isRememberMeIsChecked = isChecked);

        Button btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        progressLogin = findViewById(R.id.progress_login);
    }

    private void logIntoRocketChat() {
        progressLogin.setVisibility(View.VISIBLE);
        Log.i(TAG, "logIntoRocketChat: Trying to log into RocketChat now");
        Constants.client = new RocketChatAPI(Constants.ROCKET_CHAT_URL);
        Constants.client.setReconnectionStrategy(new ReconnectionStrategy(5, 1000));
        Constants.client.connect(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                String username = eTEmail.getText().toString();
                String password = eTPassword.getText().toString();

                if (username.trim().isEmpty()) {
                    eTEmail.setError("Required field");
                } else if (password.trim().isEmpty()) {
                    eTPassword.setError("Required field");
                } else if (!isValidEmail(username)) {
                    eTEmail.setError("Invalid email");
                } else {
                    performLogin(username, password);
                }
                break;

            case R.id.txt_forgot_pswd:
                Intent forgotPasswordIntent = new Intent(mContext, ForgotPasswordActivity.class);
                forgotPasswordIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(forgotPasswordIntent);
                break;

            case R.id.txt_register:
                Intent registerIntent = new Intent(mContext, RegistrationActivity.class);
                registerIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(registerIntent);
                break;
        }
    }

    void performLogin(String userName, String password) {
        LoginInterface loginInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(LoginInterface.class);
        loginInterface.login(userName, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        if (isRememberMeIsChecked) {
                            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_EMAIL, userName);
                            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, password);
                            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_EMAIL, userName.replace("@", "_"));
                            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_PASSWORD, password);
                        }
                        Constants.userType = response.body().getData().getUserType();
                        Constants.XUserId = response.body().getData().getUserId();
                        Constants.XAuthToken = response.body().getData().getAuthToken();
                        Constants.chatAuthToken = response.body().getData().getChatAuthToken();
                        Constants.chatUserId = response.body().getData().getChatUserId();
                        Constants.userDetails = response.body().getData().getUserDetails();
                        logIntoRocketChat();
                    } else {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext);
            }
        });
    }

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onConnect(String sessionID) {
        if (Constants.chatAuthToken != null) {
            Constants.client.loginUsingToken(Constants.chatAuthToken, this);
        } else {
            progressLogin.setVisibility(View.GONE);
            runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show());
        }
    }

    @Override
    public void onDisconnect(boolean closedByServer) {
        logIntoRocketChat();
        Log.i(TAG, "onDisconnect: Closed by Server?: " + closedByServer);
    }

    @Override
    public void onConnectError(Exception e) {
        Log.e(TAG, "onConnectError: " + e.getMessage());
    }

    private void updateToken() {
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_AUTH_TOKEN, Constants.chatAuthToken);
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_USER_ID, Constants.chatUserId);
        runOnUiThread(() -> progressLogin.setVisibility(View.VISIBLE));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            String deviceToken = instanceIdResult.getToken();
            FCMTokenUpdateInterface fcmTokenUpdateInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(FCMTokenUpdateInterface.class);
            fcmTokenUpdateInterface.updateFCMToken(Constants.chatAuthToken, Constants.chatUserId, "gcm", deviceToken, getResources().getString(R.string.app_name)).enqueue(new Callback<FCMTokenResponse>() {
                @Override
                public void onResponse(@NonNull Call<FCMTokenResponse> call, @NonNull Response<FCMTokenResponse> response) {
                    runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
                    Log.i("Login", "FCM Token updated");
                }

                @Override
                public void onFailure(@NonNull Call<FCMTokenResponse> call, @NonNull Throwable t) {
                    runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
                    Log.e("Login", "Could not update FCM token");
                }
            });
        });
    }

    @Override
    public void onLogin(TokenObject token, ErrorObject error) {
        runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
        if (error == null) {
            Constants.client.setStatus(UserObject.Status.ONLINE, (success, error1) -> {
                if (success) {
                    System.out.println("Status set to online");
                }
            });
            updateToken();
            getUserDetails();
        }
        else {
            System.out.println("Got error " + error.getReason());
        }
    }

    private void getUserDetails() {
        if (Constants.userType.equals(Constants.PATIENT)) {
            PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
            patientInterface.getPatientDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<PatientProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<PatientProfileResponse> call, @NonNull Response<PatientProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getPid());
                        Constants.userDetails.setEmail(response.body().getDetails().getPEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getPPhone());
                        Constants.userDetails.setDateOfBirth(response.body().getDetails().getDob());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getPPpPath());
                        Constants.userDetails.setGender(response.body().getDetails().getGender());
                        Constants.userDetails.setQComplete(response.body().getDetails().getQComplete());
                        Constants.userDetails.setUserName(response.body().getDetails().getPEmail());
                        Constants.userDetails.setIsVerified(response.body().getDetails().getIsVerified());

                        if (Constants.userDetails.getQComplete().equalsIgnoreCase("1") && Constants.userDetails.getIsVerified().equalsIgnoreCase("1")) {
                            startActivity(new Intent(mContext, DashboardActivity.class));
                            finish();
                        } else if (Constants.userDetails.getIsVerified().equalsIgnoreCase("0")) {
                            startActivity(new Intent(mContext, EmailVerificationActivity.class).putExtra("Message", "Registered successfully! Please verify your email now.").putExtra("isForgotten", false));
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else if (Constants.userDetails.getQComplete().equalsIgnoreCase("0")) {
                            Intent mainIntent = new Intent(mContext, NewQuestionnairesActivity.class);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show());
                        }
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PatientProfileResponse> call, @NonNull Throwable t) {
                    if (CommonUtils.failureMessage(mContext, false))
                        runOnUiThread(() -> Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show());
                }
            });
        }  else {
            DoctorInterface doctorInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(DoctorInterface.class);
            doctorInterface.getDoctorDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<DoctorProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<DoctorProfileResponse> call, @NonNull Response<DoctorProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getDid());
                        Constants.userDetails.setEmail(response.body().getDetails().getDEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getDPhone());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getDPpPath());
                        Constants.userDetails.setUserName(response.body().getDetails().getDEmail());
                        Constants.userDetails.setdDegree(response.body().getDetails().getDDegree());
                        Constants.userDetails.setHospitalName(response.body().getDetails().getHospitalName());
                        Constants.userDetails.setHospitalAddress(response.body().getDetails().getHospitalAddress());
                        Constants.userDetails.setSpecialityName(response.body().getDetails().getSpName());
                        Constants.userDetails.setSpecialityId(response.body().getDetails().getSpId());
                        startActivity(new Intent(mContext, DoctorDashboardActivity.class));
                        finish();
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorProfileResponse> call, @NonNull Throwable t) {
                    CommonUtils.failureMessage(mContext, true);
                }
            });
        }
    }


}
