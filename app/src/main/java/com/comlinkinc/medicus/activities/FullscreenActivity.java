package com.comlinkinc.medicus.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        getIds();

    }

    private void getIds() {
        WebView ivReport = findViewById(R.id.webview_report);
        ivReport.getSettings().setJavaScriptEnabled(true);
        ivReport.getSettings().setLoadWithOverviewMode(true);
        ivReport.getSettings().setUseWideViewPort(true);
        ivReport.getSettings().setBuiltInZoomControls(true);

        ImageView ivBack = findViewById(R.id.iv_back);

        progressBar = findViewById(R.id.progress_report);

        progressBar.setVisibility(View.VISIBLE);
        if (getIntent() != null && getIntent().getExtras() != null) {
            String imgPath = getIntent().getExtras().getString("ReportImage");
            progressBar.setVisibility(View.VISIBLE);
            ivReport.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    progressBar.setVisibility(View.GONE);
                }
            });
            if (imgPath != null) {
                if (imgPath.contains("pdf")) {
                    ivReport.loadUrl("https://docs.google.com/viewer?url=" + imgPath);
                } else {
                    ivReport.loadUrl(imgPath);
                }
            }
        }
        ivBack.setOnClickListener(v -> finish());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
