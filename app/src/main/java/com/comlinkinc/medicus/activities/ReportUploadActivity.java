package com.comlinkinc.medicus.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.patientDetails.UploadReportResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.RetrofitConstants;
import com.comlinkinc.medicus.retrofit.models.interfaces.GetReportTypesListInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.reporttype.ReportTypeListResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.reporttype.RtTypeList;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.FilePath;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportUploadActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private Spinner spinnerReportType;
    public static final int PICK_IMAGE = 100;
    private static final int RESULT_SELECT_IMAGE = 1;
    public ImageView imageView;
    public String imagePath;
    private MultipartBody.Part part;

    private static final String TAG = ReportUploadActivity.class.getSimpleName();
    private GetReportTypesListInterface getReportTypesListInterface;
    private String reportTypeId;
    private ProgressBar progressBar;
    private String selectedFilePath;
    private TextView txtUploadedDocsname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_upload);

        mContext = ReportUploadActivity.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getReportTypesListInterface = RetrofitClient.getClient(RetrofitConstants.BASE_URL).create(GetReportTypesListInterface.class);

        getIds();
        fetchReportTypeList();
    }


    private void getIds() {
        spinnerReportType = findViewById(R.id.spinner_report_type);

        imageView = findViewById(R.id.imageView);

        progressBar = findViewById(R.id.progress_upload_docs);

        txtUploadedDocsname = findViewById(R.id.uploaded_docs_name);

        Button btnChooseDocs = findViewById(R.id.btn_choose_docs);
        Button btnUploadDocs = findViewById(R.id.btn_upload_docs);

        btnChooseDocs.setOnClickListener(this);
        btnUploadDocs.setOnClickListener(this);
    }


    private void fetchReportTypeList() {
        getReportTypesListInterface.getReportTypesList(Constants.XAuthToken, Constants.XUserId).enqueue(new Callback<ReportTypeListResponse>() {
            @Override
            public void onResponse(@NonNull Call<ReportTypeListResponse> call, @NonNull Response<ReportTypeListResponse> response) {
                if (response.body() != null) {
                    final ArrayList<RtTypeList> reportTypeList = new ArrayList<>();
                    RtTypeList rtList;
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        if (response.body().getRtTypeList() != null && response.body().getRtTypeList().size() > 0) {
                            for (int i = 0; i < response.body().getRtTypeList().size(); i++) {
                                rtList = new RtTypeList();
                                rtList.setRtId(response.body().getRtTypeList().get(i).getRtId());
                                rtList.setRtName(response.body().getRtTypeList().get(i).getRtName());
                                reportTypeList.add(rtList);
                            }
                        }

                        ArrayAdapter<RtTypeList> adapter =
                                new ArrayAdapter<RtTypeList>(mContext, R.layout.speciality_spinner_item, reportTypeList);
                        adapter.setDropDownViewResource(R.layout.speciality_spinner_item);

                        spinnerReportType.setAdapter(adapter);

                        spinnerReportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                                reportTypeId = reportTypeList.get(position).getRtId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReportTypeListResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_choose_docs:
                selectImage();
                break;
            case R.id.btn_upload_docs:
                progressBar.setVisibility(View.VISIBLE);

                Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.dialog_with_progress_text);
                dialog.setCancelable(false);
                TextView text = dialog.findViewById(R.id.text);
                text.setText("Uploading report(s)...");
                dialog.show();

                if (selectedFilePath != null) {
                    uploadReport();
                } else {
                    progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                    txtUploadedDocsname.setText(getString(R.string.please_select_report_to_upload));
                    txtUploadedDocsname.setTextColor(mContext.getResources().getColor(R.color.red));
                }

                break;
        }
    }

    //function to select a image
    private void selectImage() {
        //open album to select image
//        Intent gallaryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(gallaryIntent, RESULT_SELECT_IMAGE);

        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), RESULT_SELECT_IMAGE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_SELECT_IMAGE && resultCode == RESULT_OK && data != null) {

            Uri selectedFileUri = data.getData();
            selectedFilePath = FilePath.getPath(this, selectedFileUri);

            if (!selectedFilePath.equals("")) {
                String[] splittedSelectedFilePath = selectedFilePath.split("/");
                txtUploadedDocsname.setText(splittedSelectedFilePath[splittedSelectedFilePath.length - 1]);
                txtUploadedDocsname.setTextColor(mContext.getResources().getColor(R.color.splash_color));
                if (selectedFilePath.contains("pdf")) {
                    imageView.setImageResource(R.drawable.pdf);
                } else {
                    imageView.setImageURI(selectedFileUri);
                    imagePath = getRealPathFromURI(selectedFileUri);
                }
            }
            File f = new File(selectedFilePath);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("*/*"), f);
            part = MultipartBody.Part.createFormData("file", f.getName(), fileReqBody);
        }
    }

    public void uploadReport() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("pid", RequestBody.create(MediaType.parse("text/plain"), Constants.XUserId));
        map.put("rt_id", RequestBody.create(MediaType.parse("text/plain"), reportTypeId));

        PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
        patientInterface.uploadReport(Constants.XAuthToken, Constants.XUserId, map, part).enqueue(new Callback<UploadReportResponse>() {
            @Override
            public void onResponse(@NonNull Call<UploadReportResponse> call, @NonNull Response<UploadReportResponse> response) {
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    Toast.makeText(mContext, "Uploaded report successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UploadReportResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
