package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public class LogisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logister);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = LogisterActivity.this;
        getIds();
    }

    private void getIds() {
        Button btnLogin = findViewById(R.id.btn_login);
        Button btnRegister = findViewById(R.id.btn_register);
        TextView tVForgotPassword = findViewById(R.id.txt_forgot_pswd);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        tVForgotPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                Intent loginIntent = new Intent(mContext, LoginActivity.class);
                loginIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(loginIntent);
                break;

            case R.id.btn_register:
                Intent regIntent = new Intent(mContext, RegistrationActivity.class);
                regIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(regIntent);
                break;

            case R.id.txt_forgot_pswd:
                Intent forgotPswdIntent = new Intent(mContext, ForgotPasswordActivity.class);
                forgotPswdIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(forgotPswdIntent);
                break;
        }
    }
}
