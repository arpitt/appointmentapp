package com.comlinkinc.medicus.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.QuestionnaireListWithOptionsInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SaveSingleQuestionnaireInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.Datum;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.GetListOfQuestionnarieWithOptions;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.Option;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.Option_;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.SubQuestion;
import com.comlinkinc.medicus.retrofit.models.pojo.savesinglequesonserver.SingleQuestionnarieResponse;
import com.comlinkinc.medicus.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewQuestionnairesActivity extends FragmentActivity {
    private Context mContext;
    private MyPageAdapter pageAdapter;
    private List<Datum> listQuestionnaire;
    private ProgressBar progressQuestionnaire;
    private Button btnBack, btnNextQuestion;
    private ViewPager pager;
    private static List<Datum> questionnaireNewListObject;
    private SaveSingleQuestionnaireInterface saveSingleQuestionnaireInterface;
    private static boolean isMultilevelQuestion = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = NewQuestionnairesActivity.this;


        saveSingleQuestionnaireInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(SaveSingleQuestionnaireInterface.class);
        listQuestionnaire = new ArrayList<>();
        progressQuestionnaire = findViewById(R.id.progress_questionnarie);
        fetchQuestionnaireFromAPI();

        btnBack = findViewById(R.id.btn_back);
        btnNextQuestion = findViewById(R.id.btn_next_que);

        pager = findViewById(R.id.viewpager);

        if (pager.getCurrentItem() == 0) {
            btnBack.setVisibility(View.GONE);
            btnNextQuestion.setText(getString(R.string.str_btn_proceed));
        } else {
            btnBack.setVisibility(View.VISIBLE);
        }

        btnBack.setOnClickListener(v -> {
            if (pager.getCurrentItem() != 0) {
                btnNextQuestion.setText("Next");

                if (pager.getCurrentItem() == listQuestionnaire.size()) {
                    btnNextQuestion.setVisibility(View.VISIBLE);
                }

                if (pager.getCurrentItem() == 1) {
                    btnBack.setVisibility(View.GONE);
                    btnNextQuestion.setVisibility(View.VISIBLE);
                    btnNextQuestion.setText(getString(R.string.str_btn_proceed));
                }
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            } else {
                btnNextQuestion.setText(getString(R.string.str_btn_proceed));
            }
        });

        btnNextQuestion.setOnClickListener(v -> {
            btnBack.setVisibility(View.VISIBLE);

            boolean isLastQuestion;

            if (btnNextQuestion.getText().toString().trim().equalsIgnoreCase("Finish")) {
                questionnaireNewListObject.get(0);
                isLastQuestion = true;
                saveSingleQuestionnaireToServer(questionnaireNewListObject.get(pager.getCurrentItem()), isLastQuestion);
            } else {
                btnNextQuestion.setText("Next");
                isLastQuestion = false;

                if (questionnaireNewListObject != null && pager != null && questionnaireNewListObject.size() > 0) {
                    saveSingleQuestionnaireToServer(questionnaireNewListObject.get(pager.getCurrentItem()), isLastQuestion);
                }
            }

            if (pager.getCurrentItem() != listQuestionnaire.size()) {
                if (pager.getCurrentItem() == listQuestionnaire.size() - 2) {
                    btnNextQuestion.setVisibility(View.GONE);
                }

                if (pager.getCurrentItem() + 2 == listQuestionnaire.size()) {
                    btnBack.setVisibility(View.VISIBLE);
                    btnNextQuestion.setVisibility(View.VISIBLE);
                    btnNextQuestion.setText("Finish");
                }
                pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        });
    }

    private void fetchQuestionnaireFromAPI() {
        progressQuestionnaire.setVisibility(View.VISIBLE);
        QuestionnaireListWithOptionsInterface questionnaireListWithOptionsInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(QuestionnaireListWithOptionsInterface.class);
        questionnaireListWithOptionsInterface.getAllQuestionsWithOptions(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<GetListOfQuestionnarieWithOptions>() {
            @Override
            public void onResponse(@NonNull Call<GetListOfQuestionnarieWithOptions> call, @NonNull Response<GetListOfQuestionnarieWithOptions> response) {
                progressQuestionnaire.setVisibility(View.GONE);
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        Datum datum = response.body().getData().get(i);
                        datum.setQaId("0");
                        listQuestionnaire.add(datum);
                    }

                    questionnaireNewListObject = new ArrayList<>(listQuestionnaire);

                    List<Fragment> fragments = getFragments();
                    pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
                    pager = findViewById(R.id.viewpager);
                    pager.setOffscreenPageLimit(response.body().getData().size());
                    pager.setAdapter(pageAdapter);
                } else {
                    Toast.makeText(mContext, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetListOfQuestionnarieWithOptions> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }

    static class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();

        int totalSize = listQuestionnaire.size();

        for (int i = 0; i < totalSize; i++) {
            int currentPage = i + 1;
            fList.add(QuestionnaireFragments.newInstance(listQuestionnaire.get(i), currentPage, totalSize));
        }
        return fList;
    }

    public static class QuestionnaireFragments extends Fragment {
        static final String DATUM = "DATUM";
        static final String CURRENT_PAGE = "CURRENT_PAGE";
        static final String TOTAL_QUESTIONS = "TOTAL_QUESTIONS";
        private EditText layoutDescriptive, layoutDate;
        private Spinner spinnerOptions;
        Calendar myCalendar = Calendar.getInstance();
        Calendar myCalendarSubQue = Calendar.getInstance();
        private DatePickerDialog.OnDateSetListener selectedStartDate, selectedStartDateSubQue;
        private LinearLayout layoutMultiLevelSubQue;

        private TextView txtSubQuestion;
        private EditText layoutSubQueDescriptive, layoutSubQueDate;
        private LinearLayout layoutSubQueDropdown;
        private Spinner spinnerSubQueOptions;
        private LinearLayout layoutSubQueCheckbox;

        public static QuestionnaireFragments newInstance(Datum message, int curPage, int totalSize) {
            QuestionnaireFragments f = new QuestionnaireFragments();
            Bundle bdl = new Bundle(1);
            bdl.putParcelable(DATUM, message);
            bdl.putInt(CURRENT_PAGE, curPage);
            bdl.putInt(TOTAL_QUESTIONS, totalSize);
            f.setArguments(bdl);
            return f;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Datum datum = getArguments().getParcelable(DATUM);
            final int curPage = getArguments().getInt(CURRENT_PAGE);

            final View v = inflater.inflate(R.layout.ques_runtimefragment_layout, container, false);

            TextView txtQuestion = v.findViewById(R.id.txt_question);
            layoutDescriptive = v.findViewById(R.id.layout_descriptive);
            layoutDate = v.findViewById(R.id.layout_date);
            LinearLayout layoutDropdown = v.findViewById(R.id.layout_dropdown);
            spinnerOptions = v.findViewById(R.id.spinner_option);
            LinearLayout layoutCheckbox = v.findViewById(R.id.layout_checkbox);
            LinearLayout layoutMultiLevel = v.findViewById(R.id.layout_multi_level);
            layoutMultiLevelSubQue = v.findViewById(R.id.layout_multi_level_sub_que);
            RadioGroup rg1 = v.findViewById(R.id.radio_group);

            //sub question layout
            txtSubQuestion = v.findViewById(R.id.txt_sub_question);
            layoutSubQueDescriptive = v.findViewById(R.id.layout_sub_descriptive);
            layoutSubQueDate = v.findViewById(R.id.layout_sub_date);
            layoutSubQueDropdown = v.findViewById(R.id.layout_sub_dropdown);
            spinnerSubQueOptions = v.findViewById(R.id.spinner_sub_option);
            layoutSubQueCheckbox = v.findViewById(R.id.layout_sub_checkbox);

            selectedStartDate = (view, year, monthOfYear, dayOfMonth) -> {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateDateLabel(layoutDate, myCalendar);
            };

            layoutDate.setOnClickListener(v1 -> {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, selectedStartDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());//Disable future date
                datePickerDialog.show();
            });

            txtQuestion.setText("Q." + curPage + "  " + datum.getQuestion());
            questionnaireNewListObject.get(curPage - 1).setQaId("0");
            if (datum.getAns() != null) {
                questionnaireNewListObject.get(curPage - 1).setAns(datum.getAns().toString());
            } else {
                questionnaireNewListObject.get(curPage - 1).setAns("");
            }

            layoutDescriptive.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    questionnaireNewListObject.get(curPage - 1).setAns(layoutDescriptive.getText().toString());
                }
            });

            layoutDate.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    DateFormat inputFormat = new SimpleDateFormat("MM-dd-yyyy");
                    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String startDateStr = layoutDate.getText().toString();
                    Date date = null;
                    String startDateStrNewFormat = "";
                    try {
                        date = inputFormat.parse(startDateStr);
                        startDateStrNewFormat = outputFormat.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    questionnaireNewListObject.get(curPage - 1).setAns(startDateStrNewFormat);
                }
            });


            if (datum.getQtId().trim().equals("1")) { //Question type is Descriptive

                layoutDescriptive.setVisibility(View.VISIBLE);
                layoutDate.setVisibility(View.GONE);
                layoutDropdown.setVisibility(View.GONE);
                layoutCheckbox.setVisibility(View.GONE);
                layoutMultiLevel.setVisibility(View.GONE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);

                if (datum.getAns() != null) {
                    layoutDescriptive.setText(datum.getAns().toString());
                }

            } else if (datum.getQtId().trim().equals("2")) { //Question type is Datepicker

                layoutDescriptive.setVisibility(View.GONE);
                layoutDate.setVisibility(View.VISIBLE);
                layoutDropdown.setVisibility(View.GONE);
                layoutCheckbox.setVisibility(View.GONE);
                layoutMultiLevel.setVisibility(View.GONE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);

                if (datum.getAns() != null) {
                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("MM-dd-yyyy");
                    String startDateStr = datum.getAns().toString();
                    Date date = null;
                    String startDateStrNewFormat = "";
                    try {
                        date = inputFormat.parse(startDateStr);
                        startDateStrNewFormat = outputFormat.format(date);
                        myCalendar.setTime(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    layoutDate.setText(startDateStrNewFormat);
                }


            } else if (datum.getQtId().trim().equals("3")) { //Question type is Dropdown

                layoutDescriptive.setVisibility(View.GONE);
                layoutDate.setVisibility(View.GONE);
                layoutDropdown.setVisibility(View.VISIBLE);
                layoutCheckbox.setVisibility(View.GONE);
                layoutMultiLevel.setVisibility(View.GONE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);

                List<Option> optionsList = new ArrayList<>(datum.getOptions());

                ArrayAdapter<Option> adapter =
                        new ArrayAdapter<Option>(getActivity(), R.layout.date_spinner_item, optionsList);
                adapter.setDropDownViewResource(R.layout.date_spinner_item);
                spinnerOptions.setAdapter(adapter);
                spinnerOptions.setSelection(0);

                spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        questionnaireNewListObject.get(curPage - 1).setAns(((Option) spinnerOptions.getSelectedItem()).getQoId());

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                if (datum.getAns() != null) {
                    for (int position = 0; position < adapter.getCount(); position++) {
                        if (adapter.getItem(position).getQoId().equals(datum.getAns())) {
                            spinnerOptions.setSelection(position);
                        }
                    }
                }

            } else if (datum.getQtId().trim().equals("4")) { //Question type is Checkbox (Multiple selection)

                layoutDescriptive.setVisibility(View.GONE);
                layoutDate.setVisibility(View.GONE);
                layoutDropdown.setVisibility(View.GONE);
                layoutCheckbox.setVisibility(View.VISIBLE);
                layoutMultiLevel.setVisibility(View.GONE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);

                final ArrayList<String> srtArr = new ArrayList<>();

                for (int i = 0; i < datum.getOptions().size(); i++) {
                    final CheckBox cbOption = new CheckBox(this.getContext());
                    cbOption.setPadding(30, 30, 30, 30);
                    cbOption.setText(datum.getOptions().get(i).getQOption());
                    cbOption.setTag(datum.getOptions().get(i).getQoId());
                    cbOption.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    cbOption.setTextSize(16);

                    if (Build.VERSION.SDK_INT >= 21) {
                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{
                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{

                                        getResources().getColor(R.color.txt_color), //disabled
                                        getResources().getColor(R.color.txt_color) //enabled
                                }
                        );
                        cbOption.setButtonTintList(colorStateList);//set the color tint list
                        cbOption.invalidate(); //could not be necessary
                    }

                    layoutCheckbox.addView(cbOption);

                    cbOption.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        if (isChecked) {
                            srtArr.add(cbOption.getTag().toString());
                            StringBuilder finalCbOptions = new StringBuilder();
                            for (int j = 0; j < srtArr.size(); j++) {
                                finalCbOptions.append(srtArr.get(j)).append(",");
                            }

                            if (!finalCbOptions.toString().trim().equals("")) {
                                finalCbOptions = new StringBuilder(finalCbOptions.substring(0, finalCbOptions.length() - 1));
                            }
                            questionnaireNewListObject.get(curPage - 1).setAns(finalCbOptions.toString());
                        } else {
                            srtArr.remove(cbOption.getTag().toString());
                            StringBuilder finalCbOptions = new StringBuilder();
                            for (int j = 0; j < srtArr.size(); j++) {
                                finalCbOptions.append(srtArr.get(j)).append(",");
                            }

                            if (!finalCbOptions.toString().trim().equals("")) {
                                finalCbOptions = new StringBuilder(finalCbOptions.substring(0, finalCbOptions.length() - 1));
                            }

                            questionnaireNewListObject.get(curPage - 1).setAns(finalCbOptions.toString());
                        }
                    });

                    if (datum.getAns() != null) {
                        String[] allOps = datum.getAns().toString().split(",");
                        for (String allOp : allOps) {
                            if (allOp.equals(datum.getOptions().get(i).getQoId())) {
                                cbOption.setChecked(true);
                            }
                        }
                    }
                }

            } else if (datum.getQtId().trim().equals("5")) { //Question type is Radio Button (Single selection)

                layoutDescriptive.setVisibility(View.GONE);
                layoutDate.setVisibility(View.GONE);
                layoutDropdown.setVisibility(View.GONE);
                layoutCheckbox.setVisibility(View.VISIBLE);
                layoutMultiLevel.setVisibility(View.GONE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);

                final RadioButton[] rb = new RadioButton[datum.getOptions().size()];
                RadioGroup rg = new RadioGroup(getActivity());
                rg.setOrientation(RadioGroup.VERTICAL);
                for (int i = 0; i < datum.getOptions().size(); i++) {
                    rb[i] = new RadioButton(getActivity());
                    rb[i].setText(datum.getOptions().get(i).getQOption());
                    rb[i].setTag(datum.getOptions().get(i).getQoId());
                    rb[i].setPadding(30, 30, 30, 30);
                    rb[i].setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    rb[i].setTextSize(16);

                    if (Build.VERSION.SDK_INT >= 21) {
                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{
                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{
                                        getResources().getColor(R.color.txt_color), //disabled
                                        getResources().getColor(R.color.txt_color) //enabled
                                }
                        );
                        rb[i].setButtonTintList(colorStateList);//set the color tint list
                        rb[i].invalidate(); //could not be necessary
                    }

                    rg.addView(rb[i]);

                    if (datum.getAns() != null) {
                        String[] allOps = datum.getAns().toString().split(",");
                        for (String allOp : allOps) {
                            if (allOp.equals(datum.getOptions().get(i).getQoId())) {
                                rb[i].setChecked(true);
                            }
                        }
                    }
                }
                layoutCheckbox.addView(rg);

                rg.setOnCheckedChangeListener((group, checkedId) -> {
                    RadioButton radioButton = v.findViewById(checkedId);
                    questionnaireNewListObject.get(curPage - 1).setAns(radioButton.getTag().toString());
                });
            } else if (datum.getQtId().trim().equals("6")) { //Question type is multi level question.

                layoutDescriptive.setVisibility(View.GONE);
                layoutDate.setVisibility(View.GONE);
                layoutDropdown.setVisibility(View.GONE);
                layoutCheckbox.setVisibility(View.GONE);
                layoutMultiLevel.setVisibility(View.VISIBLE);
                layoutMultiLevelSubQue.setVisibility(View.GONE);


                final RadioButton[] rb = new RadioButton[datum.getOptions().size()];
                for (int i = 0; i < datum.getOptions().size(); i++) {
                    rb[i] = new RadioButton(getActivity());
                    rb[i].setText(datum.getOptions().get(i).getQOption());
                    rb[i].setTag(datum.getOptions().get(i).getQoId() + "_" + datum.getOptions().get(i).getSubQId());
                    rb[i].setPadding(30, 30, 30, 30);
                    rb[i].setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    rb[i].setTextSize(16);

                    if (Build.VERSION.SDK_INT >= 21) {
                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{
                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{

                                        getResources().getColor(R.color.txt_color), //disabled
                                        getResources().getColor(R.color.txt_color) //enabled
                                }
                        );
                        rb[i].setButtonTintList(colorStateList);//set the color tint list
                        rb[i].invalidate(); //could not be necessary
                    }

                    rg1.addView(rb[i]);

                    if (datum.getAns() != null && !datum.getAns().toString().trim().equals("")) {
                        String[] allOps = datum.getAns().toString().split(",");
                        for (int j = 0; j < allOps.length; j++) {
                            if (allOps[j].equals(datum.getOptions().get(i).getQoId())) {
                                rb[i].setChecked(true);
                                String[] strTag = rb[i].getTag().toString().trim().split("_");
                                String selectedOptionId = strTag[0].trim();
                                String selectedOptionSubQueId = strTag[1].trim();
                                questionnaireNewListObject.get(curPage - 1).setAns(selectedOptionId);
                                for (int k = 0; k < datum.getSubQuestion().size(); k++) {
                                    if (datum.getSubQuestion().get(k).getQId().equals(selectedOptionSubQueId)) {
                                        layoutMultiLevelSubQue.setVisibility(View.VISIBLE);
                                        isMultilevelQuestion = true;
                                        handleSubQuestionLayout(curPage, datum.getSubQuestion().get(j), k);
                                    }
//                                    else {
//                                        isMultilevelQuestion = false;
//                                        layoutMultiLevelSubQue.setVisibility(View.GONE);
//                                    }
                                }
                            }
                        }
                    }
                }
                rg1.setOnCheckedChangeListener((group, checkedId) -> {
                    int i = group.getCheckedRadioButtonId();
                    RadioButton radioButton = v.findViewById(i);
                    radioButton.setChecked(true);
                    String[] strTag = radioButton.getTag().toString().trim().split("_");
                    String selectedOptionId = strTag[0].trim();
                    String selectedOptionSubQueId = strTag[1].trim();
                    questionnaireNewListObject.get(curPage - 1).setAns(selectedOptionId);
                    if (!selectedOptionSubQueId.equalsIgnoreCase("0")) {
                        for (int j = 0; j < datum.getSubQuestion().size(); j++) {
                            if (datum.getSubQuestion().get(j).getQId() != null) {
                                if (datum.getSubQuestion().get(j).getQId().equals(selectedOptionSubQueId)) {
                                    layoutMultiLevelSubQue.setVisibility(View.VISIBLE);
                                    isMultilevelQuestion = true;
                                    handleSubQuestionLayout(curPage, datum.getSubQuestion().get(j), j);
                                    return;
                                } else {
                                    isMultilevelQuestion = false;
                                    layoutMultiLevelSubQue.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else {
                        isMultilevelQuestion = false;
                        layoutMultiLevelSubQue.setVisibility(View.GONE);
                    }
                });
            }
            return v;
        }

        private void handleSubQuestionLayout(final int curPage, SubQuestion subQuestion, int subQueSelectedOpId) {
            selectedStartDateSubQue = (view, year, monthOfYear, dayOfMonth) -> {
                myCalendarSubQue.set(Calendar.YEAR, year);
                myCalendarSubQue.set(Calendar.MONTH, monthOfYear);
                myCalendarSubQue.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                updateDateLabel(layoutSubQueDate, myCalendarSubQue);
            };

            layoutSubQueDate.setOnClickListener(v -> {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, selectedStartDateSubQue, myCalendarSubQue
                        .get(Calendar.YEAR), myCalendarSubQue.get(Calendar.MONTH),
                        myCalendarSubQue.get(Calendar.DAY_OF_MONTH));
//                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000); //Disable Past date
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());//Disable future date
                datePickerDialog.show();
            });


            layoutSubQueDescriptive.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                    for (int i = 0; i < questionnaireNewListObject.get(curPage - 1).getSubQuestion().size(); i++) {
                        questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(i).setAns(layoutSubQueDescriptive.getText().toString());
                    }
                }
            });

            layoutSubQueDate.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    DateFormat inputFormat = new SimpleDateFormat("MM-dd-yyyy");
                    DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String startDateStr = layoutSubQueDate.getText().toString();
                    Date date = null;
                    String startDateStrNewFormat = "";
                    try {
                        date = inputFormat.parse(startDateStr);
                        startDateStrNewFormat = outputFormat.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < questionnaireNewListObject.get(curPage - 1).getSubQuestion().size(); i++) {
                        questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(i).setAns(startDateStrNewFormat);
                    }
                }
            });


            txtSubQuestion.setText("Q." + "  " + subQuestion.getQuestion());

            if (subQuestion.getQtId().trim().equals("1")) { //Question type is Descriptive

                layoutSubQueDescriptive.setVisibility(View.VISIBLE);
                layoutSubQueDate.setVisibility(View.GONE);
                layoutSubQueDropdown.setVisibility(View.GONE);
                layoutSubQueCheckbox.setVisibility(View.GONE);

                if (subQuestion.getAns() != null) {
                    layoutSubQueDescriptive.setText(subQuestion.getAns().toString());
                }

            } else if (subQuestion.getQtId().trim().equals("2")) { //Question type is Datepicker

                layoutSubQueDescriptive.setVisibility(View.GONE);
                layoutSubQueDate.setVisibility(View.VISIBLE);
                layoutSubQueDropdown.setVisibility(View.GONE);
                layoutSubQueCheckbox.setVisibility(View.GONE);

                if (subQuestion.getAns() != null) {
                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("MM-dd-yyyy");
                    String startDateStr = subQuestion.getAns().toString();
                    Date date = null;
                    String startDateStrNewFormat = "";
                    try {
                        date = inputFormat.parse(startDateStr);
                        startDateStrNewFormat = outputFormat.format(date);
                        myCalendarSubQue.setTime(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    layoutSubQueDate.setText(startDateStrNewFormat);
                }

            } else if (subQuestion.getQtId().trim().equals("3")) { //Question type is Dropdown

                layoutSubQueDescriptive.setVisibility(View.GONE);
                layoutSubQueDate.setVisibility(View.GONE);
                layoutSubQueDropdown.setVisibility(View.VISIBLE);
                layoutSubQueCheckbox.setVisibility(View.GONE);

                List<Option_> optionsList = new ArrayList<>(subQuestion.getOptions());

                ArrayAdapter<Option_> adapter =
                        new ArrayAdapter<Option_>(getActivity(), R.layout.date_spinner_item, optionsList);
                adapter.setDropDownViewResource(R.layout.date_spinner_item);
                spinnerSubQueOptions.setAdapter(adapter);
                spinnerSubQueOptions.setSelection(0);

                spinnerSubQueOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        for (int i = 0; i < questionnaireNewListObject.get(curPage - 1).getSubQuestion().size(); i++) {
                            questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(i).setAns(((Option_) spinnerSubQueOptions.getSelectedItem()).getQoId());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                if (subQuestion.getAns() != null) {
                    for (int position = 0; position < adapter.getCount(); position++) {
                        if (adapter.getItem(position).getQoId().equals(subQuestion.getAns())) {
                            spinnerSubQueOptions.setSelection(position);
                        }
                    }
                }

            } else if (subQuestion.getQtId().trim().equals("4")) { //Question type is Checkbox (Multiple selection)
                layoutSubQueDescriptive.setVisibility(View.GONE);
                layoutSubQueDate.setVisibility(View.GONE);
                layoutSubQueDropdown.setVisibility(View.GONE);
                layoutSubQueCheckbox.setVisibility(View.VISIBLE);
                layoutSubQueCheckbox.removeAllViews();

                final ArrayList<String> srtArr = new ArrayList<>();
                final String cbSelectedValues = "";

                for (int i = 0; i < subQuestion.getOptions().size(); i++) {
                    final CheckBox cbOption = new CheckBox(this.getContext());
                    cbOption.setPadding(30, 30, 30, 30);
                    cbOption.setText(subQuestion.getOptions().get(i).getQOption());
                    cbOption.setTag(subQuestion.getOptions().get(i).getQoId());
                    cbOption.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    cbOption.setTextSize(16);

                    if (Build.VERSION.SDK_INT >= 21) {
                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{
                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{

                                        getResources().getColor(R.color.txt_color), //disabled
                                        getResources().getColor(R.color.txt_color) //enabled
                                }
                        );
                        cbOption.setButtonTintList(colorStateList);//set the color tint list
                        cbOption.invalidate(); //could not be necessary
                    }

                    layoutSubQueCheckbox.addView(cbOption);

                    cbOption.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        if (isChecked) {
                            srtArr.add(cbOption.getTag().toString());
                            StringBuilder finalCbOptions = new StringBuilder();
                            for (int j = 0; j < srtArr.size(); j++) {
                                finalCbOptions.append(srtArr.get(j)).append(",");
                            }

                            if (!finalCbOptions.toString().trim().equals("")) {
                                finalCbOptions = new StringBuilder(finalCbOptions.substring(0, finalCbOptions.length() - 1));
                            }
                            questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(subQueSelectedOpId).setAns(finalCbOptions.toString());

                        } else {
                            srtArr.remove(cbOption.getTag().toString());
                            StringBuilder finalCbOptions = new StringBuilder();
                            for (int j = 0; j < srtArr.size(); j++) {
                                finalCbOptions.append(srtArr.get(j)).append(",");
                            }

                            if (!finalCbOptions.toString().trim().equals("")) {
                                finalCbOptions = new StringBuilder(finalCbOptions.substring(0, finalCbOptions.length() - 1));
                            }

                            questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(subQueSelectedOpId).setAns(finalCbOptions.toString());
                        }
                    });

                    if (subQuestion.getAns() != null) {
                        String[] allOps = subQuestion.getAns().toString().split(",");
                        for (String allOp : allOps) {
                            if (allOp.equals(subQuestion.getOptions().get(i).getQoId())) {
                                cbOption.setChecked(true);
                            }
                        }
                    }
                }

            } else if (subQuestion.getQtId().trim().equals("5")) { //Question type is Radio Button (Single selection)

                layoutSubQueDescriptive.setVisibility(View.GONE);
                layoutSubQueDate.setVisibility(View.GONE);
                layoutSubQueDropdown.setVisibility(View.GONE);
                layoutSubQueCheckbox.setVisibility(View.VISIBLE);
                layoutSubQueCheckbox.removeAllViews();

                final RadioButton[] rb = new RadioButton[subQuestion.getOptions().size()];
                RadioGroup rg = new RadioGroup(getActivity());
                rg.setOrientation(RadioGroup.VERTICAL);
                for (int i = 0; i < subQuestion.getOptions().size(); i++) {
                    rb[i] = new RadioButton(getActivity());
                    rb[i].setText(subQuestion.getOptions().get(i).getQOption());
                    rb[i].setTag(subQuestion.getOptions().get(i).getQoId());
                    rb[i].setPadding(30, 30, 30, 30);
                    rb[i].setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                    rb[i].setTextSize(16);

                    if (Build.VERSION.SDK_INT >= 21) {
                        ColorStateList colorStateList = new ColorStateList(
                                new int[][]{
                                        new int[]{-android.R.attr.state_enabled}, //disabled
                                        new int[]{android.R.attr.state_enabled} //enabled
                                },
                                new int[]{

                                        getResources().getColor(R.color.txt_color), //disabled
                                        getResources().getColor(R.color.txt_color) //enabled
                                }
                        );
                        rb[i].setButtonTintList(colorStateList);//set the color tint list
                        rb[i].invalidate(); //could not be necessary
                    }

                    rg.addView(rb[i]);

                    if (subQuestion.getAns() != null) {
                        String[] allOps = subQuestion.getAns().toString().split(",");
                        for (String allOp : allOps) {
                            if (allOp.equals(subQuestion.getOptions().get(i).getQoId())) {
                                rb[i].setChecked(true);
                            }
                        }
                    }
                }
                layoutSubQueCheckbox.addView(rg);

                rg.setOnCheckedChangeListener((group, checkedId) -> {
                    RadioButton radioButton = getActivity().findViewById(checkedId);
                    for (int i = 0; i < questionnaireNewListObject.get(curPage - 1).getSubQuestion().size(); i++) {
                        questionnaireNewListObject.get(curPage - 1).getSubQuestion().get(i).setAns(radioButton.getTag().toString());
                    }
                });
            }
        }

        private void updateDateLabel(EditText edtxtDate, Calendar calendar) {
            String myFormat = "MM-dd-yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            edtxtDate.setText(sdf.format(calendar.getTime()));
            edtxtDate.setError(null);
        }
    }

    public void saveSingleQuestionnaireToServer(Datum queDatum, final boolean isLastQuestion) {
        Integer isQuestionComplete = 0;
        if (isLastQuestion) {
            isQuestionComplete = 1;
        }

        if (!queDatum.getQtId().equals("6")) {
            String ans;
            if (queDatum.getAns() == null) {
                ans = "";
            } else {
                ans = queDatum.getAns().toString();
            }
            saveSingleQuestionnaireInterface.saveSingleQuesOnServer(Constants.XAuthToken, Constants.XUserId,
                    Constants.userDetails.getPatientId(),
//                    "2",
                    queDatum.getQId(),
                    queDatum.getQtId(),
                    ans,
                    "0",
                    isQuestionComplete).enqueue(new Callback<SingleQuestionnarieResponse>() {
                @Override
                public void onResponse(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Response<SingleQuestionnarieResponse> response) {
                    if (isLastQuestion) {
                        if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                            Intent intent = new Intent(mContext, QuestionnaireCompletedActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Throwable t) {
                    Log.d("", "");
                }
            });
        } else {
            if (isMultilevelQuestion) {
                String subAns;

                for (int k = 0; k < queDatum.getSubQuestion().size(); k++) {

                    if (queDatum.getSubQuestion().get(k).getAns() == null) {
                        subAns = "";
                    } else {
                        subAns = queDatum.getSubQuestion().get(k).getAns().toString();
                    }

                    saveSingleQuestionnaireInterface.saveMultiLevelQuesOnServer(Constants.XAuthToken, Constants.XUserId,
                            Constants.userDetails.getPatientId(),
//                            "2",
                            queDatum.getQId(),
                            queDatum.getQtId(),
                            queDatum.getAns().toString(),
//                            queDatum.getOptions().get(k).getQoId(),
                            queDatum.getQaId(),
                            queDatum.getSubQuestion().get(k).getQId(),
                            queDatum.getSubQuestion().get(k).getQtId(),
                            subAns,
                            "0",
                            isQuestionComplete).enqueue(new Callback<SingleQuestionnarieResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Response<SingleQuestionnarieResponse> response) {
                            if (isLastQuestion) {
                                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                                    Intent intent = new Intent(mContext, QuestionnaireCompletedActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Throwable t) {
                            Log.d("", "");
                        }
                    });
                }
            } else {
                String ans;
                if (queDatum.getAns() == null) {
                    ans = "";
                } else {
                    ans = queDatum.getAns().toString();
                }
                saveSingleQuestionnaireInterface.saveSingleQuesOnServer(Constants.XAuthToken, Constants.XUserId,
                        Constants.userDetails.getPatientId(),
//                        "2",
                        queDatum.getQId(),
                        queDatum.getQtId(),
                        ans,
                        "0",
                        isQuestionComplete).enqueue(new Callback<SingleQuestionnarieResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Response<SingleQuestionnarieResponse> response) {
                        if (isLastQuestion) {
                            if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                                Intent intent = new Intent(mContext, QuestionnaireCompletedActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<SingleQuestionnarieResponse> call, @NonNull Throwable t) {
                        Log.d("", "");
                    }
                });
            }
        }


    }
}
