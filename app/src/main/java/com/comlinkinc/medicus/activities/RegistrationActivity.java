package com.comlinkinc.medicus.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.interfaces.DoctorInterface;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.interfaces.PatientRegistrationInterface;
import com.comlinkinc.medicus.responses.doctorDetails.DoctorProfileResponse;
import com.comlinkinc.medicus.responses.login.UserDetails;
import com.comlinkinc.medicus.responses.patientDetails.PatientProfileResponse;
import com.comlinkinc.medicus.responses.patientRegistration.PatientRegistrationResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.FCMTokenUpdateInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.fcm.FCMTokenResponse;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.data.model.UserObject;
import com.rocketchat.common.listener.ConnectListener;
import com.rocketchat.common.network.ReconnectionStrategy;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.callback.LoginListener;
import com.rocketchat.core.model.TokenObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, ConnectListener, LoginListener {

    private final String TAG = "RegistrationActivity";
    Calendar myCalendar = Calendar.getInstance();
    String fcmToken = "";
    private Context mContext;
    private EditText edttxtFullName, edttxtMobNo, edttxtPassword, edttxtEmail, edttxtDOB;
    private RadioGroup rgGender;
    private DatePickerDialog.OnDateSetListener selectedStartDate;
    private ProgressBar progressLogin;

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = RegistrationActivity.this;
        getIds();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            fcmToken = instanceIdResult.getToken();
        });
    }

    private void getIds() {
        edttxtFullName = findViewById(R.id.edttxt_full_name);
        edttxtMobNo = findViewById(R.id.edttxt_mob_no);
        edttxtEmail = findViewById(R.id.edttxt_email);
        edttxtPassword = findViewById(R.id.eTPassword);
        edttxtDOB = findViewById(R.id.edttxt_dob);
        edttxtDOB.setOnClickListener(this);

        progressLogin = findViewById(R.id.progress_register);

        Button btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        TextView txtLogin = findViewById(R.id.txt_login);
        txtLogin.setOnClickListener(this);

        rgGender = findViewById(R.id.rg_gender);

        selectedStartDate = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDOBDateLabel();
        };
    }

    private void logIntoRocketChat() {
        progressLogin.setVisibility(View.VISIBLE);
        Constants.client = new RocketChatAPI(Constants.ROCKET_CHAT_URL);
        Constants.client.setReconnectionStrategy(new ReconnectionStrategy(5, 1000));
        Constants.client.connect(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                String fullName = edttxtFullName.getText().toString().trim();
                String mobileNumber = edttxtMobNo.getText().toString().trim();
                String email = edttxtEmail.getText().toString().trim();
                String password = edttxtPassword.getText().toString().trim();
                String dob = edttxtDOB.getText().toString().trim();

                boolean areAllFieldsValid = true;
                if (fullName.isEmpty()) {
                    edttxtFullName.setError("Required");
                    areAllFieldsValid = false;
                } else {
                    edttxtFullName.setError(null);
                }
                if (email.isEmpty()) {
                    edttxtEmail.setError("Required");
                    areAllFieldsValid = false;
                } else {
                    edttxtEmail.setError(null);
                }
                if (!isValidEmail(email)) {
                    edttxtEmail.setError("Invalid email");
                    areAllFieldsValid = false;
                } else {
                    edttxtEmail.setError(null);
                }
                if (password.isEmpty()) {
                    Toast.makeText(mContext, "Password required", Toast.LENGTH_SHORT).show();
                    areAllFieldsValid = false;
                }
                if (dob.isEmpty()) {
                    edttxtDOB.setError("Required");
                    areAllFieldsValid = false;
                } else {
                    edttxtDOB.setError(null);
                }

                int selectedGender = 0;
                RadioButton rbGenderType = findViewById(rgGender.getCheckedRadioButtonId());
                if (rbGenderType.getText().toString().equalsIgnoreCase(getString(R.string.male))) {
                    selectedGender = 1;
                } else if (rbGenderType.getText().toString().equalsIgnoreCase(getString(R.string.female))) {
                    selectedGender = 2;
                }

                if (selectedGender == 0) {
                    Toast.makeText(mContext, "Please select a gender", Toast.LENGTH_SHORT).show();
                    areAllFieldsValid = false;
                }
                if (areAllFieldsValid) {
                    PatientRegistrationInterface patientRegistrationInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientRegistrationInterface.class);
                    try {
                        patientRegistrationInterface.register(fullName, mobileNumber, email, password, String.valueOf(selectedGender), Constants.convertDate(dob, "MM-dd-yyyy", "yyyy-MM-dd"), fcmToken).enqueue(new Callback<PatientRegistrationResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<PatientRegistrationResponse> call, @NonNull Response<PatientRegistrationResponse> response) {
                                if (response.body() != null) {
                                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                                        Constants.userType = "p";
                                        Constants.XUserId = String.valueOf(response.body().getPid());
                                        Constants.XAuthToken = response.body().getAuthToken();
                                        Constants.chatAuthToken = response.body().getChatAuthToken();
                                        Constants.chatUserId = response.body().getChatUserId();
                                        UserDetails userDetails = new UserDetails();
                                        userDetails.setUserName(response.body().getPEmail());
                                        userDetails.setName(response.body().getPName());
                                        userDetails.setCreatedDatetime(String.valueOf(response.body().getCreatedDatetime()));
                                        userDetails.setEmail(response.body().getPEmail());
                                        userDetails.setPpPath(response.body().getPpPath());
                                        Constants.userDetails = userDetails;
                                        logIntoRocketChat();
                                    } else {
                                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<PatientRegistrationResponse> call, @NonNull Throwable t) {
                                CommonUtils.failureMessage(mContext);
                            }
                        });
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
                    }
                }

                break;

            case R.id.txt_login:
                Intent login_Intent = new Intent(mContext, LoginActivity.class);
                startActivity(login_Intent);
                finish();
                break;

            case R.id.edttxt_dob:
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DialogTheme, selectedStartDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
        }
    }

    private void updateDOBDateLabel() {
        String myFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        edttxtDOB.setText(sdf.format(myCalendar.getTime()));
        edttxtDOB.setError(null);
    }

    @Override
    public void onConnect(String sessionID) {
        if (Constants.chatAuthToken != null) {
            Constants.client.loginUsingToken(Constants.chatAuthToken, this);
        } else {
            progressLogin.setVisibility(View.GONE);
            runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show());
        }
    }

    @Override
    public void onDisconnect(boolean closedByServer) {
        logIntoRocketChat();
        Log.i(TAG, "onDisconnect: Closed by Server?: " + closedByServer);
    }

    @Override
    public void onConnectError(Exception e) {
        Log.e(TAG, "onConnectError: " + e.getMessage());
    }

    @Override
    public void onLogin(TokenObject token, final ErrorObject error) {
        runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
        if (error == null) {
            Constants.client.setStatus(UserObject.Status.ONLINE, (success, error1) -> {
                if (success) {
                    System.out.println("Status set to online");
                }
            });
            updateToken();
            getUserDetails();
        } else {
            System.out.println("Got error " + error.getReason());
        }
    }

    private void getUserDetails() {
        if (Constants.userType.equals(Constants.PATIENT)) {
            PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
            patientInterface.getPatientDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<PatientProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<PatientProfileResponse> call, @NonNull Response<PatientProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getPid());
                        Constants.userDetails.setEmail(response.body().getDetails().getPEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getPPhone());
                        Constants.userDetails.setDateOfBirth(response.body().getDetails().getDob());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getPPpPath());
                        Constants.userDetails.setGender(response.body().getDetails().getGender());
                        Constants.userDetails.setQComplete(response.body().getDetails().getQComplete());
                        Constants.userDetails.setUserName(response.body().getDetails().getPEmail());
                        Constants.userDetails.setIsVerified(response.body().getDetails().getIsVerified());

                        startActivity(new Intent(mContext, EmailVerificationActivity.class).putExtra("Message", "Registered successfully! Please verify your email now.").putExtra("isForgotten", false));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PatientProfileResponse> call, @NonNull Throwable t) {
                    if (CommonUtils.failureMessage(mContext, false))
                        runOnUiThread(() -> Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show());
                }
            });
        } else {
            DoctorInterface doctorInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(DoctorInterface.class);
            doctorInterface.getDoctorDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<DoctorProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<DoctorProfileResponse> call, @NonNull Response<DoctorProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getDid());
                        Constants.userDetails.setEmail(response.body().getDetails().getDEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getDPhone());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getDPpPath());
                        Constants.userDetails.setUserName(response.body().getDetails().getDEmail());
                        Constants.userDetails.setdDegree(response.body().getDetails().getDDegree());
                        Constants.userDetails.setHospitalName(response.body().getDetails().getHospitalName());
                        Constants.userDetails.setHospitalAddress(response.body().getDetails().getHospitalAddress());
                        Constants.userDetails.setSpecialityName(response.body().getDetails().getSpName());
                        Constants.userDetails.setSpecialityId(response.body().getDetails().getSpId());
                        startActivity(new Intent(mContext, DoctorDashboardActivity.class));
                        finish();
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorProfileResponse> call, @NonNull Throwable t) {
                    CommonUtils.failureMessage(mContext, true);
                }
            });
        }
    }

    private void updateToken() {
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_AUTH_TOKEN, Constants.chatAuthToken);
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_USER_ID, Constants.chatUserId);
        runOnUiThread(() -> progressLogin.setVisibility(View.VISIBLE));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            String deviceToken = instanceIdResult.getToken();
            FCMTokenUpdateInterface fcmTokenUpdateInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(FCMTokenUpdateInterface.class);
            fcmTokenUpdateInterface.updateFCMToken(Constants.chatAuthToken, Constants.chatUserId, "gcm", deviceToken, getResources().getString(R.string.app_name)).enqueue(new Callback<FCMTokenResponse>() {
                @Override
                public void onResponse(@NonNull Call<FCMTokenResponse> call, @NonNull Response<FCMTokenResponse> response) {
                    runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
                    Log.i("Login", "FCM Token updated");
                }

                @Override
                public void onFailure(@NonNull Call<FCMTokenResponse> call, @NonNull Throwable t) {
                    runOnUiThread(() -> progressLogin.setVisibility(View.GONE));
                    Log.e("Login", "Could not update FCM token");
                }
            });
        });
    }

}
