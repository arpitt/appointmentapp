package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.Constants;

public class EmailVerifySuccessActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verify_success);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = EmailVerifySuccessActivity.this;

        getIds();
    }

    private void getIds() {
        Button btnProceed = findViewById(R.id.btn_proceed);
        btnProceed.setOnClickListener(this);

        TextView txtMsg = findViewById(R.id.txt_msg);
        if (getIntent() != null && getIntent().getExtras() != null) {
            txtMsg.setText(getIntent().getExtras().getString("EmailSuccessMessage"));
        }
    }


    @Override
    public void onClick(View v) {
            if (v.getId() == R.id.btn_proceed) {
                Intent mainIntent;
                if (Constants.userDetails.getQComplete() != null && Constants.userDetails.getQComplete().equalsIgnoreCase("1")) {
                    mainIntent = new Intent(mContext, DashboardActivity.class);
                } else {
                    mainIntent = new Intent(mContext, NewQuestionnairesActivity.class);
                }
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(mainIntent);
                finish();
            }
    }
}
