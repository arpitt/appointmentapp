package com.comlinkinc.medicus.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.adapters.SpecialityAdapter;
import com.comlinkinc.medicus.interfaces.DoctorInterface;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.doctorDetails.DoctorProfileResponse;
import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.DoctorDetailsInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.DoctorUpdateProfileInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SpecialityInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.ApsList.ApsList;
import com.comlinkinc.medicus.retrofit.models.pojo.DoctorDetails.DoctorDetails;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RESULT_SELECT_IMAGE = 1;
    public static ArrayList<String> ids = new ArrayList<>();
    public String imagePath;
    public String SERVER = Constants.MAIN_URL + "upload_pp", timestamp;
    Toolbar toolbar;
    ImageView backButton;
    DoctorUpdateProfileInterface doctorUpdateProfileInterface;
    SpecialityInterface specialityInterface;
    ArrayList<String> names = new ArrayList<>();
    private EditText mTxt_name;
    private EditText mTxt_mob_no;
    private EditText mTxt_email;
    private EditText mTxt_hosp_name;
    private EditText mTxt_hosp_address;
    private EditText mTxt_specialities;
    private EditText mTxt_qualification;
    private TextView btnEdit;
    private Button btnUpdateProfile;
    private CircleImageView profileImage;
    private Dialog dialog;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_details);
        getIds();
        setData();
    }

    private void setData() {
        mTxt_name.setText(Constants.userDetails.getName());
        mTxt_qualification.setText(Constants.userDetails.getdDegree());
        mTxt_email.setText(Constants.userDetails.getEmail());
        mTxt_mob_no.setText(Constants.userDetails.getPhoneNumber());
        mTxt_hosp_name.setText(Constants.userDetails.getHospitalName());
        mTxt_hosp_address.setText(Constants.userDetails.getHospitalAddress());
        Glide.with(getApplicationContext()).load(Constants.userDetails.getPpPath()).into(profileImage);
        mTxt_specialities.setText(Constants.userDetails.getSpecialityName());
    }


    private void getIds() {
        mTxt_name = findViewById(R.id.txt_name);
        toolbar = findViewById(R.id.toolbar);
        backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        setSupportActionBar(toolbar);
        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);
        mTxt_qualification = findViewById(R.id.txt_qualification);
        mTxt_mob_no = findViewById(R.id.txt_mob_no);
        mTxt_email = findViewById(R.id.txt_email);
        mTxt_hosp_name = findViewById(R.id.txt_hosp_name);
        mTxt_hosp_address = findViewById(R.id.txt_hosp_address);
        mTxt_specialities = findViewById(R.id.txt_specialities);
        profileImage = findViewById(R.id.profile_image);
        profileImage.setOnClickListener(this);
        mTxt_specialities.setOnClickListener(this);

        mTxt_name.setFocusable(false);
        mTxt_name.setFocusableInTouchMode(false);
        mTxt_name.setClickable(false);

        mTxt_qualification.setFocusable(false);
        mTxt_qualification.setFocusableInTouchMode(false);
        mTxt_qualification.setClickable(false);

        mTxt_hosp_name.setFocusable(false);
        mTxt_hosp_name.setFocusableInTouchMode(false);
        mTxt_hosp_name.setClickable(false);

        mTxt_hosp_address.setFocusable(false);
        mTxt_hosp_address.setFocusableInTouchMode(false);
        mTxt_hosp_address.setClickable(false);

        mTxt_specialities.setFocusable(false);
        mTxt_specialities.setFocusableInTouchMode(false);
        mTxt_specialities.setClickable(false);
        mTxt_specialities.setEnabled(false);

        mTxt_mob_no.setFocusable(false);
        mTxt_mob_no.setFocusableInTouchMode(false);
        mTxt_mob_no.setClickable(false);

        btnEdit = findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(this);

        btnUpdateProfile = findViewById(R.id.btn_update_profile);
        btnUpdateProfile.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update_profile:
                if (mTxt_name.getText().toString().trim().equals("")) {
                    mTxt_name.setError("Enter full name");
                } else {
                    updateProfileData();
                }
                break;

            case R.id.back_button:
                finish();
                break;

            case R.id.btn_edit:
                if (btnEdit.getText().toString().equalsIgnoreCase("Edit")) {
                    btnUpdateProfile.setVisibility(View.VISIBLE);
                    btnEdit.setText("Cancel");

                    mTxt_name.setFocusable(true);
                    mTxt_name.setFocusableInTouchMode(true);
                    mTxt_name.setClickable(true);

                    mTxt_qualification.setFocusable(true);
                    mTxt_qualification.setFocusableInTouchMode(true);
                    mTxt_qualification.setClickable(true);

                    mTxt_mob_no.setFocusable(true);
                    mTxt_mob_no.setFocusableInTouchMode(true);
                    mTxt_mob_no.setClickable(true);

                    mTxt_hosp_name.setFocusable(true);
                    mTxt_hosp_name.setFocusableInTouchMode(true);
                    mTxt_hosp_name.setClickable(true);

                    mTxt_hosp_address.setFocusable(true);
                    mTxt_hosp_address.setFocusableInTouchMode(true);
                    mTxt_hosp_address.setClickable(true);

                    mTxt_specialities.setFocusable(false);
                    mTxt_specialities.setFocusableInTouchMode(false);
                    mTxt_specialities.setClickable(true);
                    mTxt_specialities.setEnabled(true);

                } else {
                    btnUpdateProfile.setVisibility(View.GONE);
                    btnEdit.setText("Edit");

                    mTxt_name.setFocusable(false);
                    mTxt_name.setFocusableInTouchMode(false);
                    mTxt_name.setClickable(false);

                    mTxt_mob_no.setFocusable(false);
                    mTxt_mob_no.setFocusableInTouchMode(false);
                    mTxt_mob_no.setClickable(false);

                    mTxt_hosp_name.setFocusable(false);
                    mTxt_hosp_name.setFocusableInTouchMode(false);
                    mTxt_hosp_name.setClickable(false);

                    mTxt_hosp_address.setFocusable(false);
                    mTxt_hosp_address.setFocusableInTouchMode(false);
                    mTxt_hosp_address.setClickable(false);

                    mTxt_specialities.setFocusable(false);
                    mTxt_specialities.setFocusableInTouchMode(false);
                    mTxt_specialities.setClickable(false);
                    mTxt_specialities.setEnabled(false);
                }
                break;
            case R.id.profile_image:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_SELECT_IMAGE);
                break;

            case R.id.txt_specialities:
                showBottomSheetDialog();
                break;

        }
    }

    private void updateProfileData() {
        doctorUpdateProfileInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(DoctorUpdateProfileInterface.class);
        doctorUpdateProfileInterface.updateDoctorProfile(Constants.XAuthToken, Constants.XUserId, Constants.userDetails.getPatientId(), mTxt_name.getText().toString(), mTxt_qualification.getText().toString(), mTxt_mob_no.getText().toString(), mTxt_hosp_name.getText().toString(), mTxt_hosp_address.getText().toString(), ids).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Toast.makeText(DoctorDetailsActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                    mTxt_name.setFocusable(false);
                    mTxt_name.setFocusableInTouchMode(false);
                    mTxt_name.setClickable(false);
                    mTxt_mob_no.setFocusable(false);
                    mTxt_mob_no.setFocusableInTouchMode(false);
                    mTxt_mob_no.setClickable(false);
                    getDetails();
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void loadProfile(String url) {
        Glide.with(getApplicationContext()).load(url)
                .apply(new RequestOptions()
                        .error(R.drawable.profile)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .priority(Priority.HIGH))
                .into(profileImage);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == RESULT_SELECT_IMAGE) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), uri);

                    loadProfile(uri.toString());
                    String fileName = Prefs.getSharedPreferenceString(getApplicationContext(), Constants.userDetails.getPatientId(), "") + "_" + System.currentTimeMillis();
                    File f = new File(getApplicationContext().getCacheDir(), fileName + ".png");
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                    byte[] bitmapData = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapData);
                    fos.flush();
                    fos.close();

                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), f);
                    MultipartBody.Part part = MultipartBody.Part.createFormData("file", f.getName(), fileReqBody);
                    PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
                    patientInterface.uploadProfilePhoto(Constants.XAuthToken, Constants.XUserId, part).enqueue(new Callback<ForgotPasswordResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                            if (response.body() != null) {
                                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200))
                                    getDetails();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                            CommonUtils.failureMessage(getApplicationContext(), true);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getDetails() {
        DoctorInterface doctorInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(DoctorInterface.class);
        doctorInterface.getDoctorDetails(Constants.XAuthToken, Constants.XUserId, Constants.userDetails.getPatientId()).enqueue(new Callback<DoctorProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<DoctorProfileResponse> call, @NonNull Response<DoctorProfileResponse> response) {
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    Constants.userDetails.setPatientId(response.body().getDetails().getDid());
                    Constants.userDetails.setName(response.body().getDetails().getDName());
                    Constants.userDetails.setdDegree(response.body().getDetails().getDDegree());
                    Constants.userDetails.setEmail(response.body().getDetails().getDEmail());
                    Constants.userDetails.setPhoneNumber(response.body().getDetails().getDPhone());
                    Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getDPpPath());
                    Constants.userDetails.setHospitalAddress(response.body().getDetails().getHospitalAddress());
                    Constants.userDetails.setHospitalName(response.body().getDetails().getHospitalName());
                    Constants.userDetails.setSpecialityId(response.body().getDetails().getSpId());
                    getSpeciality(Constants.userDetails.getSpecialityId());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorProfileResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(getApplicationContext(), true);
            }
        });
    }

    private void getSpeciality(String spIds) {
        specialityInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(SpecialityInterface.class);
        specialityInterface.specialities(Constants.XAuthToken, Constants.XUserId).enqueue(new Callback<ApsList>() {
            @Override
            public void onResponse(@NonNull Call<ApsList> call, @NonNull Response<ApsList> response) {
                if (response.body() != null) {
                    names = new ArrayList<>();
                    String[] split = spIds.split(", ");
                    ids.clear();
                    ids.addAll(Arrays.asList(split));
                    for (int i = 0; i < response.body().getApsList().size(); i++) {
                        String id = response.body().getApsList().get(i).getSpId();
                        String n = response.body().getApsList().get(i).getSpName();
                        for (int j = 0; j < ids.size(); j++) {
                            String id1 = ids.get(j);
                            if (id.contains(id1)) {
                                names.add(n);
                            }
                        }
                    }
                    String csv = TextUtils.join(", ", names);
                    mTxt_specialities.setText(csv);
                    Constants.userDetails.setSpecialityName(TextUtils.join(", ", names));
                    setData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApsList> call, @NonNull Throwable t) {

            }
        });
    }

    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.speciality_basic, null);
        RecyclerView list = view.findViewById(R.id.list);
        RecyclerView.LayoutManager lmanager = new LinearLayoutManager(DoctorDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(lmanager);
        specialityInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(SpecialityInterface.class);
        specialityInterface.specialities(Constants.XAuthToken, Constants.XUserId).enqueue(new Callback<ApsList>() {
            @Override
            public void onResponse(@NonNull Call<ApsList> call, @NonNull Response<ApsList> response) {
                if (response.body() != null) {
                    ids.clear();
                    ids.addAll(Arrays.asList(Constants.userDetails.getSpecialityId().split(", ")));
                    SpecialityAdapter adapter = new SpecialityAdapter(DoctorDetailsActivity.this, response.body().getApsList(), ids);
                    list.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApsList> call, @NonNull Throwable t) {

            }
        });

        (view.findViewById(R.id.cancel)).setOnClickListener(view1 -> mBottomSheetDialog.dismiss());

        (view.findViewById(R.id.ok)).setOnClickListener(v -> {
            mBottomSheetDialog.dismiss();
            Constants.userDetails.setSpecialityId(TextUtils.join(", ", ids));
            getSpeciality(Constants.userDetails.getSpecialityId());
        });

        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

}