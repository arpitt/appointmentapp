package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.PasswordInterface;
import com.comlinkinc.medicus.interfaces.PatientRegistrationInterface;
import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.Constants;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private EditText eTDynamicAccessCode;
    private ProgressBar progressBar;
    private boolean isResetPassword = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = EmailVerificationActivity.this;

        getIds();
    }


    private void getIds() {
        Intent i = getIntent();
        TextInputLayout tilPassword = findViewById(R.id.tilPassword);

        Button btnProceed = findViewById(R.id.btn_proceed);
        btnProceed.setOnClickListener(this);

        TextView btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        TextView btnResend = findViewById(R.id.btn_resend);
        btnResend.setOnClickListener(this);

        TextView txtMsg = findViewById(R.id.txt_msg);
        if (i != null && i.getExtras() != null) {
            isResetPassword = i.getBooleanExtra("isForgotten", false);
            if (isResetPassword) tilPassword.setVisibility(View.VISIBLE);
            txtMsg.setText(i.getExtras().getString("Message"));
        }

        eTDynamicAccessCode = findViewById(R.id.edttxt_dymanic_access_code);
        progressBar = findViewById(R.id.progress_verify_email);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed:
                String resetCode = eTDynamicAccessCode.getText().toString().trim();
                if (resetCode.isEmpty()) {
                    eTDynamicAccessCode.setError("Required");
                } else {
                    eTDynamicAccessCode.setError(null);
                    if(!isResetPassword) {
                        verifyEmail(resetCode);
                    } else {
                        TextView eTPassword = findViewById(R.id.eTPassword);
                        String password = eTPassword.getText().toString().trim();
                        if (password.isEmpty()) {
                            Toast.makeText(mContext, "Password required.", Toast.LENGTH_SHORT).show();
                        } else {
                            resetEmail(resetCode, password);
                        }
                    }
                }
                break;
            case R.id.btn_login:
                Intent mainIntent = new Intent(mContext, LoginActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(mainIntent);
                break;
            case R.id.btn_resend:
                sendVerificationMail();
                break;
        }
    }

    private void verifyEmail(String resetCode) {
        progressBar.setVisibility(View.VISIBLE);
        PatientRegistrationInterface verifyEmail = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientRegistrationInterface.class);
        verifyEmail.verifyEmail(Constants.userDetails.getEmail(), resetCode).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        startActivity(new Intent(mContext, EmailVerifySuccessActivity.class).putExtra("EmailSuccessMessage", response.body().getMessage()));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                CommonUtils.failureMessage(mContext);
            }
        });
    }

    private void sendVerificationMail() {
        PasswordInterface passwordInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PasswordInterface.class);
        passwordInterface.forgotPassword(Constants.userDetails.getEmail(), "1").enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Toast.makeText(mContext, "Email sent again!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "Could not send the email again. Please contact support.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext);
            }
        });
    }

    public void resetEmail(String resetCode, String password) {
        progressBar.setVisibility(View.VISIBLE);
        PasswordInterface passwordInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PasswordInterface.class);
        passwordInterface.resetPassword(Constants.userDetails.getEmail(), password, resetCode).enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        startActivity(new Intent(mContext, EmailVerifySuccessActivity.class).putExtra("EmailSuccessMessage", response.body().getMessage()));
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        Toast.makeText(mContext, "Could not verify email.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                CommonUtils.failureMessage(mContext);
            }
        });
    }
}
