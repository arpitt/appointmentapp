package com.comlinkinc.medicus.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.adapters.PatientListAdapter;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.fragments.ChatDoctorMembersFragment;
import com.comlinkinc.medicus.fragments.ResetPasswordFragment;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.PatientListInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.PatientList.PatientList;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.listener.ConnectListener;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.callback.LoginListener;
import com.rocketchat.core.model.SubscriptionObject;
import com.rocketchat.core.model.TokenObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorDashboardActivity extends AppCompatActivity implements ConnectListener, LoginListener, GetSubscriptionListener {

    private final static int LOADING_DURATION = 2000;
    private static final String TAG = "DoctorDashboardActivity";
    PatientListAdapter adapter = null;
    RecyclerView recyclerView;
    DrawerLayout drawerLayout;
    ImageView menu;
    NavigationView navigationView;
    SwipeRefreshLayout refresh;
    TextView listsize, tVUnreadCount;
    LinearLayout error;
    ImageView iVMessages;
    ImageView backButton;
    List<SubscriptionObject> subscribedMembers;
    String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG, Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    private FragmentManager fragmentManager;
    private Toolbar toolbar;
    private Activity mActivity;
    private Context mContext;
    private int unreadMessagesCount = 0;
    private boolean showMessageCounter = true;
    private Handler handler = new Handler();
    private boolean isInternetConnected = true;
    private ProgressBar pBLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_dashboard);
        mContext = this;
        mActivity = this;
        getPermissions();
        fragmentManager = getSupportFragmentManager();
        recyclerView = findViewById(R.id.patient_list);
        refresh = findViewById(R.id.refresh);
        error = findViewById(R.id.error);
        toolbar = findViewById(R.id.toolbar);
        menu = findViewById(R.id.imageView6);
        pBLoading = findViewById(R.id.pBLoading);

        backButton = findViewById(R.id.back_button);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);
        listsize = findViewById(R.id.textView2);
        setSupportActionBar(toolbar);

        tVUnreadCount = toolbar.findViewById(R.id.tVUnreadCount);
        iVMessages = toolbar.findViewById(R.id.iVMessages);

        if (getSupportActionBar() != null) {
            toolbar.setTitle("Dashboard");
            getSupportActionBar().setTitle("Dashboard");
        }
        refresh.setOnRefreshListener(() -> {
            updateProfileHeader();
            getPatients();
        });
//        updateProfileHeader();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        menu.setOnClickListener(view -> drawerLayout.openDrawer(GravityCompat.START));
        handler.postDelayed(new Runnable() {
            public void run() {
                getPatients();
                if (Constants.client != null) {
                    unreadMessagesCount = 0;
                    Constants.client.getSubscriptions(DoctorDashboardActivity.this);
                }
                handler.postDelayed(this, LOADING_DURATION);
            }
        }, LOADING_DURATION);

        navigationView.setNavigationItemSelectedListener(item -> {
            showMessageCounter(true);
            drawerLayout.closeDrawers();
            switch (item.getItemId()) {
                case R.id.nav_my_profile:
                    Intent in = new Intent(mActivity, DoctorDetailsActivity.class);
                    startActivity(in);
                    break;

                case R.id.nav_reset_password:
                    startActivity(new Intent(mContext, ResetPasswordActivity.class));
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                    replaceFragments(ResetPasswordFragment.class, null, getResources().getString(R.string.lbl_reset_password), true);
                    break;

                case R.id.nav_logout:
                    CommonUtils.hideKeyboard(mActivity);
                    logoutDialog();
                    break;
            }
            return true;
        });
        showMessageCounter(true);
    }

    public void updateProfileHeader() {
        View header = navigationView.getHeaderView(0);
        CircleImageView cVProfile = header.findViewById(R.id.profile_image);
        Glide.with(mContext).load(Constants.userDetails.getPpPath()).placeholder(R.drawable.user_default_thumb).into(cVProfile);
        TextView tVName = header.findViewById(R.id.txt_title_name_nav);
        tVName.setText(Constants.userDetails.getName());
        TextView tVEmail = header.findViewById(R.id.txt_title_email_nav);
        tVEmail.setText(Constants.userDetails.getEmail());
    }

    private void getPatients() {
        if (adapter == null) pBLoading.setVisibility(View.VISIBLE);
        PatientListInterface patientListInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientListInterface.class);
        patientListInterface.getPatients(Constants.XAuthToken, Constants.XUserId, Constants.userDetails.getPatientId()).enqueue(new Callback<PatientList>() {
            @Override
            public void onResponse(@NonNull Call<PatientList> call, @NonNull Response<PatientList> response) {
                runOnUiThread(() -> {
                    pBLoading.setVisibility(View.GONE);
                    refresh.setRefreshing(false);
                });
                isInternetConnected = true;
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    if (response.body().getDrPtList().size() != 0) {
                        adapter = new PatientListAdapter(DoctorDashboardActivity.this, response.body().getDrPtList(), subscribedMembers);
                        runOnUiThread(() -> {
                            recyclerView.setAdapter(adapter);
                            listsize.setText(String.valueOf(response.body().getDrPtList().size()));
                        });
                    } else {
                        adapter = null;
                        runOnUiThread(() -> error.setVisibility(View.VISIBLE));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PatientList> call, @NonNull Throwable t) {
                adapter = null;
                runOnUiThread(() -> {
                    pBLoading.setVisibility(View.GONE);
                    refresh.setRefreshing(false);
                    isInternetConnected = CommonUtils.failureMessage(mContext, isInternetConnected);
                });
            }
        });
    }

    public void setToolbarBarTitle(String title) {
        toolbar.setTitle(title);
    }

    public void logoutDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);
        alertDialog.setTitle(getString(R.string.header_logout));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getString(R.string.want_to_logout));
        alertDialog.setIcon(R.drawable.ic_logout_black);
        alertDialog.setPositiveButton(getString(R.string.yes_please), (dialog, which) -> {
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_EMAIL, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_EMAIL, "");
            Prefs.setSharedPreferenceString(mContext, Prefs.PREF_COMLINK_PASSWORD, "");
            Intent intent = new Intent(mContext, SplashScreenActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        });

        alertDialog.setNegativeButton(getString(R.string.no_stay_here), (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            updateProfileHeader();
        } catch (Exception e) {
            Log.e(TAG, "onResume: " + e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        boolean isInsideFragment = false;
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f instanceof ChatDoctorMembersFragment) {
                ((ChatDoctorMembersFragment) f).onBackPressed();
                isInsideFragment = true;
            }
        }
        if (!isInsideFragment) super.onBackPressed();
    }

    private void getPermissions() {
        Dexter.withContext(mContext).withPermissions(PERMISSIONS).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                multiplePermissionsReport.areAllPermissionsGranted(); //TODO - Do something
                if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                    CommonUtils.showAlertDialog(mContext, "Permission Denied", "You've denied one of the permissions required permanently. You can enable it if you wish to from your phone's settings.");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).onSameThread().check();
    }

    public void showMessageCounter(boolean status) {
        showMessageCounter = status;
        if (showMessageCounter && unreadMessagesCount > 0) {
            tVUnreadCount.setVisibility(View.VISIBLE);
            iVMessages.setVisibility(View.VISIBLE);
        } else {
            tVUnreadCount.setVisibility(View.GONE);
            iVMessages.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnect(String sessionID) {
        if (Constants.client != null)
            Constants.client.loginUsingToken(Constants.chatAuthToken, this);
    }

    @Override
    public void onDisconnect(boolean closedByServer) {
        Constants.client.reconnect();
    }

    @Override
    public void onConnectError(Exception e) {
        Log.e(TAG, "onConnectError: " + e.getMessage());
    }

    @Override
    public void onGetSubscriptions(List<SubscriptionObject> subscriptions, ErrorObject error) {
        subscribedMembers = new ArrayList<>();
        for (int i = 0; i < subscriptions.size(); i++) {
            if (("" + subscriptions.get(i).getRoomType()).equalsIgnoreCase("ONE_TO_ONE")) {
                unreadMessagesCount += subscriptions.get(i).getUnread();
                subscribedMembers.add(subscriptions.get(i));
            }
        }

        if (subscribedMembers != null) {
            Collections.reverse(subscribedMembers);
        }
        if (showMessageCounter && unreadMessagesCount > 0) {
            runOnUiThread(() -> {
                tVUnreadCount.setText(String.valueOf(unreadMessagesCount));
                tVUnreadCount.setVisibility(View.VISIBLE);
                iVMessages.setVisibility(View.VISIBLE);
            });
        } else {
            runOnUiThread(() -> {
                tVUnreadCount.setVisibility(View.GONE);
                iVMessages.setVisibility(View.GONE);
            });
        }
    }

    @Override
    public void onLogin(TokenObject token, ErrorObject error) {

    }
}
