package com.comlinkinc.medicus.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.DoctorInterface;
import com.comlinkinc.medicus.interfaces.LoginInterface;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.doctorDetails.DoctorProfileResponse;
import com.comlinkinc.medicus.responses.login.LoginResponse;
import com.comlinkinc.medicus.responses.patientDetails.PatientProfileResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.FCMTokenUpdateInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.fcm.FCMTokenResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.data.model.UserObject;
import com.rocketchat.common.listener.ConnectListener;
import com.rocketchat.common.network.ReconnectionStrategy;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.callback.LoginListener;
import com.rocketchat.core.model.TokenObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenActivity extends AppCompatActivity implements ConnectListener, LoginListener {

    private static final String TAG = "SplashScreenActivity";
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private Context mContext;
    private ProgressBar pBLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mContext = SplashScreenActivity.this;
        pBLoading = findViewById(R.id.progress_register);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        new Handler().postDelayed(() -> {
            String strEmail = Prefs.getSharedPreferenceString(mContext, Prefs.PREF_EMAIL, "");
            String strPassword = Prefs.getSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, "");

            if (!strEmail.trim().equals("") && !strPassword.trim().equals("")) {
                performLogin(strEmail, strPassword);
            } else {
                Intent mainIntent = new Intent(mContext, LogisterActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        new Handler().postDelayed(() -> {
            String strEmail = Prefs.getSharedPreferenceString(mContext, Prefs.PREF_EMAIL, "");
            String strPassword = Prefs.getSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, "");

            String strComlinkUsername = Prefs.getSharedPreferenceString(mContext, Constants.PREFS_UNAME, "");
            String strComlinkPassword = Prefs.getSharedPreferenceString(mContext, Constants.PREFS_PSWD, "");

            if (!strEmail.trim().equals("") && !strPassword.trim().equals("") && !strComlinkUsername.trim().equals("")
                    && !strComlinkPassword.trim().equals("")) {
                performLogin(strEmail, strPassword);
            } else {
                Intent mainIntent = new Intent(mContext, LogisterActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    void performLogin(String userName, String password) {
        pBLoading.setVisibility(View.VISIBLE);
        LoginInterface loginInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(LoginInterface.class);
        loginInterface.login(userName, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                pBLoading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    Constants.userType = response.body().getData().getUserType();
                    Constants.XUserId = response.body().getData().getUserId();
                    Constants.XAuthToken = response.body().getData().getAuthToken();
                    Constants.chatAuthToken = response.body().getData().getChatAuthToken();
                    Constants.chatUserId = response.body().getData().getChatUserId();
                    Constants.userDetails = response.body().getData().getUserDetails();
                    logIntoRocketChat();
                } else {
                    startActivity(new Intent(mContext, LogisterActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                pBLoading.setVisibility(View.GONE);
                CommonUtils.failureMessage(mContext);
            }
        });
    }

    private void logIntoRocketChat() {
        pBLoading.setVisibility(View.VISIBLE);
        Log.i(TAG, "logIntoRocketChat: Trying to log into RocketChat now");
        Constants.client = new RocketChatAPI(Constants.ROCKET_CHAT_URL);
        Constants.client.setReconnectionStrategy(new ReconnectionStrategy(5, 1000));
        Constants.client.connect(this);
    }


    @Override
    public void onConnect(String sessionID) {
        if (Constants.chatAuthToken != null) {
            Constants.client.loginUsingToken(Constants.chatAuthToken, this);
        } else {
            pBLoading.setVisibility(View.GONE);
            runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show());
        }
    }

    @Override
    public void onDisconnect(boolean closedByServer) {
        logIntoRocketChat();
        Log.i(TAG, "onDisconnect: Closed by Server?: " + closedByServer);
    }

    @Override
    public void onConnectError(Exception e) {
        Log.e(TAG, "onConnectError: " + e.getMessage());
        pBLoading.setVisibility(View.GONE);
        Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLogin(TokenObject token, ErrorObject error) {
        runOnUiThread(() -> pBLoading.setVisibility(View.GONE));
        if (error == null) {
            Constants.client.setStatus(UserObject.Status.ONLINE, (success, error1) -> {
                if (success) {
                    System.out.println("Status set to online");
                }
            });
            updateToken();
            getUserDetails();
        } else if (error.getError() == 403) {
            performLogin(Prefs.getSharedPreferenceString(mContext, Prefs.PREF_EMAIL, ""), Prefs.getSharedPreferenceString(mContext, Prefs.PREF_PASSWORD, ""));
        }
        else {
            runOnUiThread(() -> {
                pBLoading.setVisibility(View.GONE);
                Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show();
            });
        }
    }

    private void updateToken() {
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_AUTH_TOKEN, Constants.chatAuthToken);
        Prefs.setSharedPreferenceString(mContext, Prefs.PREF_USER_ID, Constants.chatUserId);
        runOnUiThread(() -> pBLoading.setVisibility(View.VISIBLE));
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            String deviceToken = instanceIdResult.getToken();
            FCMTokenUpdateInterface fcmTokenUpdateInterface = RetrofitClient.getClient(Constants.BASE_URL).create(FCMTokenUpdateInterface.class);
            fcmTokenUpdateInterface.updateFCMToken(Constants.chatAuthToken, Constants.chatUserId, "gcm", deviceToken, getResources().getString(R.string.app_name)).enqueue(new Callback<FCMTokenResponse>() {
                @Override
                public void onResponse(@NonNull Call<FCMTokenResponse> call, @NonNull Response<FCMTokenResponse> response) {
                    runOnUiThread(() -> pBLoading.setVisibility(View.GONE));
                    Log.i("Login", "FCM Token updated");
                }

                @Override
                public void onFailure(@NonNull Call<FCMTokenResponse> call, @NonNull Throwable t) {
                    runOnUiThread(() -> pBLoading.setVisibility(View.GONE));
                    Log.e("Login", "Could not update FCM token");
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void getUserDetails() {
        if (Constants.userType.equals(Constants.PATIENT)) {
            PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
            patientInterface.getPatientDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<PatientProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<PatientProfileResponse> call, @NonNull Response<PatientProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getPid());
                        Constants.userDetails.setEmail(response.body().getDetails().getPEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getPPhone());
                        Constants.userDetails.setDateOfBirth(response.body().getDetails().getDob());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getPPpPath());
                        Constants.userDetails.setGender(response.body().getDetails().getGender());
                        Constants.userDetails.setQComplete(response.body().getDetails().getQComplete());
                        Constants.userDetails.setUserName(response.body().getDetails().getPEmail());
                        Constants.userDetails.setIsVerified(response.body().getDetails().getIsVerified());

                        if (Constants.userDetails.getQComplete().equalsIgnoreCase("1") && Constants.userDetails.getIsVerified().equalsIgnoreCase("1")) {
                            startActivity(new Intent(mContext, DashboardActivity.class));
                            finish();
                        } else if (Constants.userDetails.getIsVerified().equalsIgnoreCase("0")) {
                            startActivity(new Intent(mContext, EmailVerificationActivity.class).putExtra("Message", "Registered successfully! Please verify your email now.").putExtra("isForgotten", false));
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else if (Constants.userDetails.getQComplete().equalsIgnoreCase("0")) {
                            Intent mainIntent = new Intent(mContext, NewQuestionnairesActivity.class);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong. Please contact the system admin.", Toast.LENGTH_SHORT).show());
                        }
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PatientProfileResponse> call, @NonNull Throwable t) {
                    CommonUtils.failureMessage(mContext, true);
                }
            });
        } else {
            DoctorInterface doctorInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(DoctorInterface.class);
            doctorInterface.getDoctorDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<DoctorProfileResponse>() {
                @Override
                public void onResponse(@NonNull Call<DoctorProfileResponse> call, @NonNull Response<DoctorProfileResponse> response) {
                    if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        Constants.userDetails.setPatientId(response.body().getDetails().getDid());
                        Constants.userDetails.setEmail(response.body().getDetails().getDEmail());
                        Constants.userDetails.setPhoneNumber(response.body().getDetails().getDPhone());
                        Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getDPpPath());
                        Constants.userDetails.setUserName(response.body().getDetails().getDEmail());
                        Constants.userDetails.setdDegree(response.body().getDetails().getDDegree());
                        Constants.userDetails.setHospitalName(response.body().getDetails().getHospitalName());
                        Constants.userDetails.setHospitalAddress(response.body().getDetails().getHospitalAddress());
                        Constants.userDetails.setSpecialityName(response.body().getDetails().getSpName());
                        Constants.userDetails.setSpecialityId(response.body().getDetails().getSpId());
                        startActivity(new Intent(mContext, DoctorDashboardActivity.class));
                        finish();
                    } else {
                        runOnUiThread(() -> Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorProfileResponse> call, @NonNull Throwable t) {
                    CommonUtils.failureMessage(mContext, true);
                }
            });
        }
    }

}
