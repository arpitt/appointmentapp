package com.comlinkinc.medicus.progress;

import android.view.animation.Animation;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  17 Jan 2018
 ***********************************************************************/

public abstract class AnimationListener implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {
        // stub
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // stub
    }
}
