package com.comlinkinc.medicus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.comlinkinc.medicus.R;


/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  17 Jul 2018
 ***********************************************************************/

public class DialogMembersMoreOptions extends Dialog implements View.OnClickListener {

    public Activity c;
    public Dialog d;
    public TextView optionFiles, optionPinnedMsg, optionStarredMsg;

    public DialogMembersMoreOptions(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_member_more_options);
        optionFiles = findViewById(R.id.option_files);
        optionPinnedMsg = findViewById(R.id.option_pinned_msg);
        optionStarredMsg = findViewById(R.id.options_starred_msg);

        optionFiles.setOnClickListener(this);
        optionPinnedMsg.setOnClickListener(this);
        optionStarredMsg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.option_files:
//                Intent filesIntent = new Intent(c, FilesActivity.class);
//                c.startActivity(filesIntent);
//                dismiss();
                break;
            case R.id.option_pinned_msg:
//                Intent pinnedIntent = new Intent(c, PinnedMessageActivity.class);
//                c.startActivity(pinnedIntent);
//                dismiss();
                break;
            case R.id.options_starred_msg:
//                Intent starredIntent = new Intent(c, StarredMessageActivity.class);
//                c.startActivity(starredIntent);
//                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}
