package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.responses.patientRegistration.PatientRegistrationResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PatientRegistrationInterface {
    @FormUrlEncoded
    @POST("mapi/v1/register")
    Call<PatientRegistrationResponse> register(@Field("un") String userName,
                                               @Field("m") String mobileNumber,
                                               @Field("ue") String emailAddress,
                                               @Field("p") String password,
                                               @Field("g") String gender,
                                               @Field("dob") String dateOfBirth,
                                               @Field("dt") String fcmToken);

    @FormUrlEncoded
    @POST("mapi/v1/email_verify")
    Call<ForgotPasswordResponse> verifyEmail(@Field("pe") String emailAddress,
                                             @Field("rc") String resetCode);
}
