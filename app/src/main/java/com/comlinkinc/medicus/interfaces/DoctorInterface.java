package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.doctorDetails.DoctorProfileResponse;
import com.comlinkinc.medicus.responses.doctorResponses.SpecialityListResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DoctorInterface {
    @POST("mapi/v1/doctor/{did}")
    Call<DoctorProfileResponse> getDoctorDetails(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId, @Path("did") String doctorId);

    @FormUrlEncoded
    @POST("mapi/v1/specialities/list")
    Call<SpecialityListResponse> getSpecialityList(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId);
}
