package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.login.LoginResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface LoginInterface {
    @FormUrlEncoded
    @POST("mapi/v1/login")
    Call<LoginResponse> login(@Field("user") String userName, @Field("password") String password);
}