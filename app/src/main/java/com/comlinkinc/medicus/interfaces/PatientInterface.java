package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.responses.labTests.LabTestListResponse;
import com.comlinkinc.medicus.responses.patientDetails.PatientProfileResponse;
import com.comlinkinc.medicus.responses.patientDetails.PatientReportsResponse;
import com.comlinkinc.medicus.responses.patientDetails.UploadReportResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface PatientInterface {
    @POST("mapi/v1/patients/{pid}")
    Call<PatientProfileResponse> getPatientDetails(@Header("X-Auth-Token") String authToken,
                                                   @Header("X-User-Id") String userId,
                                                   @Path("pid") String patientId);

    @FormUrlEncoded
    @POST("mapi/v1/update_patient")
    Call<ForgotPasswordResponse> updatePatientDetails(@Header("X-Auth-Token") String authToken,
                                                      @Header("X-User-Id") String userId,
                                                      @Field("pid") String patientId,
                                                      @Field("un") String fullName,
                                                      @Field("m") String mobileNumber,
                                                      @Field("g") String gender,
                                                      @Field("dob") String dateOfBirth);

    @Multipart
    @POST("mapi/v1/upload_pp")
    Call<ForgotPasswordResponse> uploadProfilePhoto(@Header("X-Auth-Token") String authToken,
                                                    @Header("X-User-Id") String userId,
                                                    @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("mapi/v1/patient_tests")
    Call<LabTestListResponse> getLabTests(@Header("X-Auth-Token") String authToken,
                                          @Header("X-User-Id") String userId,
                                          @Field("pid") String patientId);

    @FormUrlEncoded
    @POST("mapi/v1/patients_reports")
    Call<PatientReportsResponse> getReports(@Header("X-Auth-Token") String authToken,
                                            @Header("X-User-Id") String userId,
                                            @Field("pid") String patientId);

    @Multipart
    @POST("mapi/v1/upload_report")
    Call<UploadReportResponse> uploadReport(@Header("X-Auth-Token") String authToken,
                                            @Header("X-User-Id") String userId,
                                            @PartMap Map<String, RequestBody> params,
                                            @Part MultipartBody.Part file);
}
