package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.login.LogoutResponse;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LogoutInterface {
    @FormUrlEncoded
    @POST("mapi/v1/logout")
    Call<LogoutResponse> logout(@Header("X-Auth-Token") String authToken, @Header("X-User-Id") String userId);
}
