package com.comlinkinc.medicus.interfaces;

import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PasswordInterface {
    @FormUrlEncoded
    @POST("mapi/v1/forgot_password")
    Call<ForgotPasswordResponse> forgotPassword(@Field("email_address") String emailId,
                                                @Field("verify") String isVerification);

    @FormUrlEncoded
    @POST("mapi/v1/reset_password")
    Call<ForgotPasswordResponse> resetPassword(@Field("email_address") String emailId,
                                               @Field("pwd") String newPassword,
                                               @Field("rc") String resetToken);
    @FormUrlEncoded
    @POST("mapi/v1/new_password")
    Call<ForgotPasswordResponse> changePassword(@Header("X-Auth-Token") String authToken,
                                                @Header("X-User-Id") String userId,
                                                @Field("op") String oldPassword,
                                                @Field("np") String newPassword);
}
