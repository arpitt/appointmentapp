package com.comlinkinc.medicus.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.responses.labTests.LabTestList;

import java.util.List;

public class LabTestListAdapter extends RecyclerView.Adapter<LabTestListAdapter.ItemHolder> {
    private List<LabTestList> apsList;

    public LabTestListAdapter(List<LabTestList> data) {
        apsList = data;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_labtest, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.tVTestName.setText(apsList.get(position).getLabTestName());
        holder.tVOldDate.setText(apsList.get(position).getOldDate());
        holder.tVUpcomingDate.setText(apsList.get(position).getUpcomingDate());
    }

    @Override
    public int getItemCount() {
        return apsList.size();
    }


    static class ItemHolder extends RecyclerView.ViewHolder {
        TextView tVTestName, tVOldDate, tVUpcomingDate;

        ItemHolder(@NonNull View itemView) {
            super(itemView);
            tVTestName = itemView.findViewById(R.id.txt_labtestname);
            tVOldDate = itemView.findViewById(R.id.txt_olddate);
            tVUpcomingDate = itemView.findViewById(R.id.txt_upcommingdate);
        }
    }
}