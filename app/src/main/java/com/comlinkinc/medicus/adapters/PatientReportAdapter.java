package com.comlinkinc.medicus.adapters;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.FullscreenActivity;
import com.comlinkinc.medicus.responses.patientDetails.ReportList;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class PatientReportAdapter extends RecyclerView.Adapter<PatientReportAdapter.ItemHolder>{

    private Context context;
    private String baseUrl;
    private List<ReportList> data;
    private NotificationCompat.Builder builder;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy hh:mm aa", Locale.getDefault());
    private int progress = 0;


    public PatientReportAdapter(Context context, String baseUrl, List<ReportList> data) {
        this.context = context;
        this.baseUrl = baseUrl;
        this.data = data;
        sdf.setTimeZone(TimeZone.getDefault());
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
     View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_patient_report, parent, false);
     return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        if (holder.iVDocType != null && data.get(position).getFilePath() != null) {
            if (data.get(position).getFilePath().contains("pdf")) {
                holder.iVDocType.setImageResource(R.drawable.pdf);
            } else {
                holder.iVDocType.setImageResource(R.drawable.ic_type_image);
            }
        }
        holder.tVReportName.setText(data.get(position).getReportType());
        holder.tVReportTime.setText(sdf.format(new Date(Long.parseLong(data.get(position).getUploadedTime()) * 1000)));
        holder.rLViewReport.setOnClickListener(v -> {
            Intent intent = new Intent(context, FullscreenActivity.class);
            intent.putExtra("ReportImage", baseUrl + data.get(position).getFilePath());
            context.startActivity(intent);
        });
        holder.rLDownloadReport.setOnClickListener(v -> Dexter.withContext(context).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                MyAsyncTask asyncTask = new MyAsyncTask();
                asyncTask.execute(baseUrl + data.get(position).getFilePath());
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                if (permissionDeniedResponse.isPermanentlyDenied()) {
                    Toast.makeText(context, "You'll need to allow us to write to storage from the phone settings.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Can't download file without permission.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).onSameThread().check());

//         holder.reportName.setText(apsList.get(position).getRtName());
//        long miliSec = Long.parseLong(apsList.get(position).getCreatedDatetime());
//        long timestamp = Long.parseLong(apsList.get(position).getCreatedDatetime());
//        String date = GetHumanReadableDate(timestamp, "MM-dd-yyyy HH:mm aa");
//        holder.reportTime.setText(date);
//        holder.view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, FullscreenActivity.class);
//                intent.putExtra("ReportImage", Prefs.getSharedPreferenceString(context, Prefs.PREF_UPLOADED_FILE_BASE_URL, "") + apsList.get(position).getLrPath());
//                context.startActivity(intent);
//            }
//        });
//
//        holder.download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    Dexter.withContext(context).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
//                        @Override
//                        public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
//                            MyAsyncTask asyncTask = new MyAsyncTask();
//                            asyncTask.execute(Prefs.getSharedPreferenceString(context, Prefs.PREF_UPLOADED_FILE_BASE_URL, "") + apsList.get(position).getLrPath());
//                        }
//
//                        @Override
//                        public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
//                            if(permissionDeniedResponse.isPermanentlyDenied()) {
//                                Toast.makeText(context, "You'll need to allow us to write to storage from the phone settings.", Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(context, "Can't download file without permission.", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//
//                        @Override
//                        public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
//                            permissionToken.continuePermissionRequest();
//                        }
//                    }).check();
//                } else {
//                    MyAsyncTask asyncTask = new MyAsyncTask();
//                    asyncTask.execute(Prefs.getSharedPreferenceString(context, Prefs.PREF_UPLOADED_FILE_BASE_URL, "") + apsList.get(position).getLrPath());
//                }
//            }
//        });
//        setAnimation(holder.itemView, position);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ItemHolder extends RecyclerView.ViewHolder {
        TextView tVReportName,tVReportTime;
        RelativeLayout rLViewReport, rLDownloadReport;
        ImageView iVDocType;

        ItemHolder(@NonNull View itemView) {
            super(itemView);
            iVDocType = itemView.findViewById(R.id.iv_docs_type);
            tVReportName = itemView.findViewById(R.id.report_name);
            tVReportTime = itemView.findViewById(R.id.report_time);
            rLViewReport = itemView.findViewById(R.id.btn_view_report);
            rLDownloadReport = itemView.findViewById(R.id.btn_download_report);
        }
    }

    class MyAsyncTask extends AsyncTask<String, Void, Void> {

        ProgressDialog mProgressDialog = new ProgressDialog(context);
        File file;
        String imageName;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.setMessage("Downloading");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMax(100);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

            int NOTIFICATION_ID = 234;

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            String CHANNEL_ID = "my_channel_01";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                CharSequence name = "File downloads";
                String Description = "This channel notifies you about your downloads";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mChannel.setDescription(Description);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.RED);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mChannel.setShowBadge(false);
                notificationManager.createNotificationChannel(mChannel);
            }

            builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("File download")
                    .setOngoing(true)
                    .setContentText("Downloading report");
            builder.setProgress(100, progress, false);


            notificationManager.notify(NOTIFICATION_ID, builder.build());



        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            builder.setProgress(100, progress, false);
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                String imagePath = params[0];
                imageName = imagePath.substring(imagePath.indexOf("reports")).replace("reports/", "");
                URL url = new URL(imagePath);
                //create the new connection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                //set up some things on the connection
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                //and connect!
                urlConnection.connect();
                //set the path where we want to save the file in this case, going to save it on the root directory of the sd card.
                File SDCardRoot = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                //create a new file, specifying the path, and the filename which we want to save the file as.
                file = new File(SDCardRoot, imageName);
                //this will be used to write the downloaded data into the file we created
                FileOutputStream fileOutput = new FileOutputStream(file);
                //this will be used in reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();
                //this is the total size of the file
                int totalSize = urlConnection.getContentLength();
                //variable to store total downloaded bytes
                long downloadedSize = 0;
                progress = (int) downloadedSize;
                byte[] buffer = new byte[1024];
                int bufferLength = 0; //used to store a temporary size of the buffer
                //now, read through the input buffer and write the contents to the file
                while ((bufferLength = inputStream.read(buffer)) != -1) {
                    //add the data in the buffer to the file in the file output stream (the file on the sd card
                    fileOutput.write(buffer, 0, bufferLength);
                    //add up the size so we know how much is downloaded
                    downloadedSize += bufferLength;
                    progress = (int)(downloadedSize * 100 / totalSize);
                    builder.setProgress(100, progress, false);
                    publishProgress();
//                    publishProgress((int)(downloadedSize * 100 / totalSize));
//                    fileOutput.write(buffer, 0, bufferLength);
                    //this is where you would do something to report the prgress, like this maybe
                    //updateProgress(downloadedSize, totalSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("File download", e.getMessage());
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            builder.setProgress(0, 0, false);
            mProgressDialog.dismiss();
            Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show();

            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1);
            String type = mime.getMimeTypeFromExtension(ext);

            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri contentUri = FileProvider.getUriForFile(context, "com.comlinkinc.medicus.fileProvider", file);
                intent.setDataAndType(contentUri, type);
            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
            }

            int NOTIFICATION_ID = 234;

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            String CHANNEL_ID = "my_channel_01";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                CharSequence name = "File Downloads";
                String Description = "This channel notifies you about your downloads";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mChannel.setDescription(Description);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.GREEN);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mChannel.setShowBadge(false);
                notificationManager.createNotificationChannel(mChannel);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("File download")
                    .setContentText("Downloaded report.");


            notificationManager.notify(NOTIFICATION_ID, builder.build());


        }
    }
}