package com.comlinkinc.medicus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.responses.doctorDetails.SpecialityDetailsResponse;
import com.comlinkinc.medicus.activities.DoctorDetailsActivity;
import com.comlinkinc.medicus.retrofit.models.pojo.ApsList.ApsList_;

import java.util.ArrayList;
import java.util.List;

import static com.comlinkinc.medicus.activities.DoctorDetailsActivity.ids;

public class SpecialityAdapter extends RecyclerView.Adapter<SpecialityAdapter.ItemHolder> {

    private static final String TAG = "SpecialityAdapter";
    Context context;
    List<ApsList_> apsList;
    List<SpecialityDetailsResponse> selected;
    List<String> selected1;
    onItemClickListner onItemClickListner;

    public SpecialityAdapter(DoctorDetailsActivity editDoctorProfileActivity, List<ApsList_> apsList, ArrayList<String> selected) {
        context = editDoctorProfileActivity;
        this.apsList = apsList;
        this.selected1 = selected;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_speciality, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.sp.setText(apsList.get(position).getSpName());
        for(String speciality: selected1) {
            if (speciality.equals(apsList.get(position).getSpId())) {
                holder.sp.setChecked(true);
                break;
            }
        }

        holder.sp.setOnCheckedChangeListener((compoundButton, b) -> {
            String id = apsList.get(position).getSpId();
            String name = apsList.get(position).getSpName();
            if (b) {
                ids.add(id);
            } else {
                for (int j = 0; j < ids.size(); j++) {
                    String id2 = ids.get(j);
                    if (apsList.get(position).getSpId().equals(id2)) {
                        ids.remove(id2);
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return apsList.size();
    }

    public interface onItemClickListner {
        void onClick(ArrayList<String> d);
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        CheckBox sp;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            sp = itemView.findViewById(R.id.sp);
        }
    }

}