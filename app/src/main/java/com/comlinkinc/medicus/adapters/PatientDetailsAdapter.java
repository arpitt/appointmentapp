package com.comlinkinc.medicus.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.comlinkinc.medicus.fragments.ChatDoctorFragment;
import com.comlinkinc.medicus.fragments.PatientTestsFragment;
import com.comlinkinc.medicus.fragments.ReportsFragment;
import com.comlinkinc.medicus.fragments.SubmittedQuestionnaireFragment;

public class PatientDetailsAdapter extends FragmentPagerAdapter {

    private Fragment fragment;
    private String pid, email, roomId, roomName;


    public PatientDetailsAdapter(@NonNull FragmentManager fm, int behavior, String user, String email, String roomName, String roomId) {
        super(fm, behavior);
        this.pid = user;
        this.email = email;
        this.roomName = roomName;
        this.roomId = roomId;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragment = new PatientTestsFragment(pid);
                break;

            case 1:
                fragment = new ReportsFragment(pid);
                break;

            case 2:
                fragment = new SubmittedQuestionnaireFragment(pid);
                break;

            case 3:
                fragment = new ChatDoctorFragment(pid, email, roomName, roomId);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
