package com.comlinkinc.medicus.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.comlinkinc.medicus.R;
import com.rocketchat.core.model.SubscriptionObject;



import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ChatMembersAdapter extends ArrayAdapter<SubscriptionObject> {
    private List<SubscriptionObject> members;
    private Context context;
    private Random rnd;


    public ChatMembersAdapter(Context context, List<SubscriptionObject> members, Random rnd) {
        super(context, 0, members);
        this.context = context;
        this.members = members;
        this.rnd = rnd;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item
        SubscriptionObject user = getItem(position);
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        // Check if an existing view is being reused, otherwise inflate the view
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_chat_members, parent, false);
        }
        // Populate the data into the template view using the data object
        TextView tvContactInitial = view.findViewById(R.id.ivContactImage);
        TextView tvContactName = view.findViewById(R.id.tvContactName);
        TextView tvContactStatus = view.findViewById(R.id.tvContactStatus);
        TextView tvMsgTime = view.findViewById(R.id.tvMsgTime);
        TextView tvUnreadCount = view.findViewById(R.id.tvUnreadCount);
        LinearLayout tunreadCountLL = view.findViewById(R.id.unread_count_ll);

        tvContactName.setText(user.getRoomName());


        String shortName = "";
        if (user.getRoomName() != null && !user.getRoomName().equals("")) {
            if (user.getRoomName().length() == 1) {
                shortName = (user.getRoomName().toUpperCase());
            } else if (user.getRoomName().length() > 1) {
                shortName = (user.getRoomName().substring(0, 1).toUpperCase());
            }
            tvContactInitial.setText(shortName);
        }

        GradientDrawable gd = new GradientDrawable();
        gd.setColor(color);
        gd.setCornerRadius(20);
        tvContactInitial.setBackgroundDrawable(gd);

        if (user.getUpdatedAt() != null) {
            String datem = (user.getUpdatedAt().toString());
            DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            // DateFormat dateFormat= new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            Date date = null;
            try {
                date = formatter.parse(datem);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(date);

            SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            String finalDate = timeFormat.format(date);
            tvContactStatus.setText(finalDate);
        }


        if (user.getUnread() > 0) {
            tunreadCountLL.setVisibility(View.VISIBLE);
            tvUnreadCount.setText(user.getUnread() + "");
        } else {
            tunreadCountLL.setVisibility(View.GONE);
        }
        return view;
    }
}