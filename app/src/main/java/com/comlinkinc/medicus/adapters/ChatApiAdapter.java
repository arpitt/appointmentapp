package com.comlinkinc.medicus.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.FullscreenActivity;
import com.comlinkinc.medicus.utils.ItemClickListener;
import com.comlinkinc.medicus.utils.MainActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Attachment;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Message;
import com.comlinkinc.medicus.utils.Constants;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.comlinkinc.medicus.utils.Constants.isFromChatAdapterClick;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  16 Feb 2018
 ***********************************************************************/

public class ChatApiAdapter extends ArrayAdapter<Message> {
    private Context context;
    private List<Message> messageList;
    private ItemClickListener clickListener;
    private String uploadedMediaPath = "";
    private SharedPreferences sharedpreferences;
    private MediaController mediacontroller;
    private Uri uri;
    private String fileName;


    public ChatApiAdapter(Context context, List<Message> messageList) {
        super(context, 0, messageList);
        this.messageList = messageList;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_chat_history, parent, false);

            sharedpreferences = context.getSharedPreferences(Constants.COMLINK_PREFS, Context.MODE_PRIVATE);

            viewHolder = new ViewHolder();

            viewHolder.tv_username = convertView.findViewById(R.id.tv_username);
            viewHolder.tv_msg = convertView.findViewById(R.id.tv_msg);
            viewHolder.tv_userfirstchar = convertView.findViewById(R.id.tv_userfirstchar);
            viewHolder.tv_time = convertView.findViewById(R.id.tv_time);
            viewHolder.ivUploadedFile = convertView.findViewById(R.id.iv_uploaded_file);
            viewHolder.tv_desc = convertView.findViewById(R.id.tv_desc);
            viewHolder.tv_pinned_by_user = convertView.findViewById(R.id.txt_pinned_by_user);
            viewHolder.tv_pinned_msg = convertView.findViewById(R.id.txt_pinned_message);
            viewHolder.layoutPinnedMessage = convertView.findViewById(R.id.layout_pinned_msg);
            viewHolder.iv_star = convertView.findViewById(R.id.iv_star);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Message message = messageList.get(position);
        viewHolder.layoutPinnedMessage.setVisibility(View.GONE);
        viewHolder.iv_star.setVisibility(View.GONE);
        viewHolder.tv_desc.setVisibility(View.GONE);

        viewHolder.tv_username.setText(message.getU().getUsername());                                              //
        String shortName = "";
        if (message.getU().getUsername().length() == 1) {
            shortName = (message.getU().getUsername().toUpperCase());                                              //
        } else if (message.getU().getUsername().length() > 1) {
            shortName = (message.getU().getUsername().substring(0, 1).toUpperCase());                              //
        }
        viewHolder.tv_userfirstchar.setText(shortName);
        int color;
        if (!sharedpreferences.getString(Constants.PREFS_USER_COLOR, "").equals("")) {
            color = Integer.parseInt(sharedpreferences.getString(Constants.PREFS_USER_COLOR, ""));
        } else {
            color = context.getResources().getColor(R.color.appcolorPrimary);
        }

        String id = messageList.get(0).getU().getId();
        if (!id.equals(messageList.get(position).getU().getId())) {
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(color);
            gd.setCornerRadius(20);
            viewHolder.tv_userfirstchar.setBackgroundDrawable(gd);
        } else {
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(context.getResources().getColor(R.color.colorPrimary));
            gd.setCornerRadius(20);
            viewHolder.tv_userfirstchar.setBackgroundDrawable(gd);
        }

        viewHolder.tv_msg.setVisibility(View.VISIBLE);
        viewHolder.ivUploadedFile.setVisibility(View.GONE);
        viewHolder.ivUploadedFile.setTag(position);

        if (message.getTs() != null) {
            String datem = (message.getTs());                                                                         //

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'", Locale.getDefault());  // EEE MMM dd HH:mm:ss zzz yyyy
            SimpleDateFormat formatter1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.getDefault());  // EEE MMM dd HH:mm:ss zzz yyyy
            formatter.setTimeZone(TimeZone.getDefault());
            formatter1.setTimeZone(TimeZone.getDefault());
            Date date = new Date();
            try {
                date = formatter.parse(datem);
            } catch (ParseException e) {
//                e.printStackTrace();
                try {
                    date = formatter1.parse(datem);
                } catch (ParseException e1) {
//                    e1.printStackTrace();
                }
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy hh:mm aa", Locale.getDefault());
//            timeFormat.setTimeZone(TimeZone.getDefault());
            String finalDate = timeFormat.format(date);
            viewHolder.tv_time.setText(finalDate);
        }

        if (message.getMsg() != null) {
            Log.i("Message", "getView: " + message.getMsg());
            if (message.getAttachments() == null || message.getAttachments().size() == 0) {
                if (message.getMsg().contains("http")) {
                    if (message.getMsg().contains("mvoipctsi.com")) {
                        viewHolder.tv_msg.setTextColor(Color.parseColor("#7F7F7F"));
                        viewHolder.tv_msg.setText(Html.fromHtml("Hi you have received a video call. Please click on <font color=\"#0000EE\">Join Conference</font>"));
//                        viewHolder.tv_msg.setPaintFlags(viewHolder.tv_msg.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//                        viewHolder.tv_msg.setTextColor(Color.parseColor("#98AFC7"));
                    } else {
                        viewHolder.tv_msg.setText(message.getMsg());
                        viewHolder.tv_msg.setTextColor(Color.parseColor("#98AFC7"));
                    }
                } else {
                    viewHolder.tv_msg.setTextColor(Color.parseColor("#7F7F7F"));
                    viewHolder.tv_msg.setPaintFlags(0);
                    if (message.getT() != null) {
                        String userName = message.getU().getUsername();
                        if (message.getT().equalsIgnoreCase("au")) { //Add user by someone
                            viewHolder.tv_msg.setText("User " + message.getMsg() + " added by " + userName);
                        } else if (message.getT().equalsIgnoreCase("ru")) {// Remove user by someone
                            viewHolder.tv_msg.setText("User " + message.getMsg() + " removed by " + userName);
                        } else if (message.getT().equalsIgnoreCase("jitsi_call_started")) {
                            viewHolder.tv_msg.setText(Html.fromHtml("Hi you have received a video call. Please click on <font color=\"#0000EE\">Join Conference</font>"));
                            Log.i("Video call...", "getView: " + position + " " + message.getRid());
                        } else if (message.getT().equalsIgnoreCase("uj")) {
                            viewHolder.tv_msg.setText(message.getMsg() + " has joined the forum.");
                        } else if (message.getT().equalsIgnoreCase("ul")) {
                            viewHolder.tv_msg.setText(message.getMsg() + " has left the forum.");
                        } else if (message.getT().equalsIgnoreCase("r")) {
                            viewHolder.tv_msg.setText("Forum name changed by " + userName + "(" + message.getMsg() + ")");
                        } else if (message.getT().equalsIgnoreCase("room_changed_topic")) {
                            viewHolder.tv_msg.setText("Topic set by " + userName);
                        } else if (message.getT().equalsIgnoreCase("room_changed_description")) {
                            viewHolder.tv_msg.setText("Description set by " + userName);
                        } else {
                            viewHolder.tv_msg.setText(message.getMsg());
                        }
                    } else {
                        //Simple message
                        if (message.getAttachments() == null &&
                                message.getFile() == null &&
                                message.getStarred() == null &&
                                !message.getMsg().trim().equals("")) {

                            viewHolder.tv_msg.setText(message.getMsg());
                        }

                        //Pinned message
                        else if (message.getAttachments() != null &&
                                message.getAttachments().size() > 0 &&
                                message.getFile() == null &&
                                message.getStarred() == null &&
                                message.getMsg().trim().equals("") &&
                                message.getT().equals("message_pinned")) {

                            viewHolder.tv_msg.setText("Pinned a message:");
                            viewHolder.layoutPinnedMessage.setVisibility(View.VISIBLE);
                            viewHolder.tv_pinned_by_user.setText(message.getAttachments().get(0).getAuthorName());
                            viewHolder.tv_pinned_msg.setText(message.getAttachments().get(0).getText());
                        }

                        //Actual attachment (audio/video/image)
                        else if (message.getAttachments() != null &&
                                message.getAttachments().size() > 0 &&
                                message.getFile() != null &&
                                message.getStarred() == null &&
                                message.getMsg().trim().equals("") &&
                                message.getT() == null) {

                            List<Attachment> listAttachment = message.getAttachments();
                            String urlLink = null;

                            for (int i = 0; i < listAttachment.size(); i++) {
                                urlLink = listAttachment.get(i).getTitleLink();
                            }
                            if (message.getFile() != null) {
                                String url = "<font color='red'>" + Constants.MAIN_URL + urlLink + "</font>";
                                String fileUrl = Constants.MAIN_URL + urlLink;

                                //Uploaded IMAGE
                                if (message.getFile().getType().contains("image")) {
                                    viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                                    if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                        viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                        viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                                    } else {
                                        viewHolder.tv_desc.setVisibility(View.GONE);
                                    }
                                    uploadedMediaPath = fileUrl;
                                    viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                                    Picasso.get().load(fileUrl).error(R.drawable.app_logo_splash).into(viewHolder.ivUploadedFile);
                                }

                                //Uploaded AUDIO
                                else if (message.getFile().getType().contains("audio")) {
                                    viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                                    viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                                    if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                        viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                        viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                                    } else {
                                        viewHolder.tv_desc.setVisibility(View.GONE);
                                    }
                                    Picasso.get().load(R.drawable.audio).into(viewHolder.ivUploadedFile);
                                }

                                //Uploaded VIDEO
                                else if (message.getFile().getType().contains("video")) {
                                    viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                                    viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                                    if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                        viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                        viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                                    } else {
                                        viewHolder.tv_desc.setVisibility(View.GONE);
                                    }
                                    Picasso.get().load(R.drawable.video_thumbnail1).into(viewHolder.ivUploadedFile);
                                }
                            }
                        }

                        //Starred message message
                        else if (message.getAttachments() == null &&
                                message.getFile() == null &&
                                message.getStarred() != null &&
                                message.getT() == null &&
                                !message.getMsg().trim().equals("")) {

                            viewHolder.iv_star.setVisibility(View.VISIBLE);
                            viewHolder.tv_msg.setText(message.getMsg());
                        } else {
                            viewHolder.tv_msg.setText(message.getMsg());
                        }
                    }
                }
            } else {

                //Simple message
                if (message.getAttachments() == null &&
                        message.getFile() == null &&
                        message.getStarred() == null &&
                        !message.getMsg().trim().equals("")) {

                    viewHolder.tv_msg.setText(message.getMsg());
                }

                //Pinned message
                else if (message.getAttachments() != null &&
                        message.getAttachments().size() > 0 &&
                        message.getFile() == null &&
                        message.getStarred() == null &&
                        message.getMsg().trim().equals("") &&
                        message.getT().equals("message_pinned")) {

                    viewHolder.tv_msg.setText("Pinned a message:");
                    viewHolder.layoutPinnedMessage.setVisibility(View.VISIBLE);
                    viewHolder.tv_pinned_by_user.setText(message.getAttachments().get(0).getAuthorName());
                    viewHolder.tv_pinned_msg.setText(message.getAttachments().get(0).getText());
                }

                //Actual attachment (audio/video/image)
                else if (message.getAttachments() != null &&
                        message.getAttachments().size() > 0 &&
                        message.getFile() != null &&
                        message.getStarred() == null &&
                        message.getMsg().trim().equals("") &&
                        message.getT() == null) {

                    List<Attachment> listAttachment = message.getAttachments();
                    String urlLink = null;

                    for (int i = 0; i < listAttachment.size(); i++) {
                        urlLink = listAttachment.get(i).getTitleLink();
                    }
                    if (message.getFile() != null) {
                        String url = "<font color='red'>" + Constants.MAIN_URL + urlLink + "</font>";
                        String fileUrl = Constants.MAIN_URL + urlLink;

                        //Uploaded IMAGE
                        if (message.getFile().getType().contains("image")) {
                            viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                            if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                            } else {
                                viewHolder.tv_desc.setVisibility(View.GONE);
                            }
                            uploadedMediaPath = fileUrl;
                            viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                            Picasso.get().load(fileUrl).error(R.drawable.app_logo_splash).into(viewHolder.ivUploadedFile);
                        }

                        //Uploaded AUDIO
                        else if (message.getFile().getType().contains("audio")) {
                            viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                            viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                            if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                            } else {
                                viewHolder.tv_desc.setVisibility(View.GONE);
                            }
                            Picasso.get().load(R.drawable.audio).into(viewHolder.ivUploadedFile);
                        }

                        //Uploaded VIDEO
                        else if (message.getFile().getType().contains("video")) {
                            viewHolder.ivUploadedFile.setVisibility(View.VISIBLE);
                            viewHolder.tv_msg.setText("" + listAttachment.get(0).getTitle());
                            if (listAttachment.get(0).getDescription() != null && !listAttachment.get(0).getDescription().equals("")) {
                                viewHolder.tv_desc.setVisibility(View.VISIBLE);
                                viewHolder.tv_desc.setText(">> " + listAttachment.get(0).getDescription());
                            } else {
                                viewHolder.tv_desc.setVisibility(View.GONE);
                            }
                            Picasso.get().load(R.drawable.video_thumbnail1).into(viewHolder.ivUploadedFile);
                        }
                    }
                }

                //Starred message message
                else if (message.getAttachments() == null &&
                        message.getFile() == null &&
                        message.getStarred() != null &&
                        message.getT() == null &&
                        !message.getMsg().trim().equals("")) {

                    viewHolder.iv_star.setVisibility(View.VISIBLE);
                    viewHolder.tv_msg.setText(message.getMsg());
                }
            }
        }


        viewHolder.ivUploadedFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer imagePos = (Integer) v.getTag();

                String uploadedFileType = messageList.get(imagePos).getFile().getType();
                fileName = messageList.get(imagePos).getAttachments().get(0).getTitle();

                String selectedImgUrl = Constants.MAIN_URL + messageList.get(imagePos).getAttachments().get(0).getTitleLink();
                Intent intent = new Intent(MyApplication.getInstance(), FullscreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.UPLOADED_MEDIA_PATH, selectedImgUrl);
                intent.putExtra(Constants.UPLOADED_MEDIA_TYPE, uploadedFileType);
                intent.putExtra(Constants.UPLOADED_MEDIA_FILE_NAME, fileName);
                MyApplication.getInstance().startActivity(intent);
            }
        });

        viewHolder.tv_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromChatAdapterClick = true;
                if (!message.getMsg().equals("")) {
                    if (message.getMsg().contains("http")) {
                        if (message.getMsg().contains("mvoipctsi.com")) {
                            Intent intent = new Intent(context, MainActivity.class);
                            if (message.getMsg().contains("*[Join Conference]")) {
                                intent.putExtra(Constants.INTENT_VIDEO_URL, message.getMsg().substring(message.getMsg().indexOf("(") + 1, message.getMsg().indexOf(")")));
                            } else {
                                intent.putExtra(Constants.INTENT_VIDEO_URL, message.getMsg());
                            }
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        } else {
                            Uri uri = Uri.parse(message.getMsg());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    }
                } else { // video call from Web
                    if (message.getT() != null && message.getT().equalsIgnoreCase("jitsi_call_started")) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(Constants.INTENT_VIDEO_URL, "https://newvideo.mvoipctsi.com/comlinkHznA3eWAhRcAoYemQ" + message.getRid()); // + sharedpreferences.getString(Constants.PREFS_USER_ID, "")
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        MyApplication.getInstance().startActivity(intent);
                    }
                }
            }
        });
        return convertView;
    }

    static class ViewHolder {
        private TextView tv_username, tv_msg, tv_userfirstchar, tv_time;
        public TextView tv_desc;
        private ImageView ivUploadedFile, iv_star;
        private LinearLayout layoutPinnedMessage;
        public TextView tv_pinned_by_user, tv_pinned_msg;
    }

}
