package com.comlinkinc.medicus.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DoctorDashboardActivity;
import com.comlinkinc.medicus.activities.PatientDetailsActivity;
import com.comlinkinc.medicus.retrofit.models.pojo.ForeGroundCount.Update;
import com.comlinkinc.medicus.retrofit.models.pojo.PatientList.DrPtList;
import com.comlinkinc.medicus.utils.Constants;
import com.rocketchat.core.model.SubscriptionObject;

import java.util.Calendar;
import java.util.List;

public class PatientListAdapter extends RecyclerView.Adapter<PatientListAdapter.ItemHolder> {

    private static final String TAG = "PatientListAdapter";
    private Context context;
    private List<DrPtList> drPtLists;
    private List<SubscriptionObject> subscribedMembers;

    public PatientListAdapter(DoctorDashboardActivity doctorDashboardActivity, List<DrPtList> drPtList, List<SubscriptionObject> subscribedMembers) {
        context = doctorDashboardActivity;
        drPtLists = drPtList;
        this.subscribedMembers = subscribedMembers;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_patient_list, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        Glide.with(context).load(Constants.MAIN_URL + "/" + drPtLists.get(position).getPPpPath()).placeholder(R.drawable.profile).error(R.drawable.profile).into(holder.avatarImage);
        holder.username.setText(drPtLists.get(position).getPName());
        holder.email.setText(drPtLists.get(position).getPEmail());
        holder.pid.setText(drPtLists.get(position).getPid());
        if (drPtLists.get(position).getPPhone() == null || (drPtLists.get(position).getPPhone() != null && drPtLists.get(position).getPPhone().equals(""))) {
            holder.phone.setVisibility(View.GONE);
        } else {
            holder.phone.setText(drPtLists.get(position).getPPhone());
        }
        String comLinkEmail = drPtLists.get(position).getPEmail().replace("@", "_");
        for (SubscriptionObject obj : subscribedMembers) {
            if (obj.getRoomName().equals(comLinkEmail)) {
                holder.count.setVisibility(View.VISIBLE);
                holder.count.setText(String.valueOf(obj.getUnread()));
                break;
            }
        }
        if (holder.count.getText().toString().equals("0")) {
            holder.count.setVisibility(View.GONE);
        }
        String gender;
        if (drPtLists.get(position).getGender().equals("1")) {
            gender = "Gender: Male";
        } else {
            gender = "Gender: Female";
        }
        holder.gender.setText(gender);
        String[] date = drPtLists.get(position).getDob().split("-");
        getAge(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]), holder.age);
    }

    @Override
    public int getItemCount() {
        return drPtLists.size();
    }

    private void getAge(int year, int month, int day, TextView age1) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        age1.setText("Age: ".concat(String.valueOf(age)));
    }

    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView avatarImage;
        TextView username, age, email, phone, gender, pid, count;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            avatarImage = itemView.findViewById(R.id.avatar_image);
            username = itemView.findViewById(R.id.username);
            age = itemView.findViewById(R.id.age);
            count = itemView.findViewById(R.id.count);
            pid = itemView.findViewById(R.id.pid);
            gender = itemView.findViewById(R.id.gender);
            email = itemView.findViewById(R.id.email);
            phone = itemView.findViewById(R.id.phoneno);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent in = new Intent(context, PatientDetailsActivity.class);
            in.putExtra("pid", drPtLists.get(getAdapterPosition()).getPid());
            in.putExtra("email", drPtLists.get(getAdapterPosition()).getPEmail());
            context.startActivity(in);
        }
    }
}