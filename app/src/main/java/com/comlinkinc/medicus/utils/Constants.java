package com.comlinkinc.medicus.utils;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.comlinkinc.medicus.responses.login.UserDetails;
import com.rocketchat.core.RocketChatAPI;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public class Constants {

    public static final String ADMIN = "a";
    public static final String PATIENT = "p";
    public static final String DOCTOR = "d";
    public static final String LABORATORY = "l";
    //Error code
    public static final int ERROR_CODE_200 = 200;
    public static final String ROCKET_CHAT_URL = "https://telemedicus.mvoipctsi.com/";
    public static final String MAIN_URL = "https://medicus.mvoipctsi.com";
    public static final String BASE_URL = "https://medicus.mvoipctsi.com/api/v1/";
    public static final String COMLINK_PREFS = "ComlinkPrefs";
    public static final String PREFS_LOGIN_USER_NAME = "LoginUserName";
    public static final String PREFS_USER_ID = "UserId";
    public static final String PREFS_AUTH_TOKEN = "AuthToken";
    public static final String ROOM_ID = "AuthToken";

    public static final String PREFS_ROOM_ID = "RoomId";
    public static final String PREFS_PEER_USER = "PeerUser";
    public static final String PREFS_PSWD = "PrefsPswd";
    public static final String PREFS_UNAME = "PrefsUname";
    public static final String INTENT_VIDEO_URL = "IntentVideoUrl";
    public static final String UPLOADED_MEDIA_PATH = "UploadeMediaPath";
    public static final String UPLOADED_MEDIA_TYPE = "UploadeMediaType";
    public static final String UPLOADED_MEDIA_FILE_NAME = "UploadeMediaFilename";
    public static final String PREFS_USER_NAME = "PeerUserName";
    public static final String PREFS_READ_ONLY_GROUP = "ReadOnlyGroup";
    // User presence status
    public static final String ONLINE = "Online";
    public static final String OFFLINE = "Offline";
    public static final String AWAY = "Away";
    public static final String BUSY = "Busy";
    //User square view color
    public static final String PREFS_USER_COLOR = "UserColor";
    public static String XAuthToken = "";
    public static String XUserId = "";
    public static String userType = "";
    public static String chatUserId = "";
    public static String chatAuthToken = "";
    public static UserDetails userDetails = null;
    public static String TEXT_FOR_200 = "Success";
    public static RocketChatAPI client = null;
    public static Boolean isFromGroupChat = false;
    public static Boolean isFromSearchView = false;
    public static Boolean isFromPeerChat = false;
    public static Boolean isFromChatMembersFragment = false;
    public static Boolean isFromGroupsListFragment = false;
    public static Boolean isFromChatAdapterClick = false;
    public static Boolean isLivePhotoUpload = false;
    public static Boolean isLiveVideoUpload = false;
    public static Boolean isImageUpload = false;
    public static Boolean isAudioUpload = false;
    public static Boolean isVidioUpload = false;


    public static String convertDate(String oldDate, String oldFormat, String newFormat) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(oldFormat);
        Date date = format.parse(oldDate);
        format = new SimpleDateFormat(newFormat);
        return format.format(date);
    }

}
