package com.comlinkinc.medicus.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.comlinkinc.medicus.R;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  19 Jan 2018
 ***********************************************************************/

public class CommonUtils {


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean canReachServers() {
        final boolean[] isConnected = {false};
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InetAddress ipAddress = InetAddress.getByName("www.google.com");
                    isConnected[0] = ipAddress.isReachable(5000);
                } catch (Exception e) {
                    isConnected[0] = false;
                }
            }
        }).start();
        return isConnected[0];
    }

    public static boolean canReachServers(final Activity activity) {
        try {
            InetAddress ipAddr = InetAddress.getByName("www.google.com");
            return ipAddr.isReachable(5000);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean failureMessage(Context mContext) {
        boolean isInternetConnected = canReachServers();
        if (isInternetConnected) {
            Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
        } else {
            new AlertDialog.Builder(mContext, R.style.AlertDialogTheme)
                    .setTitle("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> dialog.dismiss())
                    .setIcon(R.drawable.no_internet)
                    .show();
        }
        return isInternetConnected;
    }

    public static boolean failureMessage(Context mContext, boolean showDialog) {
        boolean isInternetConnected = canReachServers();
        if (isInternetConnected) {
            Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
        } else {
            if(showDialog)
                new AlertDialog.Builder(mContext, R.style.AlertDialogTheme)
                        .setTitle("No Internet Connection")
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, (dialog, which) -> dialog.dismiss())
                        .setIcon(R.drawable.no_internet)
                        .show();
        }
        return isInternetConnected;
    }

    public static void showAlertDialog(Context mContext, String title, String message) {
        new AlertDialog.Builder(mContext, R.style.AlertDialogTheme)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> dialog.dismiss())
                .show();
    }

}
