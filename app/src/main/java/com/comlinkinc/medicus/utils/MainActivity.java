/*
 * Copyright @ 2017-present Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.comlinkinc.medicus.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetView;
import org.jitsi.meet.sdk.JitsiMeetViewListener;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * The one and only {@link Activity} that the Jitsi Meet app needs. The
 * {@code Activity} is launched in {@code singleTask} mode, so it will be
 * created upon application initialization and there will be a single instance
 * of it. Further attempts at launching the application once it was already
 * launched will result in {@link Activity#onNewIntent(Intent)} being called.
 * <p>
 * This {@code Activity} extends {@link JitsiMeetActivity} to keep the React
 * Native CLI working, since the latter always tries to launch an
 * {@code Activity} named {@code MainActivity} when doing
 * {@code react-native run-android}.
 */
public class MainActivity extends JitsiMeetActivity {
    URL url;
    private String receivedUrl = "";

    protected JitsiMeetView initializeView() {
        JitsiMeetView view = new JitsiMeetView(this);
        try {
            if (receivedUrl != null && !receivedUrl.equals("")) {
                url = new URL(receivedUrl);
            } else {
                url = new URL("https://newvideo.mvoipctsi.com/comlinkHznA3eWAhRcAoYemQ" + getIntent().getStringExtra(Constants.ROOM_ID));
            }

            JitsiMeetConferenceOptions options = new JitsiMeetConferenceOptions.Builder().setWelcomePageEnabled(false).setRoom(url.toString()).build();
            view.join(options);
            view.setListener(new JitsiMeetViewListener() {
                private void on(String name, Map<String, Object> data) {
                    Log.d("ReactNative: ", JitsiMeetViewListener.class.getSimpleName() + " " + name + " " + data);
                }

                @Override
                public void onConferenceJoined(Map<String, Object> data) {
                    on("CONFERENCE_JOINED", data);
                }

                @Override
                public void onConferenceTerminated(Map<String, Object> data) {
                    on("CONFERENCE_TERMINATED", data);
                    onBackPressed();
                }

                @Override
                public void onConferenceWillJoin(Map<String, Object> data) {
                    on("CONFERENCE_WILL_JOIN", data);
                }

//                @Override
//                public void onConferenceWillLeave(Map<String, Object> data) {
//                    on("CONFERENCE_WILL_LEAVE", data);
////                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
////                    //                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                    startActivity(intent);
//                    sharedpreferences = getSharedPreferences(Constants.COMLINK_PREFS, Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    editor.putString(Constants.INTENT_VIDEO_URL, "");
//                    editor.apply();
//                    finish();
//                }
//
//                @Override
//                public void onLoadConfigError(Map<String, Object> data) {
//                    on("LOAD_CONFIG_ERROR", data);
//                }
            });
//                runOnUiThread(() -> {
//                    if (!Constants.isFromNotification) {
//                        createMessage(url.toString());
//                    }
//                });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //                Toast.makeText(this, url.toString(), Toast.LENGTH_SHORT).show();

        //                if (room != null) {
        //                    room.sendMessage(url.toString());
        //                }
        return view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // As this is the Jitsi Meet app (i.e. not the Jitsi Meet SDK), we do
        // want the Welcome page to be enabled. It defaults to disabled in the
        // SDK at the time of this writing but it is clearer to be explicit
        // about what we want anyway.


        if (getIntent() != null) {
            receivedUrl = getIntent().getStringExtra(Constants.INTENT_VIDEO_URL);
        }

        if (receivedUrl == null) {
            Bundle notificationBundle = getIntent().getExtras();
            if (notificationBundle != null) {
                receivedUrl = notificationBundle.getString("link");
            }
        }
        setContentView(initializeView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
