package com.comlinkinc.medicus.utils;

public interface OnBackPressed {
    void onBackPressed();
}