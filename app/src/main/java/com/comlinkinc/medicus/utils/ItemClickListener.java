package com.comlinkinc.medicus.utils;

import android.view.View;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface ItemClickListener {
    void onClick(View view, int position);
}
