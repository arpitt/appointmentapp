package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.channelinfo.ChannelInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by sachink on 1/26/2018.
 */

public interface ChannelInfoInterface {
    @GET("channels.info")
    Call<ChannelInfoResponse> getChannelInfo(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Query("roomId") String roomId);
}
