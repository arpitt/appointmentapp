
package com.comlinkinc.medicus.retrofit.models.pojo.createmessage;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("parseUrls")
    @Expose
    private Boolean parseUrls;
    @SerializedName("groupable")
    @Expose
    private Boolean groupable;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("u")
    @Expose
    private U u;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("mentions")
    @Expose
    private List<Object> mentions = null;
    @SerializedName("channels")
    @Expose
    private List<Object> channels = null;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getParseUrls() {
        return parseUrls;
    }

    public void setParseUrls(Boolean parseUrls) {
        this.parseUrls = parseUrls;
    }

    public Boolean getGroupable() {
        return groupable;
    }

    public void setGroupable(Boolean groupable) {
        this.groupable = groupable;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public List<Object> getMentions() {
        return mentions;
    }

    public void setMentions(List<Object> mentions) {
        this.mentions = mentions;
    }

    public List<Object> getChannels() {
        return channels;
    }

    public void setChannels(List<Object> channels) {
        this.channels = channels;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
