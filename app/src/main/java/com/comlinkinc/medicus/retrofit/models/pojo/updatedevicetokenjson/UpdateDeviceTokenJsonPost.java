package com.comlinkinc.medicus.retrofit.models.pojo.updatedevicetokenjson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 1/11/2018.
 */

public class UpdateDeviceTokenJsonPost {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
