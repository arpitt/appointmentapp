package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.ResetDoctorPassword.ResetDoctorPassword;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ResetDoctorPasswordInterface {

 @FormUrlEncoded
 @POST("change_password")
    Call<ResetDoctorPassword> resetPass(@Field("pid") String pid, @Field("op") String op, @Field("np") String np, @Field("ut") String ut);

}
