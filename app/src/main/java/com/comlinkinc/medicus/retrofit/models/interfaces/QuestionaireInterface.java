package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.Questionaire.PatientQuestianare;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface QuestionaireInterface {

    @FormUrlEncoded
    @POST("pt_response")
    Call<PatientQuestianare> getQuestions(@Field("pid") String pid);
}
