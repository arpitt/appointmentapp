package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.Registration.Registration;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  25 Dec 2017
 ***********************************************************************/

public interface RegistrationInterface {
  @FormUrlEncoded
  @POST("users.register")
    Call<Registration> postRegistration(@Field("username") String username,
                                        @Field("email") String email,
                                        @Field("pass") String password,
                                        @Field("name") String name,
                                        @Field("devicetoken") String tkn);
}
