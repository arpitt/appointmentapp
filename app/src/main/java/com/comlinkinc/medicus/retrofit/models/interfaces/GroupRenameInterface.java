package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.grouprename.GroupRenameResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by sachink on 1/26/2018.
 */

public interface GroupRenameInterface {
    @FormUrlEncoded
    @POST("groups.rename")
    Call<GroupRenameResponse> renameGroup(@Header("X-Auth-Token") String auth_token,
                                          @Header("X-User-Id") String user_id,
                                          @Field("roomId") String roomId,
                                          @Field("name") String newGroupName);
}
