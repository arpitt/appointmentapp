package com.comlinkinc.medicus.retrofit.models.pojo.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Token {

@SerializedName("gcm")
@Expose
private String gcm;

public String getGcm() {
return gcm;
}

public void setGcm(String gcm) {
this.gcm = gcm;
}

}