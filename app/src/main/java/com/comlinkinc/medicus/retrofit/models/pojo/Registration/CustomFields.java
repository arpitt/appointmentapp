package com.comlinkinc.medicus.retrofit.models.pojo.Registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 12/21/2017.
 */

public class CustomFields {

    @SerializedName("devicetoken")
    @Expose
    private Object devicetoken;

    public Object getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(Object devicetoken) {
        this.devicetoken = devicetoken;
    }
}
