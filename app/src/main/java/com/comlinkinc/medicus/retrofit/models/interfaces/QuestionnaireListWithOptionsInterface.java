package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions.GetListOfQuestionnarieWithOptions;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public interface QuestionnaireListWithOptionsInterface {
    @FormUrlEncoded
    @POST("mapi/v1/questionnaire")
    Call<GetListOfQuestionnarieWithOptions> getAllQuestionsWithOptions(@Header("X-Auth-Token") String authId, @Header("X-User-Id") String userId, @Field("pid") String patientId);
}
