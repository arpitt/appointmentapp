package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.userinfo.GetUserInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by sachink on 2/7/2018.
 */

public interface UserNameInterface {
    @GET("users.info")
    Call<GetUserInfoResponse> getUserName(@Header("X-Auth-Token") String auth_token,
                                          @Header("X-User-Id") String user_id,
                                          @Query("userId") String userId);
}

