package com.comlinkinc.medicus.retrofit.models.pojo.channelfiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  10 Jul 2018
 ***********************************************************************/

public class Identify {
    @SerializedName("format")
    @Expose
    private String format;
    @SerializedName("size")
    @Expose
    private Size size;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

}
