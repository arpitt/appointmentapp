package com.comlinkinc.medicus.retrofit.models.pojo.userinforesponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sachink on 1/26/2018.
 */

public class Email_ {
    @SerializedName("verificationTokens")
    @Expose
    private List<VerificationToken> verificationTokens = null;

    public List<VerificationToken> getVerificationTokens() {
        return verificationTokens;
    }

    public void setVerificationTokens(List<VerificationToken> verificationTokens) {
        this.verificationTokens = verificationTokens;
    }

}
