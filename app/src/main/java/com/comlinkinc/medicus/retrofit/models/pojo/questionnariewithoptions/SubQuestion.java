
package com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubQuestion implements Parcelable {

    @SerializedName("q_id")
    @Expose
    private String qId;
    @SerializedName("pq_id")
    @Expose
    private String pqId;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("qt_id")
    @Expose
    private String qtId;
    @SerializedName("test_name")
    @Expose
    private String testName;
    @SerializedName("selected_qo_id")
    @Expose
    private String selectedQoId;
    @SerializedName("options")
    @Expose
    private List<Option_> options = null;

    @SerializedName("ans")
    @Expose
    private Object ans;

    public String getQId() {
        return qId;
    }

    public void setQId(String qId) {
        this.qId = qId;
    }

    public String getPqId() {
        return pqId;
    }

    public void setPqId(String pqId) {
        this.pqId = pqId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQtId() {
        return qtId;
    }

    public void setQtId(String qtId) {
        this.qtId = qtId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getSelectedQoId() {
        return selectedQoId;
    }

    public void setSelectedQoId(String selectedQoId) {
        this.selectedQoId = selectedQoId;
    }

    public List<Option_> getOptions() {
        return options;
    }

    public void setOptions(List<Option_> options) {
        this.options = options;
    }

    public Object getAns() {
        return ans;
    }

    public void setAns(Object ans) {
        this.ans = ans;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.qId);
        dest.writeString(this.pqId);
        dest.writeString(this.question);
        dest.writeString(this.qtId);
        dest.writeString(this.testName);
        dest.writeString(this.selectedQoId);
        dest.writeList(this.options);
        dest.writeParcelable((Parcelable) this.ans, flags);
    }

    public SubQuestion() {
    }

    protected SubQuestion(Parcel in) {
        this.qId = in.readString();
        this.pqId = in.readString();
        this.question = in.readString();
        this.qtId = in.readString();
        this.testName = in.readString();
        this.selectedQoId = in.readString();
        this.options = new ArrayList<Option_>();
        in.readList(this.options, Option_.class.getClassLoader());
        this.ans = in.readParcelable(Object.class.getClassLoader());
    }

    public static final Creator<SubQuestion> CREATOR = new Creator<SubQuestion>() {
        @Override
        public SubQuestion createFromParcel(Parcel source) {
            return new SubQuestion(source);
        }

        @Override
        public SubQuestion[] newArray(int size) {
            return new SubQuestion[size];
        }
    };
}
