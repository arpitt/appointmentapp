package com.comlinkinc.medicus.retrofit.models.interfaces;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jan 2018
 ***********************************************************************/


import com.comlinkinc.medicus.retrofit.models.pojo.starredmsg.StarredMsgListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


public interface MembersStarredMsgListInterface {
    @GET("im.messages")
    Call<StarredMsgListResponse> getMemberStarredMsgList(@Header("X-Auth-Token") String auth_token,
                                                         @Header("X-User-Id") String user_id,
                                                         @Query("roomId") String roomId,
                                                         @Query("query") String searchQueryRequest);
}
