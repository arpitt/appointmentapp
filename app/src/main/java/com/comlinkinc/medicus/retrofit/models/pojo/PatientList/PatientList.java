package com.comlinkinc.medicus.retrofit.models.pojo.PatientList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientList {

@SerializedName("p_list")
@Expose
private List<DrPtList> drPtList;
@SerializedName("status")
@Expose
private String status;

public List<DrPtList> getDrPtList() {
return drPtList;
}

public void setDrPtList(List<DrPtList> drPtList) {
this.drPtList = drPtList;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}