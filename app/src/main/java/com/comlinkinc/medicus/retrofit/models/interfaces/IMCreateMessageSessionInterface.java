package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.im.CreateMessageSessionBody;
import com.comlinkinc.medicus.retrofit.models.pojo.im.CreateMessageSessionResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface IMCreateMessageSessionInterface {
    @POST("im.create")
    Call<CreateMessageSessionResponse> createMessageSession(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Body CreateMessageSessionBody body);
}
