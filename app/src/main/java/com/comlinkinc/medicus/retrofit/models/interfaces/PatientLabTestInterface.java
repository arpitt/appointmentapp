package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.PatientLabTest.PatientLabTest;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PatientLabTestInterface {

    @FormUrlEncoded
    @POST("lab_test_list")
    Call<PatientLabTest> getLabTest(@Field("pid") String did);

}
