package com.comlinkinc.medicus.retrofit.models.interfaces;


 import com.comlinkinc.medicus.retrofit.models.pojo.notification.NotificationData;
 import com.comlinkinc.medicus.retrofit.models.pojo.notification.NotificationResponse;


 import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface SendPushNotificationInterface {
    @POST("send")
    Call<NotificationResponse> sendPushNotification(@Header("Content-Type") String content_type,
                                                    @Header("Authorization") String authorization,
                                                    @Body NotificationData notificationData);
}
