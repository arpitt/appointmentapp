
package com.comlinkinc.medicus.retrofit.models.pojo.chathistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Attachment {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_icon")
    @Expose
    private String authorIcon;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("attachments")
    @Expose
    private List<Object> attachments = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("title_link")
    @Expose
    private String titleLink;
    @SerializedName("title_link_download")
    @Expose
    private Boolean titleLinkDownload;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("video_type")
    @Expose
    private String videoType;
    @SerializedName("video_size")
    @Expose
    private Integer videoSize;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("image_type")
    @Expose
    private String imageType;
    @SerializedName("image_size")
    @Expose
    private Integer imageSize;
    @SerializedName("image_preview")
    @Expose
    private String imagePreview;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorIcon() {
        return authorIcon;
    }

    public void setAuthorIcon(String authorIcon) {
        this.authorIcon = authorIcon;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public List<Object> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Object> attachments) {
        this.attachments = attachments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitleLink() {
        return titleLink;
    }

    public void setTitleLink(String titleLink) {
        this.titleLink = titleLink;
    }

    public Boolean getTitleLinkDownload() {
        return titleLinkDownload;
    }

    public void setTitleLinkDownload(Boolean titleLinkDownload) {
        this.titleLinkDownload = titleLinkDownload;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public Integer getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(Integer videoSize) {
        this.videoSize = videoSize;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public Integer getImageSize() {
        return imageSize;
    }

    public void setImageSize(Integer imageSize) {
        this.imageSize = imageSize;
    }

    public String getImagePreview() {
        return imagePreview;
    }

    public void setImagePreview(String imagePreview) {
        this.imagePreview = imagePreview;
    }

}
