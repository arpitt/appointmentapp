
package com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable {

    @SerializedName("q_id")
    @Expose
    private String qId;
    @SerializedName("pq_id")
    @Expose
    private String pqId;
    @SerializedName("question")
    @Expose
    private String question;

    @SerializedName("qt_id")
    @Expose
    private String qtId;

    @SerializedName("test_name")
    @Expose
    private Object testName;
    @SerializedName("selected_qo_id")
    @Expose
    private String selectedQoId;

    @SerializedName("options")
    @Expose
    private List<Option> options = null;
    @SerializedName("ans")
    @Expose
    private Object ans;
    @SerializedName("sub_question")
    @Expose
    private List<SubQuestion> subQuestion = null;
    @SerializedName("qa_id")
    @Expose
    private String qaId;


    public String getQId() {
        return qId;
    }

    public void setQId(String qId) {
        this.qId = qId;
    }

    public String getPqId() {
        return pqId;
    }

    public void setPqId(String pqId) {
        this.pqId = pqId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQtId() {
        return qtId;
    }

    public void setQtId(String qtId) {
        this.qtId = qtId;
    }

    public Object getTestName() {
        return testName;
    }

    public void setTestName(Object testName) {
        this.testName = testName;
    }

    public String getSelectedQoId() {
        return selectedQoId;
    }

    public void setSelectedQoId(String selectedQoId) {
        this.selectedQoId = selectedQoId;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Object getAns() {
        return ans;
    }

    public void setAns(Object ans) {
        this.ans = ans;
    }

    public List<SubQuestion> getSubQuestion() {
        return subQuestion;
    }

    public void setSubQuestion(List<SubQuestion> subQuestion) {
        this.subQuestion = subQuestion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.qId);
        dest.writeString(this.pqId);
        dest.writeString(this.question);
        dest.writeString(this.qtId);
        dest.writeParcelable((Parcelable) this.testName, flags);
        dest.writeString(this.selectedQoId);
        dest.writeList(this.options);
        dest.writeParcelable((Parcelable) this.ans, flags);
        dest.writeList(this.subQuestion);
        dest.writeString(this.qaId);
    }

    public Datum() {
    }

    public String getqId() {
        return qId;
    }

    public void setqId(String qId) {
        this.qId = qId;
    }

    public String getQaId() {
        return qaId;
    }

    public void setQaId(String qaId) {
        this.qaId = qaId;
    }

    public static Creator<Datum> getCREATOR() {
        return CREATOR;
    }

    protected Datum(Parcel in) {

        this.qId = in.readString();
        this.pqId = in.readString();
        this.question = in.readString();
        this.qtId = in.readString();
        this.testName = in.readParcelable(Object.class.getClassLoader());
        this.selectedQoId = in.readString();
        this.options = new ArrayList<Option>();
        in.readList(this.options, Option.class.getClassLoader());
        this.ans = in.readParcelable(Object.class.getClassLoader());
        this.subQuestion = new ArrayList<SubQuestion>();
        in.readList(this.subQuestion, SubQuestion.class.getClassLoader());
        this.qaId = in.readString();
    }

    public static final Creator<Datum> CREATOR = new Creator<Datum>() {
        @Override
        public Datum createFromParcel(Parcel source) {
            return new Datum(source);
        }

        @Override
        public Datum[] newArray(int size) {
            return new Datum[size];
        }
    };
}
