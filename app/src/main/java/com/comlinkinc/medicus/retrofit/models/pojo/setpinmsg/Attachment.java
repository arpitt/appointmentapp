package com.comlinkinc.medicus.retrofit.models.pojo.setpinmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  25 Jun 2018
 ***********************************************************************/

public class Attachment {
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_icon")
    @Expose
    private String authorIcon;
    @SerializedName("ts")
    @Expose
    private String ts;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorIcon() {
        return authorIcon;
    }

    public void setAuthorIcon(String authorIcon) {
        this.authorIcon = authorIcon;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
