package com.comlinkinc.medicus.retrofit.models.pojo.updatedevicetokenjson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 1/11/2018.
 */

public class CustomFields {
    @SerializedName("devicetoken")
    @Expose
    private String devicetoken;

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }
}
