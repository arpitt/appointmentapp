package com.comlinkinc.medicus.retrofit.models.pojo.updatedevicetokenjson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by sachink on 1/11/2018.
 */

public class Data {
    @SerializedName("customFields")
    @Expose
    private CustomFields customFields;

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }
}

