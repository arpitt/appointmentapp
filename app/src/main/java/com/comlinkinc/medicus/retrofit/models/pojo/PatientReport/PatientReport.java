package com.comlinkinc.medicus.retrofit.models.pojo.PatientReport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientReport {

@SerializedName("base_url")
@Expose
private String baseUrl;
@SerializedName("code")
@Expose
private Integer code;
@SerializedName("data")
@Expose
private List<PatientReportList> data = null;

public String getBaseUrl() {
return baseUrl;
}

public void setBaseUrl(String baseUrl) {
this.baseUrl = baseUrl;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public List<PatientReportList> getData() {
return data;
}

public void setData(List<PatientReportList> data) {
this.data = data;
}

}