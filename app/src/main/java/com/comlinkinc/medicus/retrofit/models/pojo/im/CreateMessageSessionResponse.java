package com.comlinkinc.medicus.retrofit.models.pojo.im;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateMessageSessionResponse {

@SerializedName("room")
@Expose
private Room room;
@SerializedName("success")
@Expose
private Boolean success;

/**
* No args constructor for use in serialization
*
*/
public CreateMessageSessionResponse() {
}

/**
*
* @param success
* @param room
*/
public CreateMessageSessionResponse(Room room, Boolean success) {
super();
this.room = room;
this.success = success;
}

public Room getRoom() {
return room;
}

public void setRoom(Room room) {
this.room = room;
}

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

}