package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.setpinmsg.SetPinnedMsgResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by sachink on 1/26/2018.
 */

public interface SetStarMessageInterface {
    @FormUrlEncoded
    @POST("chat.starMessage")
    Call<SetPinnedMsgResponse> setStarMessage(@Header("X-Auth-Token") String auth_token,
                                              @Header("X-User-Id") String user_id,
                                              @Field("messageId") String messageId);
}
