
package com.comlinkinc.medicus.retrofit.models.pojo.chathistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.List;

public class Message {

    @SerializedName("_id")            //
    @Expose
    private String id;
    @SerializedName("rid")            //
    @Expose
    private String rid;
    @SerializedName("msg")            //
    @Expose
    private String msg;
    @SerializedName("ts")             //
    @Expose
    private String ts;

    @SerializedName("mentions")       //
    @Expose
    private List<Object> mentions = null;
    @SerializedName("channels")       //
    @Expose
    private List<Object> channels = null;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;        //
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("groupable")       //
    @Expose
    private Boolean groupable;

    @SerializedName("alias")           //
    @Expose
    private String alias;
    @SerializedName("parseUrls")           //
    @Expose
    private Boolean parseUrls;
    @SerializedName("bot")         //
    @Expose
    private Object bot;

    @SerializedName("pinned")         //
    @Expose
    private Boolean pinned;
    @SerializedName("pinnedAt")        //
    @Expose
    private String pinnedAt;         //
    @SerializedName("pinnedBy")
    @Expose
    private PinnedBy pinnedBy; //
    @SerializedName("file")
    @Expose
    private File file;                  //
    @SerializedName("starred")
    @Expose
    private List<Starred> starred;            //
    @SerializedName("u")                //
    @Expose
    private U u;
    @SerializedName("urls")     //
    @Expose
    private List<Url> urls = null;
    @SerializedName("attachments")
    @Expose
    private List<Attachment> attachments = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public List<Object> getMentions() {
        return mentions;
    }

    public void setMentions(List<Object> mentions) {
        this.mentions = mentions;
    }

    public List<Object> getChannels() {
        return channels;
    }

    public void setChannels(List<Object> channels) {
        this.channels = channels;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Boolean getGroupable() {
        return groupable;
    }

    public void setGroupable(Boolean groupable) {
        this.groupable = groupable;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Boolean getParseUrls() {
        return parseUrls;
    }

    public void setParseUrls(Boolean parseUrls) {
        this.parseUrls = parseUrls;
    }

    public Object getBot() {
        return bot;
    }

    public void setBot(Object bot) {
        this.bot = bot;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }

    public Boolean getPinned() {
        return pinned;
    }

    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    public String getPinnedAt() {
        return pinnedAt;
    }

    public void setPinnedAt(String pinnedAt) {
        this.pinnedAt = pinnedAt;
    }

    public PinnedBy getPinnedBy() {
        return pinnedBy;
    }

    public void setPinnedBy(PinnedBy pinnedBy) {
        this.pinnedBy = pinnedBy;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<Starred> getStarred() {
        return starred;
    }

    public void setStarred(List<Starred> starred) {
        this.starred = starred;
    }

}
