package com.comlinkinc.medicus.retrofit.models.pojo.setpinmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  25 Jun 2018
 ***********************************************************************/

public class SetPinnedMsgResponse {

    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
