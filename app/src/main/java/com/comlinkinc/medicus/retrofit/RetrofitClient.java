package com.comlinkinc.medicus.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public class RetrofitClient {
    public static Retrofit getClient(String baseUrl) {
        return new Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(new OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    }
}
