package com.comlinkinc.medicus.retrofit.models.pojo.pinnedmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jun 2018
 ***********************************************************************/

public class Message {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("u")
    @Expose
    private U u;
    @SerializedName("mentions")
    @Expose
    private List<Object> mentions = null;
    @SerializedName("channels")
    @Expose
    private List<Object> channels = null;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("pinned")
    @Expose
    private Boolean pinned;
    @SerializedName("pinnedAt")
    @Expose
    private String pinnedAt;
    @SerializedName("pinnedBy")
    @Expose
    private PinnedBy pinnedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public List<Object> getMentions() {
        return mentions;
    }

    public void setMentions(List<Object> mentions) {
        this.mentions = mentions;
    }

    public List<Object> getChannels() {
        return channels;
    }

    public void setChannels(List<Object> channels) {
        this.channels = channels;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getPinned() {
        return pinned;
    }

    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    public String getPinnedAt() {
        return pinnedAt;
    }

    public void setPinnedAt(String pinnedAt) {
        this.pinnedAt = pinnedAt;
    }

    public PinnedBy getPinnedBy() {
        return pinnedBy;
    }

    public void setPinnedBy(PinnedBy pinnedBy) {
        this.pinnedBy = pinnedBy;
    }
}
