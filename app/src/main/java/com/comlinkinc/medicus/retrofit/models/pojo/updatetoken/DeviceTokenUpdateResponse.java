package com.comlinkinc.medicus.retrofit.models.pojo.updatetoken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public class DeviceTokenUpdateResponse {
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
