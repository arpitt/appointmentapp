package com.comlinkinc.medicus.retrofit.models.pojo.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

@SerializedName("token")
@Expose
private Token token;
@SerializedName("appName")
@Expose
private String appName;
@SerializedName("userId")
@Expose
private String userId;
@SerializedName("enabled")
@Expose
private Boolean enabled;
@SerializedName("createdAt")
@Expose
private String createdAt;
@SerializedName("updatedAt")
@Expose
private String updatedAt;
@SerializedName("_id")
@Expose
private String id;

public Token getToken() {
return token;
}

public void setToken(Token token) {
this.token = token;
}

public String getAppName() {
return appName;
}

public void setAppName(String appName) {
this.appName = appName;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public Boolean getEnabled() {
return enabled;
}

public void setEnabled(Boolean enabled) {
this.enabled = enabled;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

}