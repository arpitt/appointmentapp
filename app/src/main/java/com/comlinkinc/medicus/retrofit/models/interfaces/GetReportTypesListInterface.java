package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.reporttype.ReportTypeListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public interface GetReportTypesListInterface {
    @GET("/mapi/v1/report_types")
    Call<ReportTypeListResponse> getReportTypesList(@Header("X-Auth-Token") String authToken,
                                                    @Header("X-User-Id") String userId);
}
