package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.PatientReport.PatientReport;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PatientReportInterface {

    @FormUrlEncoded
    @POST("p_reports")
    Call<PatientReport> getReport(@Field("pid") String did);

}
