package com.comlinkinc.medicus.retrofit.models.pojo.fcm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FCMTokenResponse {

@SerializedName("result")
@Expose
private Result result;
@SerializedName("success")
@Expose
private Boolean success;

public Result getResult() {
return result;
}

public void setResult(Result result) {
this.result = result;
}

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

}