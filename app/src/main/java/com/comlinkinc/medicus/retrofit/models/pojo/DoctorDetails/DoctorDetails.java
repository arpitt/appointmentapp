package com.comlinkinc.medicus.retrofit.models.pojo.DoctorDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorDetails {

@SerializedName("base_url")
@Expose
private String baseUrl;
@SerializedName("code")
@Expose
private Integer code;
@SerializedName("data")
@Expose
private DoctorData data;

public String getBaseUrl() {
return baseUrl;
}

public void setBaseUrl(String baseUrl) {
this.baseUrl = baseUrl;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public DoctorData getData() {
return data;
}

public void setData(DoctorData data) {
this.data = data;
}

}