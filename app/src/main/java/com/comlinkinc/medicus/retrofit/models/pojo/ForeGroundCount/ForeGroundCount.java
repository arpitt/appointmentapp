package com.comlinkinc.medicus.retrofit.models.pojo.ForeGroundCount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForeGroundCount {

@SerializedName("update")
@Expose
private List<Update> update = null;
@SerializedName("remove")
@Expose
private List<Object> remove = null;
@SerializedName("success")
@Expose
private Boolean success;

public List<Update> getUpdate() {
return update;
}

public void setUpdate(List<Update> update) {
this.update = update;
}

public List<Object> getRemove() {
return remove;
}

public void setRemove(List<Object> remove) {
this.remove = remove;
}

public Boolean getSuccess() {
return success;
}

public void setSuccess(Boolean success) {
this.success = success;
}

}
