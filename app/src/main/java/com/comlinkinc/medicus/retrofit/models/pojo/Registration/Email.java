package com.comlinkinc.medicus.retrofit.models.pojo.Registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



import java.util.List;

/**
 * Created by sachink on 12/21/2017.
 */

public class Email {
    @SerializedName("verificationTokens")
    @Expose
    private List<VerificationToken> verificationTokens = null;

    public List<VerificationToken> getVerificationTokens() {
        return verificationTokens;
    }

    public void setVerificationTokens(List<VerificationToken> verificationTokens) {
        this.verificationTokens = verificationTokens;
    }
}
