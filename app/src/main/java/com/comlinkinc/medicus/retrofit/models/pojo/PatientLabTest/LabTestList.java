package com.comlinkinc.medicus.retrofit.models.pojo.PatientLabTest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabTestList {

@SerializedName("lab_test_name")
@Expose
private String labTestName;
@SerializedName("old_date")
@Expose
private String oldDate;
@SerializedName("upcoming_date")
@Expose
private String upcomingDate;

public String getLabTestName() {
return labTestName;
}

public void setLabTestName(String labTestName) {
this.labTestName = labTestName;
}

public String getOldDate() {
return oldDate;
}

public void setOldDate(String oldDate) {
this.oldDate = oldDate;
}

public String getUpcomingDate() {
return upcomingDate;
}

public void setUpcomingDate(String upcomingDate) {
this.upcomingDate = upcomingDate;
}

}