package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.DoctorDetails.DoctorDetails;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface DoctorDetailsInterface {

    @FormUrlEncoded
    @POST("d_details")
    Call<DoctorDetails> getDocDetails(@Field("did") String userType);

}
