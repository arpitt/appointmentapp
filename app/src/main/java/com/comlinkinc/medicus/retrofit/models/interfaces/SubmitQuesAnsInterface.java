package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.submitQuesAns.SubmitQuesAnsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public interface SubmitQuesAnsInterface {
    @FormUrlEncoded
    @POST("q_response")
    Call<SubmitQuesAnsResponse> submitQuesAns(@Field("rdata") String quesAnswersJson);
}
