package com.comlinkinc.medicus.retrofit.models.pojo.login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("auth_token")
    @Expose
    private String authToken;
    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("qc")
    @Expose
    private String qc;
    @SerializedName("p_name")
    @Expose
    private String pName;
    @SerializedName("p_email")
    @Expose
    private String pEmail;
    @SerializedName("pp_path")
    @Expose
    private String ppPath;
    @SerializedName("p_phone")
    @Expose
    private String pPhone;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("did")
    @Expose
    private String did;
    @SerializedName("d_email")
    @Expose
    private String dEmail;
    @SerializedName("d_username")
    @Expose
    private String dUsername;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("ctsi_userid")
    @Expose
    private String ctsiUserid;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("base_url")
    @Expose
    private String baseUrl;

    public LoginResponse() {
    }

    public LoginResponse(Integer code, Integer userType, String authToken, String pid, String qc, String pName, String pEmail, String ppPath, String pPhone, String gender, String did, String dEmail, String dUsername, String dob, String ctsiUserid, String isVerified, String message, String baseUrl) {
        this.code = code;
        this.userType = userType;
        this.authToken = authToken;
        this.pid = pid;
        this.qc = qc;
        this.pName = pName;
        this.pEmail = pEmail;
        this.ppPath = ppPath;
        this.pPhone = pPhone;
        this.gender = gender;
        this.did = did;
        this.dEmail = dEmail;
        this.dUsername = dUsername;
        this.dob = dob;
        this.ctsiUserid = ctsiUserid;
        this.isVerified = isVerified;
        this.message = message;
        this.baseUrl = baseUrl;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getQc() {
        return qc;
    }

    public void setQc(String qc) {
        this.qc = qc;
    }

    public String getPName() {
        return pName;
    }

    public void setPName(String pName) {
        this.pName = pName;
    }

    public String getPEmail() {
        return pEmail;
    }

    public void setPEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getPpPath() {
        return ppPath;
    }

    public void setPpPath(String ppPath) {
        this.ppPath = ppPath;
    }

    public String getPPhone() {
        return pPhone;
    }

    public void setPPhone(String pPhone) {
        this.pPhone = pPhone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDEmail() {
        return dEmail;
    }

    public void setDEmail(String dEmail) {
        this.dEmail = dEmail;
    }

    public String getDUsername() {
        return dUsername;
    }

    public void setDUsername(String dUsername) {
        this.dUsername = dUsername;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCtsiUserid() {
        return ctsiUserid;
    }

    public void setCtsiUserid(String ctsiUserid) {
        this.ctsiUserid = ctsiUserid;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpEmail() {
        return pEmail;
    }

    public void setpEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getpPhone() {
        return pPhone;
    }

    public void setpPhone(String pPhone) {
        this.pPhone = pPhone;
    }

    public String getdEmail() {
        return dEmail;
    }

    public void setdEmail(String dEmail) {
        this.dEmail = dEmail;
    }

    public String getdUsername() {
        return dUsername;
    }

    public void setdUsername(String dUsername) {
        this.dUsername = dUsername;
    }
}