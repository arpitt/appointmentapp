package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.fcm.FCMTokenResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface FCMTokenUpdateInterface {
    @FormUrlEncoded
    @POST("push.token")
    Call<FCMTokenResponse> updateFCMToken(@Header("X-Auth-Token") String auth_token,
                                          @Header("X-User-Id") String user_id,
                                          @Field("type") String type,
                                          @Field("value") String value,
                                          @Field("appName") String appName);
}
