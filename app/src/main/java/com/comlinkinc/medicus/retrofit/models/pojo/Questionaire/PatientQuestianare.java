package com.comlinkinc.medicus.retrofit.models.pojo.Questionaire;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PatientQuestianare {

@SerializedName("data")
@Expose
private List<Questions> data = null;
@SerializedName("code")
@Expose
private Integer code;

public List<Questions> getData() {
return data;
}

public void setData(List<Questions> data) {
this.data = data;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

}