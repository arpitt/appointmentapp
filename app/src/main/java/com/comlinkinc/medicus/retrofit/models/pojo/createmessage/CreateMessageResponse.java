
package com.comlinkinc.medicus.retrofit.models.pojo.createmessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateMessageResponse {

    @SerializedName("ts")
    @Expose
    private Integer ts;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Integer getTs() {
        return ts;
    }

    public void setTs(Integer ts) {
        this.ts = ts;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
