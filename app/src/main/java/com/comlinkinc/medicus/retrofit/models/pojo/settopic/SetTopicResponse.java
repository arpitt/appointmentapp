package com.comlinkinc.medicus.retrofit.models.pojo.settopic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jun 2018
 ***********************************************************************/

public class SetTopicResponse {
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
