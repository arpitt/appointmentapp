package com.comlinkinc.medicus.retrofit.models.pojo.updatetokensendjsonobj;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public class UpdateTokenJsonObject {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
