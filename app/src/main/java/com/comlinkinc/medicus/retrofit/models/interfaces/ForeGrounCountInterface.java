package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.ForeGroundCount.ForeGroundCount;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ForeGrounCountInterface {

    @GET("subscriptions.get")
    Call<ForeGroundCount> abcd(@Header("X-Auth-Token") String authtoken,@Header("X-User-Id")String userid);




}
