package com.comlinkinc.medicus.retrofit.models.pojo.groups;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.comlinkinc.medicus.retrofit.models.pojo.users.CustomFields;


import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jan 2018
 ***********************************************************************/

public class Channel {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("msgs")
    @Expose
    private Integer msgs;
    @SerializedName("u")
    @Expose
    private U u;
    @SerializedName("customFields")
    @Expose
    private CustomFields customFields;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("ro")
    @Expose
    private Boolean ro;
    @SerializedName("sysMes")
    @Expose
    private Boolean sysMes;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("usernames")
    @Expose
    private List<String> usernames = null;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("lm")
    @Expose
    private String lm;
    @SerializedName("muted")
    @Expose
    private List<String> muted = null;
    @SerializedName("default")
    @Expose
    private Boolean _default;
    @SerializedName("jitsiTimeout")
    @Expose
    private String jitsiTimeout;
    @SerializedName("announcement")
    @Expose
    private String announcement;
    @SerializedName("isChannel")
    @Expose
    private String isChannel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Integer getMsgs() {
        return msgs;
    }

    public void setMsgs(Integer msgs) {
        this.msgs = msgs;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Boolean getRo() {
        return ro;
    }

    public void setRo(Boolean ro) {
        this.ro = ro;
    }

    public Boolean getSysMes() {
        return sysMes;
    }

    public void setSysMes(Boolean sysMes) {
        this.sysMes = sysMes;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLm() {
        return lm;
    }

    public void setLm(String lm) {
        this.lm = lm;
    }

    public List<String> getMuted() {
        return muted;
    }

    public void setMuted(List<String> muted) {
        this.muted = muted;
    }

    public Boolean getDefault() {
        return _default;
    }

    public void setDefault(Boolean _default) {
        this._default = _default;
    }

    public String getJitsiTimeout() {
        return jitsiTimeout;
    }

    public void setJitsiTimeout(String jitsiTimeout) {
        this.jitsiTimeout = jitsiTimeout;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getIsChannel() {
        return isChannel;
    }

    public void setIsChannel(String isChannel) {
        this.isChannel = isChannel;
    }

}
