package com.comlinkinc.medicus.retrofit;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  18 Apr 2018
 ***********************************************************************/

public class RetrofitConstants {

    public static final String BASE_URL = "https://medicus.mvoipctsi.com/api/";
}
