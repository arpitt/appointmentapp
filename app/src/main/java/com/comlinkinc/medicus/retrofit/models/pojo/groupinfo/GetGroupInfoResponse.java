package com.comlinkinc.medicus.retrofit.models.pojo.groupinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 1/26/2018.
 */

public class GetGroupInfoResponse {
    @SerializedName("group")
    @Expose
    private Group group;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
