
package com.comlinkinc.medicus.retrofit.models.pojo.chathistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Url {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("headers")
    @Expose
    private Headers headers;
    @SerializedName("parsedUrl")
    @Expose
    private ParsedUrl parsedUrl;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public ParsedUrl getParsedUrl() {
        return parsedUrl;
    }

    public void setParsedUrl(ParsedUrl parsedUrl) {
        this.parsedUrl = parsedUrl;
    }

}
