package com.comlinkinc.medicus.retrofit.models.interfaces;




import com.comlinkinc.medicus.retrofit.models.pojo.creategroup.CreateGroupRequest;
import com.comlinkinc.medicus.retrofit.models.pojo.creategroup.CreateGroupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface CreateGroupInterface {
    @POST("groups.create")
    Call<CreateGroupResponse> createNewGroup(@Header("X-Auth-Token") String auth_token,
                                             @Header("X-User-Id") String user_id, @Header("Content-Type") String content_type,
                                             @Body CreateGroupRequest createGroupRequest);
}
