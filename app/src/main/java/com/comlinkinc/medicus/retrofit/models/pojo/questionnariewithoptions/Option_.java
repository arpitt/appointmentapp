
package com.comlinkinc.medicus.retrofit.models.pojo.questionnariewithoptions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Option_ {

    @SerializedName("qo_id")
    @Expose
    private String qoId;

    @SerializedName("q_option")
    @Expose
    private String qOption;

    @SerializedName("sub_q_id")
    @Expose
    private String subQId;

    public String getQoId() {
        return qoId;
    }

    public void setQoId(String qoId) {
        this.qoId = qoId;
    }

    public String getQOption() {
        return qOption;
    }

    public void setQOption(String qOption) {
        this.qOption = qOption;
    }

    public String getSubQId() {
        return subQId;
    }

    public void setSubQId(String subQId) {
        this.subQId = subQId;
    }

    @Override
    public String toString() {
        return qOption;
    }

}
