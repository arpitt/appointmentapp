
package com.comlinkinc.medicus.retrofit.models.pojo.chathistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParsedUrl {

    @SerializedName("host")
    @Expose
    private String host;
    @SerializedName("hash")
    @Expose
    private Object hash;
    @SerializedName("pathname")
    @Expose
    private String pathname;
    @SerializedName("protocol")
    @Expose
    private String protocol;
    @SerializedName("port")
    @Expose
    private Object port;
    @SerializedName("query")
    @Expose
    private Object query;
    @SerializedName("search")
    @Expose
    private Object search;
    @SerializedName("hostname")
    @Expose
    private String hostname;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Object getHash() {
        return hash;
    }

    public void setHash(Object hash) {
        this.hash = hash;
    }

    public String getPathname() {
        return pathname;
    }

    public void setPathname(String pathname) {
        this.pathname = pathname;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Object getPort() {
        return port;
    }

    public void setPort(Object port) {
        this.port = port;
    }

    public Object getQuery() {
        return query;
    }

    public void setQuery(Object query) {
        this.query = query;
    }

    public Object getSearch() {
        return search;
    }

    public void setSearch(Object search) {
        this.search = search;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

}
