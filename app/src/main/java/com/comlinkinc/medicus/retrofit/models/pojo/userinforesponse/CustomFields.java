package com.comlinkinc.medicus.retrofit.models.pojo.userinforesponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 1/26/2018.
 */

public class CustomFields {
    @SerializedName("devicetoken")
    @Expose
    private String devicetoken;

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }
}
