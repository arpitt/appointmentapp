package com.comlinkinc.medicus.retrofit.models.pojo.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public class Services {
    @SerializedName("resume")
    @Expose
    private Resume resume;
    @SerializedName("password")
    @Expose
    private Password password;
    @SerializedName("email")
    @Expose
    private Email email;

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }
}
