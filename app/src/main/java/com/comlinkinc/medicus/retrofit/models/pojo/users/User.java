package com.comlinkinc.medicus.retrofit.models.pojo.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public class User {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("avatarOrigin")
    @Expose
    private String avatarOrigin;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusDefault")
    @Expose
    private String statusDefault;
    @SerializedName("utcOffset")
    @Expose
    private Double utcOffset;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("services")
    @Expose
    private Services services;
    @SerializedName("emails")
    @Expose
    private List<Email_> emails = null;
    @SerializedName("customFields")
    @Expose
    private CustomFields customFields;
    @SerializedName("lastLogin")
    @Expose
    private String lastLogin;
    @SerializedName("statusConnection")
    @Expose
    private String statusConnection;
    @SerializedName("department")
    @Expose
    private Object department;
    @SerializedName("joinDefaultChannels")
    @Expose
    private Boolean joinDefaultChannels;
    @SerializedName("userAgent")
    @Expose
    private String userAgent;
    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("host")
    @Expose
    private String host;
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("visitorEmails")
    @Expose
    private List<VisitorEmail> visitorEmails = null;
    @SerializedName("requirePasswordChange")
    @Expose
    private Boolean requirePasswordChange;
    @SerializedName("operator")
    @Expose
    private Boolean operator;
    @SerializedName("statusLivechat")
    @Expose
    private String statusLivechat;
    @SerializedName("livechatCount")
    @Expose
    private Integer livechatCount;
    @SerializedName("settings")
    @Expose
    private Settings settings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAvatarOrigin() {
        return avatarOrigin;
    }

    public void setAvatarOrigin(String avatarOrigin) {
        this.avatarOrigin = avatarOrigin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDefault() {
        return statusDefault;
    }

    public void setStatusDefault(String statusDefault) {
        this.statusDefault = statusDefault;
    }

    public Double getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Double utcOffset) {
        this.utcOffset = utcOffset;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public List<Email_> getEmails() {
        return emails;
    }

    public void setEmails(List<Email_> emails) {
        this.emails = emails;
    }

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getStatusConnection() {
        return statusConnection;
    }

    public void setStatusConnection(String statusConnection) {
        this.statusConnection = statusConnection;
    }

    public Object getDepartment() {
        return department;
    }

    public void setDepartment(Object department) {
        this.department = department;
    }

    public Boolean getJoinDefaultChannels() {
        return joinDefaultChannels;
    }

    public void setJoinDefaultChannels(Boolean joinDefaultChannels) {
        this.joinDefaultChannels = joinDefaultChannels;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<VisitorEmail> getVisitorEmails() {
        return visitorEmails;
    }

    public void setVisitorEmails(List<VisitorEmail> visitorEmails) {
        this.visitorEmails = visitorEmails;
    }

    public Boolean getRequirePasswordChange() {
        return requirePasswordChange;
    }

    public void setRequirePasswordChange(Boolean requirePasswordChange) {
        this.requirePasswordChange = requirePasswordChange;
    }

    public Boolean getOperator() {
        return operator;
    }

    public void setOperator(Boolean operator) {
        this.operator = operator;
    }

    public String getStatusLivechat() {
        return statusLivechat;
    }

    public void setStatusLivechat(String statusLivechat) {
        this.statusLivechat = statusLivechat;
    }

    public Integer getLivechatCount() {
        return livechatCount;
    }

    public void setLivechatCount(Integer livechatCount) {
        this.livechatCount = livechatCount;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

}
