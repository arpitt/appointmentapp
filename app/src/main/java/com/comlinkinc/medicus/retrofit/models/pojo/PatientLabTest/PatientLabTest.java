package com.comlinkinc.medicus.retrofit.models.pojo.PatientLabTest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.comlinkinc.medicus.retrofit.models.pojo.labtest.LabTestList;

import java.util.List;

public class PatientLabTest {

@SerializedName("lab_test_list")
@Expose
private List<LabTestList> labTestList = null;
@SerializedName("code")
@Expose
private Integer code;

public List<LabTestList> getLabTestList() {
return labTestList;
}

public void setLabTestList(List<LabTestList> labTestList) {
this.labTestList = labTestList;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

}