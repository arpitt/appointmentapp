package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.groupmemberslist.GroupMembersListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Tushar on 6/24/2018.
 */

public interface ChannelMembersListInterface {
    @GET("channels.members")
    Call<GroupMembersListResponse> getChannelsMembersList(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Query("roomId") String roomId);
}
