package com.comlinkinc.medicus.retrofit.models.interfaces;




import com.comlinkinc.medicus.retrofit.models.pojo.createmessage.CreateMessageRequest;
import com.comlinkinc.medicus.retrofit.models.pojo.createmessage.CreateMessageResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface CreateMessageInterface {
    @POST("chat.postMessage")
    Call<CreateMessageResponse> createNewMessage(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Header("Content-Type") String content_type, @Body CreateMessageRequest createMessageRequest);
}
