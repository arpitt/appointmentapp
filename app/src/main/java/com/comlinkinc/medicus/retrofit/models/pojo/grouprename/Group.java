package com.comlinkinc.medicus.retrofit.models.pojo.grouprename;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.comlinkinc.medicus.retrofit.models.pojo.groupinfo.LastMessage;


import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  11 Jul 2018
 ***********************************************************************/

public class Group {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("msgs")
    @Expose
    private Integer msgs;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("ro")
    @Expose
    private Boolean ro;
    @SerializedName("sysMes")
    @Expose
    private Boolean sysMes;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("muted")
    @Expose
    private List<String> muted = null;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("lm")
    @Expose
    private String lm;
    @SerializedName("lastMessage")
    @Expose
    private LastMessage lastMessage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Integer getMsgs() {
        return msgs;
    }

    public void setMsgs(Integer msgs) {
        this.msgs = msgs;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Boolean getRo() {
        return ro;
    }

    public void setRo(Boolean ro) {
        this.ro = ro;
    }

    public Boolean getSysMes() {
        return sysMes;
    }

    public void setSysMes(Boolean sysMes) {
        this.sysMes = sysMes;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getMuted() {
        return muted;
    }

    public void setMuted(List<String> muted) {
        this.muted = muted;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLm() {
        return lm;
    }

    public void setLm(String lm) {
        this.lm = lm;
    }

    public LastMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

}
