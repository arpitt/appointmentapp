package com.comlinkinc.medicus.retrofit.models.pojo.reporttype;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by tushar on 5/6/2018.
 */

public class ReportTypeListResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rt_type_list")
    @Expose
    private List<RtTypeList> rtTypeList = null;

    public List<RtTypeList> getRtTypeList() {
        return rtTypeList;
    }

    public void setRtTypeList(List<RtTypeList> rtTypeList) {
        this.rtTypeList = rtTypeList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
