package com.comlinkinc.medicus.retrofit.models.pojo.Questionaire;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Questions {

@SerializedName("question")
@Expose
private String question;
@SerializedName("qt_id")
@Expose
private String qtId;
@SerializedName("ans")
@Expose
private String ans;
@SerializedName("sub_question")
@Expose
private SubQuestion subQuestion;

public String getQuestion() {
return question;
}

public void setQuestion(String question) {
this.question = question;
}

public String getQtId() {
return qtId;
}

public void setQtId(String qtId) {
this.qtId = qtId;
}

public String getAns() {
return ans;
}

public void setAns(String ans) {
this.ans = ans;
}

public SubQuestion getSubQuestion() {
return subQuestion;
}

public void setSubQuestion(SubQuestion subQuestion) {
this.subQuestion = subQuestion;
}

}