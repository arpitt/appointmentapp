package com.comlinkinc.medicus.retrofit.models.pojo.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public class Preferences {
    @SerializedName("newRoomNotification")
    @Expose
    private String newRoomNotification;
    @SerializedName("newMessageNotification")
    @Expose
    private String newMessageNotification;
    @SerializedName("useEmojis")
    @Expose
    private Boolean useEmojis;
    @SerializedName("convertAsciiEmoji")
    @Expose
    private Boolean convertAsciiEmoji;
    @SerializedName("saveMobileBandwidth")
    @Expose
    private Boolean saveMobileBandwidth;
    @SerializedName("collapseMediaByDefault")
    @Expose
    private Boolean collapseMediaByDefault;
    @SerializedName("autoImageLoad")
    @Expose
    private Boolean autoImageLoad;
    @SerializedName("emailNotificationMode")
    @Expose
    private String emailNotificationMode;
    @SerializedName("roomsListExhibitionMode")
    @Expose
    private String roomsListExhibitionMode;
    @SerializedName("unreadAlert")
    @Expose
    private Boolean unreadAlert;
    @SerializedName("notificationsSoundVolume")
    @Expose
    private Integer notificationsSoundVolume;
    @SerializedName("desktopNotifications")
    @Expose
    private String desktopNotifications;
    @SerializedName("mobileNotifications")
    @Expose
    private String mobileNotifications;
    @SerializedName("audioNotificationValue")
    @Expose
    private Object audioNotificationValue;
    @SerializedName("desktopNotificationDuration")
    @Expose
    private Integer desktopNotificationDuration;
    @SerializedName("viewMode")
    @Expose
    private Integer viewMode;
    @SerializedName("hideUsernames")
    @Expose
    private Boolean hideUsernames;
    @SerializedName("hideRoles")
    @Expose
    private Boolean hideRoles;
    @SerializedName("hideAvatars")
    @Expose
    private Boolean hideAvatars;
    @SerializedName("hideFlexTab")
    @Expose
    private Boolean hideFlexTab;
    @SerializedName("highlights")
    @Expose
    private List<Object> highlights = null;
    @SerializedName("sendOnEnter")
    @Expose
    private String sendOnEnter;

    public String getNewRoomNotification() {
        return newRoomNotification;
    }

    public void setNewRoomNotification(String newRoomNotification) {
        this.newRoomNotification = newRoomNotification;
    }

    public String getNewMessageNotification() {
        return newMessageNotification;
    }

    public void setNewMessageNotification(String newMessageNotification) {
        this.newMessageNotification = newMessageNotification;
    }

    public Boolean getUseEmojis() {
        return useEmojis;
    }

    public void setUseEmojis(Boolean useEmojis) {
        this.useEmojis = useEmojis;
    }

    public Boolean getConvertAsciiEmoji() {
        return convertAsciiEmoji;
    }

    public void setConvertAsciiEmoji(Boolean convertAsciiEmoji) {
        this.convertAsciiEmoji = convertAsciiEmoji;
    }

    public Boolean getSaveMobileBandwidth() {
        return saveMobileBandwidth;
    }

    public void setSaveMobileBandwidth(Boolean saveMobileBandwidth) {
        this.saveMobileBandwidth = saveMobileBandwidth;
    }

    public Boolean getCollapseMediaByDefault() {
        return collapseMediaByDefault;
    }

    public void setCollapseMediaByDefault(Boolean collapseMediaByDefault) {
        this.collapseMediaByDefault = collapseMediaByDefault;
    }

    public Boolean getAutoImageLoad() {
        return autoImageLoad;
    }

    public void setAutoImageLoad(Boolean autoImageLoad) {
        this.autoImageLoad = autoImageLoad;
    }

    public String getEmailNotificationMode() {
        return emailNotificationMode;
    }

    public void setEmailNotificationMode(String emailNotificationMode) {
        this.emailNotificationMode = emailNotificationMode;
    }

    public String getRoomsListExhibitionMode() {
        return roomsListExhibitionMode;
    }

    public void setRoomsListExhibitionMode(String roomsListExhibitionMode) {
        this.roomsListExhibitionMode = roomsListExhibitionMode;
    }

    public Boolean getUnreadAlert() {
        return unreadAlert;
    }

    public void setUnreadAlert(Boolean unreadAlert) {
        this.unreadAlert = unreadAlert;
    }

    public Integer getNotificationsSoundVolume() {
        return notificationsSoundVolume;
    }

    public void setNotificationsSoundVolume(Integer notificationsSoundVolume) {
        this.notificationsSoundVolume = notificationsSoundVolume;
    }

    public String getDesktopNotifications() {
        return desktopNotifications;
    }

    public void setDesktopNotifications(String desktopNotifications) {
        this.desktopNotifications = desktopNotifications;
    }

    public String getMobileNotifications() {
        return mobileNotifications;
    }

    public void setMobileNotifications(String mobileNotifications) {
        this.mobileNotifications = mobileNotifications;
    }

    public Object getAudioNotificationValue() {
        return audioNotificationValue;
    }

    public void setAudioNotificationValue(Object audioNotificationValue) {
        this.audioNotificationValue = audioNotificationValue;
    }

    public Integer getDesktopNotificationDuration() {
        return desktopNotificationDuration;
    }

    public void setDesktopNotificationDuration(Integer desktopNotificationDuration) {
        this.desktopNotificationDuration = desktopNotificationDuration;
    }

    public Integer getViewMode() {
        return viewMode;
    }

    public void setViewMode(Integer viewMode) {
        this.viewMode = viewMode;
    }

    public Boolean getHideUsernames() {
        return hideUsernames;
    }

    public void setHideUsernames(Boolean hideUsernames) {
        this.hideUsernames = hideUsernames;
    }

    public Boolean getHideRoles() {
        return hideRoles;
    }

    public void setHideRoles(Boolean hideRoles) {
        this.hideRoles = hideRoles;
    }

    public Boolean getHideAvatars() {
        return hideAvatars;
    }

    public void setHideAvatars(Boolean hideAvatars) {
        this.hideAvatars = hideAvatars;
    }

    public Boolean getHideFlexTab() {
        return hideFlexTab;
    }

    public void setHideFlexTab(Boolean hideFlexTab) {
        this.hideFlexTab = hideFlexTab;
    }

    public List<Object> getHighlights() {
        return highlights;
    }

    public void setHighlights(List<Object> highlights) {
        this.highlights = highlights;
    }

    public String getSendOnEnter() {
        return sendOnEnter;
    }

    public void setSendOnEnter(String sendOnEnter) {
        this.sendOnEnter = sendOnEnter;
    }
}
