package com.comlinkinc.medicus.retrofit.models.pojo.im;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Room {

@SerializedName("_id")
@Expose
private String id;
@SerializedName("_updatedAt")
@Expose
private String updatedAt;
@SerializedName("t")
@Expose
private String t;
@SerializedName("msgs")
@Expose
private Integer msgs;
@SerializedName("ts")
@Expose
private String ts;
@SerializedName("meta")
@Expose
private Meta meta;
@SerializedName("$loki")
@Expose
private Integer $loki;
@SerializedName("usernames")
@Expose
private List<String> usernames = null;

/**
* No args constructor for use in serialization
*
*/
public Room() {
}

/**
*
* @param msgs
* @param t
* @param meta
* @param usernames
* @param id
* @param $loki
* @param updatedAt
* @param ts
*/
public Room(String id, String updatedAt, String t, Integer msgs, String ts, Meta meta, Integer $loki, List<String> usernames) {
super();
this.id = id;
this.updatedAt = updatedAt;
this.t = t;
this.msgs = msgs;
this.ts = ts;
this.meta = meta;
this.$loki = $loki;
this.usernames = usernames;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public String getT() {
return t;
}

public void setT(String t) {
this.t = t;
}

public Integer getMsgs() {
return msgs;
}

public void setMsgs(Integer msgs) {
this.msgs = msgs;
}

public String getTs() {
return ts;
}

public void setTs(String ts) {
this.ts = ts;
}

public Meta getMeta() {
return meta;
}

public void setMeta(Meta meta) {
this.meta = meta;
}

public Integer get$loki() {
return $loki;
}

public void set$loki(Integer $loki) {
this.$loki = $loki;
}

public List<String> getUsernames() {
return usernames;
}

public void setUsernames(List<String> usernames) {
this.usernames = usernames;
}

}