package com.comlinkinc.medicus.retrofit.models.pojo.DoctorDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorData {

@SerializedName("did")
@Expose
private String did;
@SerializedName("d_name")
@Expose
private String dName;
@SerializedName("d_degree")
@Expose
private String dDegree;
@SerializedName("d_email")
@Expose
private String dEmail;
@SerializedName("d_phone")
@Expose
private String dPhone;
@SerializedName("d_pp_path")
@Expose
private String dPpPath;
@SerializedName("hospital_name")
@Expose
private String hospitalName;
@SerializedName("hospital_address")
@Expose
private String hospitalAddress;
@SerializedName("status")
@Expose
private String status;
@SerializedName("ctsi_userid")
@Expose
private String ctsiUserid;
@SerializedName("sp_ids")
@Expose
private String spIds;

public String getDid() {
return did;
}

public void setDid(String did) {
this.did = did;
}

public String getDName() {
return dName;
}

public void setDName(String dName) {
this.dName = dName;
}

public String getDDegree() {
return dDegree;
}

public void setDDegree(String dDegree) {
this.dDegree = dDegree;
}

public String getDEmail() {
return dEmail;
}

public void setDEmail(String dEmail) {
this.dEmail = dEmail;
}

public String getDPhone() {
return dPhone;
}

public void setDPhone(String dPhone) {
this.dPhone = dPhone;
}

public String getDPpPath() {
return dPpPath;
}

public void setDPpPath(String dPpPath) {
this.dPpPath = dPpPath;
}

public String getHospitalName() {
return hospitalName;
}

public void setHospitalName(String hospitalName) {
this.hospitalName = hospitalName;
}

public String getHospitalAddress() {
return hospitalAddress;
}

public void setHospitalAddress(String hospitalAddress) {
this.hospitalAddress = hospitalAddress;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getCtsiUserid() {
return ctsiUserid;
}

public void setCtsiUserid(String ctsiUserid) {
this.ctsiUserid = ctsiUserid;
}

public String getSpIds() {
return spIds;
}

public void setSpIds(String spIds) {
this.spIds = spIds;
}

}