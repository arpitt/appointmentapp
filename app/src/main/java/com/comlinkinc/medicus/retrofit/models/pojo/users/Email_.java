package com.comlinkinc.medicus.retrofit.models.pojo.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public class Email_ {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("verified")
    @Expose
    private Boolean verified;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
