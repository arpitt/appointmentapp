package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.login.LoginResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface DoctorUpdateProfileInterface {

    @FormUrlEncoded
    @POST("mapi/v1/update_doctor")
    Call<ForgotPasswordResponse> updateDoctorProfile(@Header("X-Auth-Token") String authToken,
                                                     @Header("X-User-Id") String userId,
                                                     @Field("did") String doctorId,
                                                     @Field("un") String uername,
                                                     @Field("qf") String qualification,
                                                     @Field("m") String mobile,
                                                     @Field("hn") String hospital_name,
                                                     @Field("ha") String hospital_address,
                                                     @Field("sp_ids[]") ArrayList<String> spIds);

}
