package com.comlinkinc.medicus.retrofit.models.interfaces;



import com.comlinkinc.medicus.retrofit.models.pojo.groups.GroupsListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface GroupsListInterface {
    @GET("groups.list")
    Call<GroupsListResponse> getGroupsList(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id);
}
