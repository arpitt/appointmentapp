package com.comlinkinc.medicus.retrofit.models.pojo.ForeGroundCount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class U {

@SerializedName("_id")
@Expose
private String id;
@SerializedName("username")
@Expose
private String username;
@SerializedName("name")
@Expose
private String name;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getUsername() {
return username;
}

public void setUsername(String username) {
this.username = username;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

}

