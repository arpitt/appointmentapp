package com.comlinkinc.medicus.retrofit.models.pojo.channelinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  07 Mar 2018
 ***********************************************************************/

public class ChannelInfoResponse {
    @SerializedName("channel")
    @Expose
    private Channel channel;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
