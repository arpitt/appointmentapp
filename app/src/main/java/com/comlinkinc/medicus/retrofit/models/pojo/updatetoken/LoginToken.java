package com.comlinkinc.medicus.retrofit.models.pojo.updatetoken;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public class LoginToken {
    @SerializedName("when")
    @Expose
    private String when;
    @SerializedName("hashedToken")
    @Expose
    private String hashedToken;

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getHashedToken() {
        return hashedToken;
    }

    public void setHashedToken(String hashedToken) {
        this.hashedToken = hashedToken;
    }
}
