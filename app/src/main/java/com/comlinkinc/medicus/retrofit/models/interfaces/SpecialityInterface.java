package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.ApsList.ApsList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface SpecialityInterface {

    @GET("mapi/v1/specialities/list")
    Call<ApsList> specialities(@Header("X-Auth-Token") String authToken,
                               @Header("X-User-Id") String userId);
}
