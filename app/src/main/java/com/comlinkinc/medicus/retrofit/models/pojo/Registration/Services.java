package com.comlinkinc.medicus.retrofit.models.pojo.Registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sachink on 12/21/2017.
 */

public class Services {
    @SerializedName("password")
    @Expose
    private Password password;
    @SerializedName("email")
    @Expose
    private Email email;

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

}
