package com.comlinkinc.medicus.retrofit.models.pojo.im;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

@SerializedName("revision")
@Expose
private Integer revision;
@SerializedName("created")
@Expose
private Integer created;
@SerializedName("version")
@Expose
private Integer version;

/**
* No args constructor for use in serialization
*
*/
public Meta() {
}

/**
*
* @param created
* @param version
* @param revision
*/
public Meta(Integer revision, Integer created, Integer version) {
super();
this.revision = revision;
this.created = created;
this.version = version;
}

public Integer getRevision() {
return revision;
}

public void setRevision(Integer revision) {
this.revision = revision;
}

public Integer getCreated() {
return created;
}

public void setCreated(Integer created) {
this.created = created;
}

public Integer getVersion() {
return version;
}

public void setVersion(Integer version) {
this.version = version;
}

}