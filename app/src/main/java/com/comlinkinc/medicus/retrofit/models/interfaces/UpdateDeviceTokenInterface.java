package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.updatedevicetokenjson.UpdateDeviceTokenJsonPost;
import com.comlinkinc.medicus.retrofit.models.pojo.updatetoken.DeviceTokenUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public interface UpdateDeviceTokenInterface {
    @POST("users.update")
    Call<DeviceTokenUpdateResponse> postDeviceTokenUpdateResponseCall(@Header("Content-type") String content_type, @Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Body UpdateDeviceTokenJsonPost jsonObject);
}
