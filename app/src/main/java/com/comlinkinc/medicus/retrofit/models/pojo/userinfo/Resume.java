package com.comlinkinc.medicus.retrofit.models.pojo.userinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public class Resume {
    @SerializedName("loginTokens")
    @Expose
    private List<LoginToken> loginTokens = null;

    public List<LoginToken> getLoginTokens() {
        return loginTokens;
    }

    public void setLoginTokens(List<LoginToken> loginTokens) {
        this.loginTokens = loginTokens;
    }

}
