package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.PatientList.PatientList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PatientListInterface {

    @FormUrlEncoded
    @POST("mapi/v1/patients")
    Call<PatientList> getPatients(@Header("X-Auth-Token") String authToken,
                                  @Header("X-User-Id") String userId,
                                  @Field("did") String did);
}
