package com.comlinkinc.medicus.retrofit.models.pojo.ForeGroundCount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Update {

@SerializedName("_id")
@Expose
private String id;
@SerializedName("rid")
@Expose
private String rid;
@SerializedName("u")
@Expose
private U u;
@SerializedName("_updatedAt")
@Expose
private String updatedAt;
@SerializedName("alert")
@Expose
private Boolean alert;
@SerializedName("fname")
@Expose
private String fname;
@SerializedName("groupMentions")
@Expose
private Integer groupMentions;
@SerializedName("ls")
@Expose
private String ls;
@SerializedName("name")
@Expose
private String name;
@SerializedName("open")
@Expose
private Boolean open;
@SerializedName("t")
@Expose
private String t;
@SerializedName("ts")
@Expose
private String ts;
@SerializedName("unread")
@Expose
private Integer unread;
@SerializedName("userMentions")
@Expose
private Integer userMentions;
@SerializedName("f")
@Expose
private Boolean f;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getRid() {
return rid;
}

public void setRid(String rid) {
this.rid = rid;
}

public U getU() {
return u;
}

public void setU(U u) {
this.u = u;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public Boolean getAlert() {
return alert;
}

public void setAlert(Boolean alert) {
this.alert = alert;
}

public String getFname() {
return fname;
}

public void setFname(String fname) {
this.fname = fname;
}

public Integer getGroupMentions() {
return groupMentions;
}

public void setGroupMentions(Integer groupMentions) {
this.groupMentions = groupMentions;
}

public String getLs() {
return ls;
}

public void setLs(String ls) {
this.ls = ls;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Boolean getOpen() {
return open;
}

public void setOpen(Boolean open) {
this.open = open;
}

public String getT() {
return t;
}

public void setT(String t) {
this.t = t;
}

public String getTs() {
return ts;
}

public void setTs(String ts) {
this.ts = ts;
}

public Integer getUnread() {
return unread;
}

public void setUnread(Integer unread) {
this.unread = unread;
}

public Integer getUserMentions() {
return userMentions;
}

public void setUserMentions(Integer userMentions) {
this.userMentions = userMentions;
}

public Boolean getF() {
return f;
}

public void setF(Boolean f) {
this.f = f;
}

}