package com.comlinkinc.medicus.retrofit.models.pojo.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageBodyResponse {

@SerializedName("host")
@Expose
private String host;
@SerializedName("rid")
@Expose
private String rid;
@SerializedName("sender")
@Expose
private Sender sender;
@SerializedName("type")
@Expose
private String type;
@SerializedName("name")
@Expose
private Object name;
@SerializedName("messageType")
@Expose
private String messageType;

public String getHost() {
return host;
}

public void setHost(String host) {
this.host = host;
}

public String getRid() {
return rid;
}

public void setRid(String rid) {
this.rid = rid;
}

public Sender getSender() {
return sender;
}

public void setSender(Sender sender) {
this.sender = sender;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

public Object getName() {
return name;
}

public void setName(Object name) {
this.name = name;
}

public String getMessageType() {
    return messageType;
}

public void setMessageType(String messageType) {
    this.messageType = messageType;
}

}