package com.comlinkinc.medicus.retrofit.models.pojo.ApsList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApsList {

@SerializedName("sp_list")
@Expose
private List<ApsList_> apsList = null;

public List<ApsList_> getApsList() {
return apsList;
}

public void setApsList(List<ApsList_> apsList) {
this.apsList = apsList;
}

}