package com.comlinkinc.medicus.retrofit.models.interfaces;



import com.comlinkinc.medicus.retrofit.models.pojo.setunreadcountzero.SetUnreadCountZeroResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public interface SetUnreadCountZeroInterface {
    @FormUrlEncoded
    @POST("subscriptions.read")
    Call<SetUnreadCountZeroResponse> setUnreadCountZero(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Field("rid") String roomId);
}
