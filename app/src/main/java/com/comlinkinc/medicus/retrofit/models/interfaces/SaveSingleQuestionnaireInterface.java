package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.savesinglequesonserver.SingleQuestionnarieResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SaveSingleQuestionnaireInterface {
    @FormUrlEncoded
    @POST("/mapi/v1/insert_ans")
    Call<SingleQuestionnarieResponse> saveSingleQuesOnServer(@Header("X-Auth-Token") String authToken,
                                                             @Header("X-User-Id") String userId,
                                                             @Field("pid") String patientId,
                                                             @Field("m_data[q_id]") String m_data_q_id,
                                                             @Field("m_data[qt_id]") String m_data_qt_id,
                                                             @Field("m_data[ans]") String m_data_ans,
                                                             @Field("m_data[qa_id]") String m_data_qa_id,
                                                             @Field("q_complete") Integer questionComplete);

    @FormUrlEncoded
    @POST("/mapi/v1/insert_ans")
    Call<SingleQuestionnarieResponse> saveMultiLevelQuesOnServer(@Header("X-Auth-Token") String authToken,
                                                                 @Header("X-User-Id") String userId,
                                                                 @Field("pid") String patientId,
                                                                 @Field("m_data[q_id]") String m_data_q_id,
                                                                 @Field("m_data[qt_id]") String m_data_qt_id,
                                                                 @Field("m_data[ans]") String m_data_ans,
                                                                 @Field("m_data[qa_id]") String m_data_qa_id,
                                                                 @Field("s_data[q_id]") String s_data_q_id,
                                                                 @Field("s_data[qt_id]") String s_data_qt_id,
                                                                 @Field("s_data[ans]") String s_data_ans,
                                                                 @Field("s_data[qa_id]") String s_data_qa_id,
                                                                 @Field("q_complete") Integer questionComplete);
}
