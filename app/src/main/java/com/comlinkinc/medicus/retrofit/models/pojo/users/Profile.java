package com.comlinkinc.medicus.retrofit.models.pojo.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  20 Dec 2017
 ***********************************************************************/

public class Profile {
    @SerializedName("guest")
    @Expose
    private Boolean guest;
    @SerializedName("token")
    @Expose
    private String token;

    public Boolean getGuest() {
        return guest;
    }

    public void setGuest(Boolean guest) {
        this.guest = guest;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
