package com.comlinkinc.medicus.retrofit.models.pojo.createmessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  21 Dec 2017
 ***********************************************************************/

public class CreateMessageRequest {
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("text")
    @Expose
    private String text;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
