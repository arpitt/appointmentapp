package com.comlinkinc.medicus.retrofit.models.pojo.Questionaire;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubQuestion {

@SerializedName("question")
@Expose
private String question;
@SerializedName("qt_id")
@Expose
private String qtId;
@SerializedName("ans")
@Expose
private String ans;

    public SubQuestion(String question, String qtId, String ans) {
        this.question = question;
        this.qtId = qtId;
        this.ans = ans;
    }

    public String getQuestion() {
return question;
}

public void setQuestion(String question) {
this.question = question;
}

public String getQtId() {
return qtId;
}

public void setQtId(String qtId) {
this.qtId = qtId;
}

public String getAns() {
return ans;
}

public void setAns(String ans) {
this.ans = ans;
}

}