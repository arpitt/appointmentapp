package com.comlinkinc.medicus.retrofit.models.pojo.grouprename;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  11 Jul 2018
 ***********************************************************************/

public class GroupRenameResponse {
    @SerializedName("group")
    @Expose
    private Group group;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
