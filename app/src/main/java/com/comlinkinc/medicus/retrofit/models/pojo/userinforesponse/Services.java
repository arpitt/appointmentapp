package com.comlinkinc.medicus.retrofit.models.pojo.userinforesponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.comlinkinc.medicus.retrofit.models.pojo.users.Password;


/**
 * Created by sachink on 1/26/2018.
 */

public class Services {
    @SerializedName("password")
    @Expose
    private Password password;
    @SerializedName("email")
    @Expose
    private Email_ email;
    @SerializedName("resume")
    @Expose
    private Resume resume;

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Email_ getEmail() {
        return email;
    }

    public void setEmail(Email_ email) {
        this.email = email;
    }

    public Resume getResume() {
        return resume;
    }
    public void setResume(Resume resume) {
        this.resume = resume;
    }
}
