package com.comlinkinc.medicus.retrofit.models.pojo.PatientList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrPtList {

    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("p_name")
    @Expose
    private String pName;
    @SerializedName("p_email")
    @Expose
    private String pEmail;
    @SerializedName("p_phone")
    @Expose
    private String pPhone;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("p_pp_path")
    @Expose
    private String pPpPath;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("q_complete")
    @Expose
    private String qComplete;
    @SerializedName("created_datetime")
    @Expose
    private String createdDatetime;
    @SerializedName("did")
    @Expose
    private String did;
    @SerializedName("doctor_email")
    @Expose
    private String doctorEmail;
    @SerializedName("doctor_name")
    @Expose
    private String doctorName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPName() {
        return pName;
    }

    public void setPName(String pName) {
        this.pName = pName;
    }

    public String getPEmail() {
        return pEmail;
    }

    public void setPEmail(String pEmail) {
        this.pEmail = pEmail;
    }

    public String getPPhone() {
        return pPhone;
    }

    public void setPPhone(String pPhone) {
        this.pPhone = pPhone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPPpPath() {
        return pPpPath;
    }

    public void setPPpPath(String pPpPath) {
        this.pPpPath = pPpPath;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getQComplete() {
        return qComplete;
    }

    public void setQComplete(String qComplete) {
        this.qComplete = qComplete;
    }

    public String getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDoctorEmail() {
        return doctorEmail;
    }

    public void setDoctorEmail(String doctorEmail) {
        this.doctorEmail = doctorEmail;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

}