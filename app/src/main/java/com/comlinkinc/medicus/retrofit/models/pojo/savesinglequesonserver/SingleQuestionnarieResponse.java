package com.comlinkinc.medicus.retrofit.models.pojo.savesinglequesonserver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  27 Jun 2018
 ***********************************************************************/

public class SingleQuestionnarieResponse {
    @SerializedName("qa_id")
    @Expose
    private String qaId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("sqa_id")
    @Expose
    private String sqaId;

    public String getQaId() {
        return qaId;
    }

    public void setQaId(String qaId) {
        this.qaId = qaId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSqaId() {
        return sqaId;
    }

    public void setSqaId(String sqaId) {
        this.sqaId = sqaId;
    }
}
