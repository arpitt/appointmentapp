package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.userinforesponse.UserInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  28 Dec 2017
 ***********************************************************************/

public interface UserInformationInterface {
    @GET("users.info")
    Call<UserInfoResponse> getUserInfoResponseCall(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Query("username") String userName);
}