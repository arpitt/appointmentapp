package com.comlinkinc.medicus.retrofit.models.pojo.setdescription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jun 2018
 ***********************************************************************/

public class SetDescriptionResponse {
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
