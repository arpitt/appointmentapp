package com.comlinkinc.medicus.retrofit.models.pojo.groupinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jun 2018
 ***********************************************************************/

public class LastMessage {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("u")
    @Expose
    private U_ u;
    @SerializedName("mentions")
    @Expose
    private List<Object> mentions = null;
    @SerializedName("channels")
    @Expose
    private List<Object> channels = null;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("sandstormSessionId")
    @Expose
    private Object sandstormSessionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public U_ getU() {
        return u;
    }

    public void setU(U_ u) {
        this.u = u;
    }

    public List<Object> getMentions() {
        return mentions;
    }

    public void setMentions(List<Object> mentions) {
        this.mentions = mentions;
    }

    public List<Object> getChannels() {
        return channels;
    }

    public void setChannels(List<Object> channels) {
        this.channels = channels;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getSandstormSessionId() {
        return sandstormSessionId;
    }

    public void setSandstormSessionId(Object sandstormSessionId) {
        this.sandstormSessionId = sandstormSessionId;
    }
}
