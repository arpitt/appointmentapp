package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.groupinfo.GetGroupInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by sachink on 1/26/2018.
 */

public interface GroupInfoInterface {
    @GET("groups.info")
    Call<GetGroupInfoResponse> getGroupInfo(@Header("X-Auth-Token") String auth_token, @Header("X-User-Id") String user_id, @Query("roomId") String roomId);
}
