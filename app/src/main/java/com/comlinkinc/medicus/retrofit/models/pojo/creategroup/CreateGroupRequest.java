package com.comlinkinc.medicus.retrofit.models.pojo.creategroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  23 Jan 2018
 ***********************************************************************/

public class CreateGroupRequest {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("members[]")
    @Expose
    private String members;
    @SerializedName("readOnly")
    @Expose
    private Boolean readOnly;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }
}
