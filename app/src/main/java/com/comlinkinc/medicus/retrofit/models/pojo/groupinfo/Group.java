package com.comlinkinc.medicus.retrofit.models.pojo.groupinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sachink on 1/26/2018.
 */

public class Group {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("msgs")
    @Expose
    private Integer msgs;
    @SerializedName("u")
    @Expose
    private U u;
    @SerializedName("customFields")
    @Expose
    private CustomFields customFields;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("ro")
    @Expose
    private Boolean ro;
    @SerializedName("sysMes")
    @Expose
    private Boolean sysMes;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("lm")
    @Expose
    private String lm;
    @SerializedName("lastMessage")
    @Expose
    private LastMessage lastMessage;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("usernames")
    @Expose
    private List<String> usernames = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Integer getMsgs() {
        return msgs;
    }

    public void setMsgs(Integer msgs) {
        this.msgs = msgs;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public CustomFields getCustomFields() {
        return customFields;
    }

    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Boolean getRo() {
        return ro;
    }

    public void setRo(Boolean ro) {
        this.ro = ro;
    }

    public Boolean getSysMes() {
        return sysMes;
    }

    public void setSysMes(Boolean sysMes) {
        this.sysMes = sysMes;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLm() {
        return lm;
    }

    public void setLm(String lm) {
        this.lm = lm;
    }

    public LastMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

}
