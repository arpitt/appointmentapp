package com.comlinkinc.medicus.retrofit.models.pojo.starredmsg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  24 Jun 2018
 ***********************************************************************/

public class Message {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("t")
    @Expose
    private String t;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("ts")
    @Expose
    private String ts;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("u")
    @Expose
    private U u;
    @SerializedName("groupable")
    @Expose
    private Boolean groupable;
    @SerializedName("_updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("starred")
    @Expose
    private List<Starred> starred = null;
    @SerializedName("mentions")
    @Expose
    private List<Object> mentions = null;
    @SerializedName("channels")
    @Expose
    private List<Object> channels = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    public Boolean getGroupable() {
        return groupable;
    }

    public void setGroupable(Boolean groupable) {
        this.groupable = groupable;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Starred> getStarred() {
        return starred;
    }

    public void setStarred(List<Starred> starred) {
        this.starred = starred;
    }

    public List<Object> getMentions() {
        return mentions;
    }

    public void setMentions(List<Object> mentions) {
        this.mentions = mentions;
    }

    public List<Object> getChannels() {
        return channels;
    }

    public void setChannels(List<Object> channels) {
        this.channels = channels;
    }
}
