package com.comlinkinc.medicus.retrofit.models.pojo.invitemember;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  25 Jan 2018
 ***********************************************************************/

public class GroupAddMembersInviteRequest {
    @SerializedName("roomId")
    @Expose
    private String roomId;
    @SerializedName("userId")
    @Expose
    private String userId;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
