package com.comlinkinc.medicus.retrofit.models.interfaces;

import com.comlinkinc.medicus.retrofit.models.pojo.questionnaries.QuestionnairesResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface QuestionnairesInterface {
    @FormUrlEncoded
    @POST("mapi/v1/question_response")
    Call<QuestionnairesResponse> getQuestionnaire(@Header("X-Auth-Token") String authToken,
                                                  @Header("X-User-Id") String userId,
                                                  @Field("pid") String patientId);

}
