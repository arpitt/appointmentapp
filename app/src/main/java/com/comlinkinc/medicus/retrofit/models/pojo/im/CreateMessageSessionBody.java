package com.comlinkinc.medicus.retrofit.models.pojo.im;

public class CreateMessageSessionBody {

    public CreateMessageSessionBody(String username) {
        this.username = username;
    }

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
