package com.comlinkinc.medicus.retrofit.models.pojo.getpresence;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  27 Feb 2018
 ***********************************************************************/

public class GetPresenceResponse {
    @SerializedName("presence")
    @Expose
    private String presence;
    @SerializedName("connectionStatus")
    @Expose
    private String connectionStatus;
    @SerializedName("lastLogin")
    @Expose
    private String lastLogin;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getPresence() {
        return presence;
    }

    public void setPresence(String presence) {
        this.presence = presence;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
