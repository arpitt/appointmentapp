package com.comlinkinc.medicus.retrofit.models.interfaces;


import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.ChatHistoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  27 Dec 2017
 ***********************************************************************/

public interface ChannelsChatHistoryInterface {
    @GET("channels.history")
    Call<ChatHistoryResponse> getChatHistoryList(@Header("X-Auth-Token") String auth_token,
                                                 @Header("X-User-Id") String user_id,
                                                 @Query("roomId") String roomId,
                                                 @Query("count") String count);
}
