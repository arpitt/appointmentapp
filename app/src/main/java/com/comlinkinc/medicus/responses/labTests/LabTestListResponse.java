package com.comlinkinc.medicus.responses.labTests;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabTestListResponse {

@SerializedName("lab_test_list")
@Expose
private List<LabTestList> labTestList = null;
@SerializedName("status")
@Expose
private String status;

public List<LabTestList> getLabTestList() {
return labTestList;
}

public void setLabTestList(List<LabTestList> labTestList) {
this.labTestList = labTestList;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}