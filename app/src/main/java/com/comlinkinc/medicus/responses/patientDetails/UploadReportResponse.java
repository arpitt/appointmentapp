package com.comlinkinc.medicus.responses.patientDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadReportResponse {

@SerializedName("rt_name")
@Expose
private String rtName;
@SerializedName("status")
@Expose
private String status;
@SerializedName("message")
@Expose
private String message;
@SerializedName("path")
@Expose
private String path;
@SerializedName("last_insert_id")
@Expose
private Integer lastInsertId;

public String getRtName() {
return rtName;
}

public void setRtName(String rtName) {
this.rtName = rtName;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getPath() {
return path;
}

public void setPath(String path) {
this.path = path;
}

public Integer getLastInsertId() {
return lastInsertId;
}

public void setLastInsertId(Integer lastInsertId) {
this.lastInsertId = lastInsertId;
}

}