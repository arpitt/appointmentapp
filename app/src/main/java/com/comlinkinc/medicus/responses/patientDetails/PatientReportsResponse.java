package com.comlinkinc.medicus.responses.patientDetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientReportsResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("base_url")
@Expose
private String baseUrl;
@SerializedName("report_list")
@Expose
private List<ReportList> reportList = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getBaseUrl() {
return baseUrl;
}

public void setBaseUrl(String baseUrl) {
this.baseUrl = baseUrl;
}

public List<ReportList> getReportList() {
return reportList;
}

public void setReportList(List<ReportList> reportList) {
this.reportList = reportList;
}

}