package com.comlinkinc.medicus.responses.doctorDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

@SerializedName("did")
@Expose
private String did;
@SerializedName("d_name")
@Expose
private String dName;
@SerializedName("d_email")
@Expose
private String dEmail;
@SerializedName("d_degree")
@Expose
private String dDegree;
@SerializedName("d_phone")
@Expose
private String dPhone;
@SerializedName("d_pp_path")
@Expose
private String dPpPath;
@SerializedName("hospital_name")
@Expose
private String hospitalName;
@SerializedName("hospital_address")
@Expose
private String hospitalAddress;
@SerializedName("status")
@Expose
private String status;
@SerializedName("sp_name")
@Expose
private String spName;
@SerializedName("sp_id")
@Expose
private String spId;

public String getDid() {
return did;
}

public void setDid(String did) {
this.did = did;
}

public String getDName() {
return dName;
}

public void setDName(String dName) {
this.dName = dName;
}

public String getDEmail() {
return dEmail;
}

public void setDEmail(String dEmail) {
this.dEmail = dEmail;
}

public String getDDegree() {
return dDegree;
}

public void setDDegree(String dDegree) {
this.dDegree = dDegree;
}

public String getDPhone() {
return dPhone;
}

public void setDPhone(String dPhone) {
this.dPhone = dPhone;
}

public String getDPpPath() {
return dPpPath;
}

public void setDPpPath(String dPpPath) {
this.dPpPath = dPpPath;
}

public String getHospitalName() {
return hospitalName;
}

public void setHospitalName(String hospitalName) {
this.hospitalName = hospitalName;
}

public String getHospitalAddress() {
return hospitalAddress;
}

public void setHospitalAddress(String hospitalAddress) {
this.hospitalAddress = hospitalAddress;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getSpName() {
return spName;
}

public void setSpName(String spName) {
this.spName = spName;
}

public String getSpId() {
return spId;
}

public void setSpId(String spId) {
this.spId = spId;
}

}