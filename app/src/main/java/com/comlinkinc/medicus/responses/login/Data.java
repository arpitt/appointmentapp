package com.comlinkinc.medicus.responses.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

@SerializedName("userType")
@Expose
private String userType;
@SerializedName("userId")
@Expose
private String userId;
@SerializedName("authToken")
@Expose
private String authToken;
@SerializedName("chatAuthToken")
@Expose
private String chatAuthToken;
@SerializedName("chatUserId")
@Expose
private String chatUserId;
@SerializedName("userDetails")
@Expose
private UserDetails userDetails;

public String getUserType() {
return userType;
}

public void setUserType(String userType) {
this.userType = userType;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getAuthToken() {
return authToken;
}

public void setAuthToken(String authToken) {
this.authToken = authToken;
}

public String getChatAuthToken() {
return chatAuthToken;
}

public void setChatAuthToken(String chatAuthToken) {
this.chatAuthToken = chatAuthToken;
}

public String getChatUserId() {
return chatUserId;
}

public void setChatUserId(String chatUserId) {
this.chatUserId = chatUserId;
}

public UserDetails getUserDetails() {
return userDetails;
}

public void setUserDetails(UserDetails userDetails) {
this.userDetails = userDetails;
}

}