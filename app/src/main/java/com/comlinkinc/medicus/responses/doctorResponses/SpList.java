package com.comlinkinc.medicus.responses.doctorResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpList {

@SerializedName("sp_id")
@Expose
private String spId;
@SerializedName("sp_name")
@Expose
private String spName;
@SerializedName("status")
@Expose
private String status;

public String getSpId() {
return spId;
}

public void setSpId(String spId) {
this.spId = spId;
}

public String getSpName() {
return spName;
}

public void setSpName(String spName) {
this.spName = spName;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}