package com.comlinkinc.medicus.responses.patientRegistration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientRegistrationResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("message")
@Expose
private String message;
@SerializedName("auth_token")
@Expose
private String authToken;
@SerializedName("device_token")
@Expose
private String deviceToken;
@SerializedName("pid")
@Expose
private Integer pid;
@SerializedName("p_name")
@Expose
private String pName;
@SerializedName("p_email")
@Expose
private String pEmail;
@SerializedName("pp_path")
@Expose
private String ppPath;
@SerializedName("p_phone")
@Expose
private String pPhone;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("chatAuthToken")
@Expose
private String chatAuthToken;
@SerializedName("chatUserId")
@Expose
private String chatUserId;
@SerializedName("created_datetime")
@Expose
private Integer createdDatetime;
@SerializedName("dob")
@Expose
private String dob;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getAuthToken() {
return authToken;
}

public void setAuthToken(String authToken) {
this.authToken = authToken;
}

public String getDeviceToken() {
return deviceToken;
}

public void setDeviceToken(String deviceToken) {
this.deviceToken = deviceToken;
}

public Integer getPid() {
return pid;
}

public void setPid(Integer pid) {
this.pid = pid;
}

public String getPName() {
return pName;
}

public void setPName(String pName) {
this.pName = pName;
}

public String getPEmail() {
return pEmail;
}

public void setPEmail(String pEmail) {
this.pEmail = pEmail;
}

public String getPpPath() {
return ppPath;
}

public void setPpPath(String ppPath) {
this.ppPath = ppPath;
}

public String getPPhone() {
return pPhone;
}

public void setPPhone(String pPhone) {
this.pPhone = pPhone;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getChatAuthToken() {
return chatAuthToken;
}

public void setChatAuthToken(String chatAuthToken) {
this.chatAuthToken = chatAuthToken;
}

public String getChatUserId() {
return chatUserId;
}

public void setChatUserId(String chatUserId) {
this.chatUserId = chatUserId;
}

public Integer getCreatedDatetime() {
return createdDatetime;
}

public void setCreatedDatetime(Integer createdDatetime) {
this.createdDatetime = createdDatetime;
}

public String getDob() {
return dob;
}

public void setDob(String dob) {
this.dob = dob;
}

}