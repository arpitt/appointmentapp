package com.comlinkinc.medicus.responses.patientDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportList {

@SerializedName("report_id")
@Expose
private String reportId;
@SerializedName("rt_id")
@Expose
private String rtId;
@SerializedName("report_type")
@Expose
private String reportType;
@SerializedName("uploaded_by")
@Expose
private String uploadedBy;
@SerializedName("uploaded_time")
@Expose
private String uploadedTime;
@SerializedName("file_path")
@Expose
private String filePath;
@SerializedName("deletable")
@Expose
private Integer deletable;

public String getReportId() {
return reportId;
}

public void setReportId(String reportId) {
this.reportId = reportId;
}

public String getRtId() {
return rtId;
}

public void setRtId(String rtId) {
this.rtId = rtId;
}

public String getReportType() {
return reportType;
}

public void setReportType(String reportType) {
this.reportType = reportType;
}

public String getUploadedBy() {
return uploadedBy;
}

public void setUploadedBy(String uploadedBy) {
this.uploadedBy = uploadedBy;
}

public String getUploadedTime() {
return uploadedTime;
}

public void setUploadedTime(String uploadedTime) {
this.uploadedTime = uploadedTime;
}

public String getFilePath() {
return filePath;
}

public void setFilePath(String filePath) {
this.filePath = filePath;
}

public Integer getDeletable() {
return deletable;
}

public void setDeletable(Integer deletable) {
this.deletable = deletable;
}

}