package com.comlinkinc.medicus.responses.patientRegistration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthToken {

@SerializedName("rc_register")
@Expose
private Integer rcRegister;
@SerializedName("rc_authToken")
@Expose
private String rcAuthToken;
@SerializedName("rc_userid")
@Expose
private String rcUserid;
@SerializedName("error_message")
@Expose
private String errorMessage;

public Integer getRcRegister() {
return rcRegister;
}

public void setRcRegister(Integer rcRegister) {
this.rcRegister = rcRegister;
}

public String getRcAuthToken() {
return rcAuthToken;
}

public void setRcAuthToken(String rcAuthToken) {
this.rcAuthToken = rcAuthToken;
}

public String getRcUserid() {
return rcUserid;
}

public void setRcUserid(String rcUserid) {
this.rcUserid = rcUserid;
}

public String getErrorMessage() {
return errorMessage;
}

public void setErrorMessage(String errorMessage) {
this.errorMessage = errorMessage;
}

}