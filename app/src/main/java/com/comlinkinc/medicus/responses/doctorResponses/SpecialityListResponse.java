package com.comlinkinc.medicus.responses.doctorResponses;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpecialityListResponse {

@SerializedName("status")
@Expose
private String status;
@SerializedName("sp_list")
@Expose
private List<SpList> spList = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public List<SpList> getSpList() {
return spList;
}

public void setSpList(List<SpList> spList) {
this.spList = spList;
}

}