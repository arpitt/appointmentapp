package com.comlinkinc.medicus.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.SplashScreenActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.retrofit.models.pojo.notification.MessageBodyResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.FCMTokenUpdateInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.fcm.FCMTokenResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  21 Dec 2017
 ***********************************************************************/

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "nMyFirebaseMsgService";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SharedPreferences sharedpreferences = getSharedPreferences(Constants.COMLINK_PREFS, Context.MODE_PRIVATE);
        FCMTokenUpdateInterface fcmTokenUpdateInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(FCMTokenUpdateInterface.class);
        fcmTokenUpdateInterface.updateFCMToken(sharedpreferences.getString(Prefs.PREF_AUTH_TOKEN, ""), sharedpreferences.getString(Prefs.PREF_USER_ID, ""), "gcm", s, getResources().getString(R.string.app_name)).enqueue(new Callback<FCMTokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<FCMTokenResponse> call, @NonNull Response<FCMTokenResponse> response) {
                Log.i(TAG, "FCM Token updated");
            }

            @Override
            public void onFailure(@NonNull Call<FCMTokenResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "Could not update FCM token");
            }
        });
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        Prefs.setSharedPreferenceString(MyApplication.getInstance(), Prefs.PREF_NOTIFICATION_DATA, data.get("app_date"));
//        Prefs.IS_FROM_NOTIFICATION = true;

        sendBackgroundNotification(remoteMessage);
    }

    private void sendBackgroundNotification(RemoteMessage remoteMessage) {
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        Prefs.IS_FROM_NOTIFICATION = true;
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,0);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Chat Notifications", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Chat Notifications");
            notificationChannel.enableLights(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        MessageBodyResponse msg = new Gson().fromJson(remoteMessage.getData().get("ejson"), MessageBodyResponse.class);
        if((msg.getMessageType() != null && msg.getMessageType().equals("jitsi_call_started")) || (remoteMessage.getData().get("message") != null && remoteMessage.getData().containsKey("message") && remoteMessage.getData().get("message").contains("https://newvideo.mvoipctsi.com"))) {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("Incoming call...")
                    .setAutoCancel(true)
                    .setSound(defaultSound)
                    .setContentText(Html.fromHtml("Incoming video call from " + remoteMessage.getData().get("title")))
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setPriority(Notification.PRIORITY_MAX);
            notificationManager.notify(1, notificationBuilder.build());
        } else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle(String.format(remoteMessage.getData().get("summaryText").replace("%n%", "%s"), remoteMessage.getData().get("msgcnt")))
                    .setAutoCancel(true)
                    .setSound(defaultSound)
                    .setContentText(Html.fromHtml("<b>" + remoteMessage.getData().get("title") + "</b> " + remoteMessage.getData().get("message")))
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setPriority(Notification.PRIORITY_DEFAULT);
            notificationManager.notify(1, notificationBuilder.build());
        }

    }

}