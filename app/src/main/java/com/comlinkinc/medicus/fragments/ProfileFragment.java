package com.comlinkinc.medicus.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DashboardActivity;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.forgotPassword.ForgotPasswordResponse;
import com.comlinkinc.medicus.responses.patientDetails.PatientProfileResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.Prefs;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private static final int RESULT_SELECT_IMAGE = 1;
    private Context mContext;
    private TextView tVUserName;
    private EditText tVFullName;
    private EditText tVPhoneNumber;
    private EditText tVEmail;
    private EditText tVDob;
    private TextView btnEdit;
    private Button btnUpdateProfile;
    private Button btnMyReports;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener selectedStartDate;
    private ProgressBar pBLoading;
    private RadioButton rbMale, rbFemale;
    private LinearLayout lLGender;
    private TextView tVGender;
    private CircleImageView civProfileImage;
    private Dialog dialog;
    private AlertDialog alert = null;

    public ProfileFragment() {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getIds(view);
        setData();

        return view;
    }

    private void getIds(View view) {
        civProfileImage = view.findViewById(R.id.profile_image);
        civProfileImage.setOnClickListener(this);
        tVUserName = view.findViewById(R.id.txt_uname);
        btnEdit = view.findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(this);
        tVFullName = view.findViewById(R.id.txt_name);
        tVEmail = view.findViewById(R.id.txt_email);
        tVDob = view.findViewById(R.id.txt_dob);
        tVGender = (EditText) view.findViewById(R.id.txt_gender);
        lLGender = view.findViewById(R.id.layout_gender);
        rbMale = view.findViewById(R.id.rb_male);
        rbFemale = view.findViewById(R.id.rb_female);
        tVPhoneNumber = view.findViewById(R.id.txt_mob_no);

        pBLoading = view.findViewById(R.id.progress_profile);

        tVDob.setOnClickListener(this);

        tVFullName.setFocusable(false);
        tVFullName.setFocusableInTouchMode(false);
        tVFullName.setClickable(false);

        tVPhoneNumber.setFocusable(false);
        tVPhoneNumber.setFocusableInTouchMode(false);
        tVPhoneNumber.setClickable(false);

        btnUpdateProfile = view.findViewById(R.id.btn_update_profile);
        btnUpdateProfile.setOnClickListener(this);

        btnMyReports = view.findViewById(R.id.btn_my_reports);
        btnMyReports.setOnClickListener(this);
        btnMyReports.setVisibility(View.VISIBLE);

        selectedStartDate = (view1, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDOBDateLabel();
        };
    }

    private void setData() {
        Glide.with(mContext).load(Constants.userDetails.getPpPath()).placeholder(R.drawable.profile).into(civProfileImage);
        tVUserName.setText(Constants.userDetails.getUserName());
        tVFullName.setText(Constants.userDetails.getName());
        tVEmail.setText(Constants.userDetails.getEmail());
        try {
            tVDob.setText(Constants.convertDate(Constants.userDetails.getDateOfBirth(), "yyyy-MM-dd", "dd-MM-yyyy"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tVGender.setText(Constants.userDetails.getGender());
        if (Constants.userDetails.getGender().equals("Male")) {
            rbMale.setChecked(true);
            rbFemale.setChecked(false);
        } else if (Constants.userDetails.getGender().equals("Female")) {
            rbMale.setChecked(false);
            rbFemale.setChecked(true);
        }
        tVPhoneNumber.setText(Constants.userDetails.getPhoneNumber());
        lLGender.setVisibility(View.GONE);
        tVGender.setVisibility(View.VISIBLE);
        btnMyReports.setVisibility(View.VISIBLE);
        ((DashboardActivity) mContext).updateProfileHeader();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_SELECT_IMAGE);
                break;
            case R.id.btn_edit:
                if (btnEdit.getText().toString().equals("Edit")) {
                    tVFullName.setClickable(true);
                    tVFullName.setFocusableInTouchMode(true);
                    tVFullName.setFocusable(true);
                    tVDob.setClickable(true);
                    tVDob.setFocusableInTouchMode(true);
                    tVDob.setFocusable(true);
                    tVPhoneNumber.setClickable(true);
                    tVPhoneNumber.setFocusableInTouchMode(true);
                    tVPhoneNumber.setFocusable(true);
                    tVGender.setVisibility(View.GONE);
                    lLGender.setVisibility(View.VISIBLE);
                    btnEdit.setText("Cancel");
                    btnMyReports.setVisibility(View.GONE);
                    btnUpdateProfile.setVisibility(View.VISIBLE);
                } else if (btnEdit.getText().toString().equals("Cancel")) {
                    tVFullName.setClickable(false);
                    tVFullName.setFocusableInTouchMode(false);
                    tVFullName.setFocusable(false);
                    tVDob.setClickable(false);
                    tVDob.setFocusableInTouchMode(false);
                    tVDob.setFocusable(false);
                    tVPhoneNumber.setClickable(false);
                    tVPhoneNumber.setFocusableInTouchMode(false);
                    tVPhoneNumber.setFocusable(false);
                    tVGender.setVisibility(View.VISIBLE);
                    lLGender.setVisibility(View.GONE);
                    btnEdit.setText("Edit");
                    btnMyReports.setVisibility(View.VISIBLE);
                    btnUpdateProfile.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_update_profile:
                String fullName = tVFullName.getText().toString().trim();
                String dob = tVDob.getText().toString().trim();
                String phoneNumber = tVPhoneNumber.getText().toString().trim();
                String gender = "0";
                if (rbMale.isChecked()) gender = "1";
                else if (rbFemale.isChecked()) gender = "2";
                boolean isAllDataCorrect = true;
                if (fullName.isEmpty()) {
                    tVFullName.setError("Required");
                    isAllDataCorrect = false;
                } else {
                    tVFullName.setError(null);
                }
                if (dob.isEmpty()) {
                    tVDob.setError("Required");
                    isAllDataCorrect = false;
                } else {
                    tVDob.setError(null);
                }
                if (gender.equals("0")) {
                    Toast.makeText(mContext, "Please select a gender!", Toast.LENGTH_SHORT).show();
                    isAllDataCorrect = false;
                }

                if (isAllDataCorrect) {
                    pBLoading.setVisibility(View.VISIBLE);
                    PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
                    try {
                        patientInterface.updatePatientDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId, fullName, phoneNumber, gender, Constants.convertDate(dob, "dd-MM-yyyy", "yyyy-MM-dd")).enqueue(new Callback<ForgotPasswordResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                                pBLoading.setVisibility(View.GONE);
                                if (response.body() != null) {
                                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                                        dialogUpdateProgress("Hooray!", response.body().getMessage());
                                        tVFullName.setClickable(false);
                                        tVFullName.setFocusableInTouchMode(false);
                                        tVFullName.setFocusable(false);
                                        tVDob.setClickable(false);
                                        tVDob.setFocusableInTouchMode(false);
                                        tVDob.setFocusable(false);
                                        tVPhoneNumber.setClickable(false);
                                        tVPhoneNumber.setFocusableInTouchMode(false);
                                        tVPhoneNumber.setFocusable(false);
                                        tVGender.setVisibility(View.VISIBLE);
                                        lLGender.setVisibility(View.GONE);
                                        btnEdit.setText("Edit");
                                        btnMyReports.setVisibility(View.VISIBLE);
                                        btnUpdateProfile.setVisibility(View.GONE);
                                        getUserDetails();
                                    } else {
                                        dialogUpdateProgress("Oops!", response.body().getMessage());
                                        setData();
                                    }
                                } else {
                                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                                pBLoading.setVisibility(View.GONE);
                                CommonUtils.failureMessage(mContext, true);
                            }
                        });
                    } catch (ParseException e) {
                        pBLoading.setVisibility(View.GONE);
                        Toast.makeText(mContext, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btn_my_reports:
                ((DashboardActivity) mContext).replaceFragments(ReportsFragment.class, null, getResources().getString(R.string.lbl_reports), true);
                break;
            case R.id.txt_dob:
                if (btnEdit.getText().toString().equalsIgnoreCase("Cancel")) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.DialogTheme, selectedStartDate, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));
                    myCalendar.set(myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
                break;
        }
    }

    private void getUserDetails() {
        pBLoading.setVisibility(View.VISIBLE);
        PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
        patientInterface.getPatientDetails(Constants.XAuthToken, Constants.XUserId, Constants.XUserId).enqueue(new Callback<PatientProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<PatientProfileResponse> call, @NonNull Response<PatientProfileResponse> response) {
                pBLoading.setVisibility(View.GONE);
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    Constants.userDetails.setName(response.body().getDetails().getPName());
                    Constants.userDetails.setPatientId(response.body().getDetails().getPid());
                    Constants.userDetails.setEmail(response.body().getDetails().getPEmail());
                    Constants.userDetails.setPhoneNumber(response.body().getDetails().getPPhone());
                    Constants.userDetails.setDateOfBirth(response.body().getDetails().getDob());
                    Constants.userDetails.setPpPath(Constants.MAIN_URL + "/" + response.body().getDetails().getPPpPath());
                    Constants.userDetails.setGender(response.body().getDetails().getGender());
                    Constants.userDetails.setQComplete(response.body().getDetails().getQComplete());
                    Constants.userDetails.setUserName(response.body().getDetails().getPEmail());
                    Constants.userDetails.setIsVerified(response.body().getDetails().getIsVerified());

                    setData();
                    tVFullName.setClickable(false);
                    tVFullName.setFocusableInTouchMode(false);
                    tVFullName.setFocusable(false);
                    tVDob.setClickable(false);
                    tVDob.setFocusableInTouchMode(false);
                    tVDob.setFocusable(false);
                    tVPhoneNumber.setClickable(false);
                    tVPhoneNumber.setFocusableInTouchMode(false);
                    tVPhoneNumber.setFocusable(false);
                    tVGender.setVisibility(View.VISIBLE);
                    lLGender.setVisibility(View.GONE);
                    btnEdit.setText("Edit");
                    btnMyReports.setVisibility(View.VISIBLE);
                    btnUpdateProfile.setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PatientProfileResponse> call, @NonNull Throwable t) {
                pBLoading.setVisibility(View.GONE);
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }

    private void loadProfile(String url) {
        Glide.with(mContext).load(url)
                .apply(new RequestOptions()
                        .error(R.drawable.profile)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .priority(Priority.HIGH))
                .into(civProfileImage);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == RESULT_SELECT_IMAGE) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

                    loadProfile(uri.toString());
                    String fileName = Prefs.getSharedPreferenceString(getContext(), Constants.userDetails.getPatientId(), "") + "_" + System.currentTimeMillis();
                    File f = new File(getContext().getCacheDir(), fileName + ".png");
                    f.createNewFile();

                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                    byte[] bitmapData = bos.toByteArray();

                    FileOutputStream fos = new FileOutputStream(f);
                    fos.write(bitmapData);
                    fos.flush();
                    fos.close();

                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), f);
                    MultipartBody.Part part = MultipartBody.Part.createFormData("file", f.getName(), fileReqBody);
                    pBLoading.setVisibility(View.VISIBLE);
                    PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
                    patientInterface.uploadProfilePhoto(Constants.XAuthToken, Constants.XUserId, part).enqueue(new Callback<ForgotPasswordResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<ForgotPasswordResponse> call, @NonNull Response<ForgotPasswordResponse> response) {
                            pBLoading.setVisibility(View.GONE);
                            if (response.body() != null) {
                                dialogUpdateProgress("Profile Picture", response.body().getMessage());
                                if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200))
                                    getUserDetails();
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ForgotPasswordResponse> call, @NonNull Throwable t) {
                            pBLoading.setVisibility(View.GONE);
                            CommonUtils.failureMessage(mContext, true);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void dialogUpdateProgress(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", (dialog, which) -> {
            if (alert != null)
                alert.dismiss();
        });
        alert = builder.create();
        alert.show();
    }

    private void updateDOBDateLabel() {
        String myFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tVDob.setText(sdf.format(myCalendar.getTime()));
        tVDob.setError(null);
    }
}
