package com.comlinkinc.medicus.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetPinMessageInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetStarMessageInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetUnPinMessageInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetUnStarMessageInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Message;
import com.comlinkinc.medicus.retrofit.models.pojo.setpinmsg.SetPinnedMsgResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.unpinmsg.UnPinMsgResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.listener.SimpleListener;
import com.rocketchat.core.RocketChatAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**********************************************************************
 * Created by   -  Tushar Patil
 * Organization -  QuicSolv Technologies Pvt.Ltd
 * Date         -  07 Mar 2018
 ***********************************************************************/

public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private RocketChatAPI.ChatRoom room;
    private LinearLayout updateMessageLayout;
    private Message message;
    private EditText edttxtMessage;
    private SetPinMessageInterface setPinMessageInterface;
    private SetStarMessageInterface setStarMessageInterface;
    private SetUnPinMessageInterface setUnPinMessageInterface;
    private SetUnStarMessageInterface setUnStarMessageInterface;
    private TextView txtPin, txtStar;


    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public BottomSheetFragment(RocketChatAPI.ChatRoom room, Message message) {
        // Required empty public constructor
        this.room = room;
        this.message = message;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);

        setPinMessageInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetPinMessageInterface.class);
        setStarMessageInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetStarMessageInterface.class);
        setUnPinMessageInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetUnPinMessageInterface.class);
        setUnStarMessageInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetUnStarMessageInterface.class);

        txtPin = view.findViewById(R.id.txt_pin);
        txtStar = view.findViewById(R.id.txt_star);

        LinearLayout deleteMessage = view.findViewById(R.id.delete_message);
        deleteMessage.setOnClickListener(this);

        LinearLayout editMessage = view.findViewById(R.id.edit_message);
        editMessage.setOnClickListener(this);

        LinearLayout pinMessage = view.findViewById(R.id.pin_message);
        pinMessage.setOnClickListener(this);

        LinearLayout starMessage = view.findViewById(R.id.star_message);
        starMessage.setOnClickListener(this);

        LinearLayout cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        updateMessageLayout = view.findViewById(R.id.update_message_layout);
        updateMessageLayout.setVisibility(View.GONE);

        Button btnUpdate = view.findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(this);

        edttxtMessage = view.findViewById(R.id.edttxt_msg);
        edttxtMessage.setText(message.getMsg());

        if (message.getPinned() != null && message.getPinned().equals(true)) {
            txtPin.setText("Unpin Message");
        }

        if (message.getStarred() != null) {
            txtStar.setText("Unstar message");
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete_message:
                updateMessageLayout.setVisibility(View.GONE);
                room.deleteMessage(message.getId(), new SimpleListener() {
                    @Override
                    public void callback(Boolean success, ErrorObject error) {
                        if (success) {
                            System.out.println("Message deleted successfully");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Message deleted successfully", Toast.LENGTH_SHORT).show();
                                    Fragment frg = null;
                                    frg = getActivity().getSupportFragmentManager().findFragmentByTag("ChatFragment");
                                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.detach(frg);
                                    ft.attach(frg);
                                    ft.commit();
                                    dismiss();
                                }
                            });
                        }
                    }
                });
                break;

            case R.id.edit_message:
                updateMessageLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.pin_message:
                updateMessageLayout.setVisibility(View.GONE);
                dismiss();
                if (txtPin.getText().toString().trim().equalsIgnoreCase("Unpin Message")) {
                    setUnPinMessage(message.getId());
                } else {
                    setPinMessage(message.getId());
                }
                break;

            case R.id.star_message:
                updateMessageLayout.setVisibility(View.GONE);
                dismiss();
                if (txtStar.getText().toString().trim().equalsIgnoreCase("Unstar Message")) {
                    setUnStarMessage(message.getId());
                } else {
                    setStarMessage(message.getId());
                }

                break;

            case R.id.cancel:
                updateMessageLayout.setVisibility(View.GONE);
                dismiss();
                break;

            case R.id.btn_update:
                updateMessageLayout.setVisibility(View.GONE);
                String updatedMsg = "";
                if (!edttxtMessage.getText().toString().trim().equals("")) {
                    updatedMsg = edttxtMessage.getText().toString();
                }
                room.updateMessage(message.getId(), updatedMsg, new SimpleListener() {
                    @Override
                    public void callback(Boolean success, final ErrorObject error) {
                        if (success != null && success) {
//                            dismiss();
                            System.out.println("Message updated successfully");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MyApplication.getInstance(), "Message updated successfully", Toast.LENGTH_SHORT).show();
                                    Fragment frg = null;
                                    frg = getActivity().getSupportFragmentManager().findFragmentByTag("ChatFragment");
                                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.detach(frg);
                                    ft.attach(frg);
                                    ft.commit();
                                    dismiss();
                                }
                            });
                        } else {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismiss();
                                    Toast.makeText(MyApplication.getInstance(), error.getReason(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });
                break;
        }
    }

    private void setPinMessage(String messageId) {
        setPinMessageInterface.setPinMessage(Constants.chatAuthToken, Constants.chatUserId, messageId).enqueue(new Callback<SetPinnedMsgResponse>() {
            @Override
            public void onResponse(@NonNull Call<SetPinnedMsgResponse> call, @NonNull Response<SetPinnedMsgResponse> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Toast.makeText(MyApplication.getInstance(), "Pinned message successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SetPinnedMsgResponse> call, @NonNull Throwable t) {
                Timber.e(t);
            }
        });
    }

    private void setStarMessage(String messageId) {
        setStarMessageInterface.setStarMessage(Constants.chatAuthToken, Constants.chatUserId, messageId).enqueue(new Callback<SetPinnedMsgResponse>() {
            @Override
            public void onResponse(@NonNull Call<SetPinnedMsgResponse> call, @NonNull Response<SetPinnedMsgResponse> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Toast.makeText(MyApplication.getInstance(), "Star message successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SetPinnedMsgResponse> call, @NonNull Throwable t) {
                Timber.e(t);
            }
        });
    }

    private void setUnPinMessage(String messageId) {
        setUnPinMessageInterface.setUnPinMessage(Constants.chatAuthToken, Constants.chatUserId, messageId).enqueue(new Callback<UnPinMsgResponse>() {
            @Override
            public void onResponse(@NonNull Call<UnPinMsgResponse> call, @NonNull Response<UnPinMsgResponse> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Toast.makeText(MyApplication.getInstance(), "Unpin message successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UnPinMsgResponse> call, @NonNull Throwable t) {
                Timber.e(t);
            }
        });
    }

    private void setUnStarMessage(String messageId) {
        setUnStarMessageInterface.setUnStarMessage(Constants.chatAuthToken, Constants.chatUserId, messageId).enqueue(new Callback<UnPinMsgResponse>() {
            @Override
            public void onResponse(@NonNull Call<UnPinMsgResponse> call, @NonNull Response<UnPinMsgResponse> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Toast.makeText(MyApplication.getInstance(), "Unstar message successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UnPinMsgResponse> call, @NonNull Throwable t) {
                Timber.e(t);
            }
        });
    }
}