package com.comlinkinc.medicus.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DashboardActivity;
import com.comlinkinc.medicus.activities.ReportUploadActivity;
import com.comlinkinc.medicus.adapters.PatientReportAdapter;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.patientDetails.PatientReportsResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.Constants;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReportsFragment extends Fragment implements View.OnClickListener {

    private final static int LOADING_DURATION = 2000;
    private Context mContext;
    private RecyclerView rVReports;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout lLNoReports;
    private RelativeLayout rLToolbar;
    private Activity mActivity;
    private ProgressBar pBLoading;
    private Handler handler = new Handler();
    private boolean isInternetConnected = true;
    private PatientReportAdapter adapter = null;
    private String pid;
    private boolean hideToolbar = false;

    public ReportsFragment() {
        pid = Constants.XUserId;
    }

    public ReportsFragment(String pid) {
        this.pid = pid;
        hideToolbar = true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        if (mContext instanceof DashboardActivity) {
            mActivity = (DashboardActivity) mContext;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports, container, false);
        getIds(view);
        getReports();
        handler.postDelayed(new Runnable(){
            public void run(){
                getReports();
                handler.postDelayed(this, LOADING_DURATION);
            }
        }, LOADING_DURATION);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryLight), getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(this::getReports);
        return view;
    }

    private void getIds(View view) {
        pBLoading = view.findViewById(R.id.pBLoading);
        rLToolbar = view.findViewById(R.id.rLToolbar);
        if (hideToolbar) rLToolbar.setVisibility(View.GONE);
        Button btnUploadReportsToolbar = view.findViewById(R.id.btnUploadReportsToolbar);
        btnUploadReportsToolbar.setOnClickListener(this);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        rVReports = view.findViewById(R.id.rVReports);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rVReports.setLayoutManager(layoutManager);
        lLNoReports = view.findViewById(R.id.lLNoReports);
        Button btnUploadReportsNoData = view.findViewById(R.id.btnUploadReportsNoData);
        btnUploadReportsNoData.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUploadReportsNoData:
            case R.id.btnUploadReportsToolbar:
                Dexter.withContext(mContext).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        startActivity(new Intent(mContext, ReportUploadActivity.class));
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        if (permissionDeniedResponse.isPermanentlyDenied()) {
                            Toast.makeText(mContext, "You'll need to allow us to write to storage from the phone settings.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Can't download file without permission.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).onSameThread().check();
                break;
        }
    }

    private void getReports() {
        if (adapter == null) pBLoading.setVisibility(View.VISIBLE);
        PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
        patientInterface.getReports(Constants.XAuthToken, Constants.XUserId, pid).enqueue(new Callback<PatientReportsResponse>() {
            @Override
            public void onResponse(@NonNull Call<PatientReportsResponse> call, @NonNull Response<PatientReportsResponse> response) {
                isInternetConnected = true;
                pBLoading.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        lLNoReports.setVisibility(View.GONE);
                        if (response.body().getReportList().size() > 0) {
                            if (!hideToolbar)
                                rLToolbar.setVisibility(View.VISIBLE);
                            adapter = new PatientReportAdapter(mContext, response.body().getBaseUrl(), response.body().getReportList());
                            rVReports.setAdapter(adapter);
                        } else {
                            adapter = null;
                            rLToolbar.setVisibility(View.GONE);
                            lLNoReports.setVisibility(View.VISIBLE);
                        }
                    } else {
                        adapter = null;
                        lLNoReports.setVisibility(View.VISIBLE);
                    }
                } else {
                    adapter = null;
                    Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<PatientReportsResponse> call, @NonNull Throwable t) {
                adapter = null;
                swipeRefreshLayout.setRefreshing(false);
                pBLoading.setVisibility(View.GONE);
                isInternetConnected = CommonUtils.failureMessage(mContext, isInternetConnected);
            }
        });
    }

}
