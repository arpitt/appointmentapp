package com.comlinkinc.medicus.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.PatientDetailsActivity;
import com.comlinkinc.medicus.utils.MainActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.adapters.ChatApiAdapter;
import com.comlinkinc.medicus.progress.DotProgressBar;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.ChatHistoryInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.ChatSearchInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetUnreadCountZeroInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.ChatHistoryResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Message;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Starred;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.U;
import com.comlinkinc.medicus.retrofit.models.pojo.setunreadcountzero.SetUnreadCountZeroResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.google.gson.Gson;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.listener.TypingListener;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.adapter.FileAdapter;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.callback.MessageListener;
import com.rocketchat.core.factory.ChatRoomFactory;
import com.rocketchat.core.model.FileObject;
import com.rocketchat.core.model.RocketChatMessage;
import com.rocketchat.core.model.SubscriptionObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatDoctorFragment extends Fragment implements View.OnClickListener, MessageListener.SubscriptionListener, GetSubscriptionListener, TypingListener {
    private static final String IMAGE_DIRECTORY = "/medicus";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final String TAG = "ChatDoctorFragment";
    private RocketChatAPI.ChatRoom room;
    private ListView lVChat;
    private String prevMessageId = "";
    private List<Message> msgList = new ArrayList<>();
    private ChatApiAdapter chatNewAdapter;
    private EditText eTChat;
    private FrameLayout mainLayout;
    private DotProgressBar progressBar;
    private File file;
    private String livePhotoPath, liveVideoPath, imagePath, audioPath, videoPath;
    private int GALLERY = 1;
    private int AUDIO = 2;
    private int VIDEO = 3;
    private TextView txtIsTyping;
    private ProgressDialog pDialog;
    private SetUnreadCountZeroInterface setUnreadCountZeroInterface;
    private Uri mCapturedImageURI;
    private TextView layoutReadOnlyRoom;
    private TextView layoutJoinChannel;
    private LinearLayout layoutSendAttachBtn;
    private ChatSearchInterface chatSearchInterface;
    private LinearLayout layoutNoChatMsg;
    private String email = "";
    private Context mContext;
    private Activity mActivity;
    private String roomId, userName;

    public ChatDoctorFragment() {
        // Required empty public constructor
    }

    public ChatDoctorFragment(String pid, String email, String userName, String roomId) {
        this.email = email;
        this.userName = userName;
        this.roomId = roomId;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = (PatientDetailsActivity) mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        mainLayout = view.findViewById(R.id.mainLayout);
        ImageView Videocall = ((PatientDetailsActivity) mActivity).toolbar.findViewById(R.id.videocall);
        Videocall.setOnClickListener(this);
        if (email.equals("")) {
            Videocall.setVisibility(View.GONE);
        } else {
            Videocall.setVisibility(View.VISIBLE);
        }

        setUnreadCountZeroInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetUnreadCountZeroInterface.class);
        chatSearchInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(ChatSearchInterface.class);
        getIds(view);

        CommonUtils.hideKeyboard(mActivity);

        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if (Constants.client != null) {
            Constants.client.getSubscriptions(this);
        }

        fetchChatHistory();
            layoutReadOnlyRoom.setVisibility(View.GONE);
            layoutSendAttachBtn.setVisibility(View.VISIBLE);
            layoutJoinChannel.setVisibility(View.GONE);
        eTChat.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        // Identifier of the action. This will be either the identifier you supplied,
                        // or EditorInfo.IME_NULL if being called due to the enter key being pressed.
                        if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN
                                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            if (room != null) {
                                if (!eTChat.getText().toString().trim().equals("")) {
                                    room.sendMessage(eTChat.getText().toString());
                                    eTChat.setText("");
                                    CommonUtils.hideKeyboard(getActivity());
                                } else {
                                    Toast.makeText(MyApplication.getInstance(), "Please enter message", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (Constants.client != null) {
                                    Constants.client.getSubscriptions((PatientDetailsActivity) getActivity());
                                }
                            }
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                            // onTouch();
                            return true;
                        }
                        // Return true if you have consumed the action, else false.
                        return false;
                    }
                });

        setUnreadCountToZero(roomId);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getIds(View view) {
        progressBar = view.findViewById(R.id.progress_chat);
        lVChat = view.findViewById(R.id.listview_chat);
        eTChat = view.findViewById(R.id.edttxt_chat);
        txtIsTyping = view.findViewById(R.id.txt_typing);
        ImageView btnSend = view.findViewById(R.id.btn_send);
        ImageView btnAttach = view.findViewById(R.id.btn_attach);
        btnAttach.setOnClickListener(this);
        btnSend.setOnClickListener(this);

        layoutReadOnlyRoom = view.findViewById(R.id.layout_read_only_room);
        layoutJoinChannel = view.findViewById(R.id.layout_join_room);
        layoutSendAttachBtn = view.findViewById(R.id.layout_chat_attach_btn);
        layoutNoChatMsg = view.findViewById(R.id.layout_no_msg);

        EditText eTSearchMsg = view.findViewById(R.id.search_message);
        eTSearchMsg.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() >= 2) {
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        progressBar.setVisibility(View.VISIBLE);
                        searchChatMsg(cs.toString());
                    }, 1200);

                } else if (cs.length() == 0) {
                    fetchChatHistory();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        lVChat.setOnItemLongClickListener((parent, view1, position, id) -> {
            Message message = msgList.get(position);
            if (message.getT() == null) {
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment(room, message);
                bottomSheetFragment.show(((PatientDetailsActivity) mActivity).getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
            return true;
        });

        eTChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (room != null) {
                        room.sendIsTyping(true);
                    }
                } else {
                    if (room != null) {
                        room.sendIsTyping(false);
                    }
                }
            }
        });
    }

    private void fetchChatHistory() {
        ChatHistoryInterface chatHistoryInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(ChatHistoryInterface.class);
        chatHistoryInterface.getChatHistoryList(Constants.chatAuthToken, Constants.chatUserId, roomId).enqueue(new Callback<ChatHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChatHistoryResponse> call, @NonNull Response<ChatHistoryResponse> response) {
                msgList = new ArrayList<>();
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessages() != null && response.body().getMessages().size() > 0) {
                            layoutNoChatMsg.setVisibility(View.GONE);
                            msgList.addAll(response.body().getMessages());
                            progressBar.setVisibility(View.GONE);
                            setChatApiAdapter();
                            lVChat.invalidateViews();
                        } else {
                            layoutNoChatMsg.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
//                            onTouch();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChatHistoryResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.i("Failed: ", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }


    private void setChatApiAdapter() {
        mActivity.runOnUiThread(() -> {
            if (msgList != null && msgList.size() > 0) {
                Collections.reverse(msgList);
                chatNewAdapter = new ChatApiAdapter(MyApplication.getInstance(), msgList);
                lVChat.setAdapter(chatNewAdapter);
                chatNewAdapter.notifyDataSetChanged();
                lVChat.bringToFront();
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.videocall:
                if (room != null) {
                    room.sendMessage("Hi you have received a video call please click on *[Join Conference](https://newvideo.mvoipctsi.com/comlinkHznA3eWAhRcAoYemQ" + roomId + ")*");
                    Intent intentMain = new Intent(mContext, MainActivity.class);
                    intentMain.putExtra(Constants.ROOM_ID, roomId);
                    intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intentMain);
                } else {
                    Toast.makeText(mActivity, "Could not find room!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_send:
                if (room != null) {
                    if (!eTChat.getText().toString().trim().equals("")) {
                        room.sendMessage(eTChat.getText().toString());
                        eTChat.setText("");
                    } else {
                        Toast.makeText(MyApplication.getInstance(), "Please enter message", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (Constants.client != null) {
                        Constants.client.getSubscriptions(this);
                    }
                }
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                break;
            case R.id.btn_attach:
                selectFile();
                break;
        }
    }

    private void selectFile() {
        showPictureDialog();
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Take photo", "Shoot video", "Select photo from gallery", "Select audio", "Select video"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            captureImage();
                            break;
                        case 1:
                            captureVideo();
                            break;
                        case 2:
                            choosePhotoFromGallery();
                            //openGallery();
                            break;
                        case 3:
                            chooseAudio();
                            break;
                        case 4:
                            chooseVideo();
                            break;
                    }
                });
        pictureDialog.show();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Image File name");
        mCapturedImageURI = mActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
        startActivityForResult(intentPicture, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Launching camera app to record video
     */
    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void chooseAudio() {
        Intent audioIntent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(audioIntent, AUDIO);
    }

    private void chooseVideo() {
        Intent videoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(videoIntent, VIDEO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }


        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            Constants.isLivePhotoUpload = true;
            livePhotoPath = getCaptureImageRealPathFromURI(mCapturedImageURI);
        }

        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            Constants.isLiveVideoUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                liveVideoPath = getRealPathFromURI(MyApplication.getInstance(), uri);
                Log.e("AUDIOPATH", "" + liveVideoPath);
            }
        }

        if (requestCode == GALLERY) {
            Constants.isImageUpload = true;
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), contentURI);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    //compress bitmap
                    byte[] byteArray = stream.toByteArray();
                    Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    imagePath = saveImage(compressedBitmap);
                    Log.e("IMAGEPATH", "" + imagePath);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Image uploading Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (requestCode == AUDIO) {
            Constants.isAudioUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                audioPath = getRealPathFromURI(MyApplication.getInstance(), uri);
                Log.e("AUDIOPATH", "" + audioPath);
            }
        }
        if (requestCode == VIDEO) {
            Constants.isVidioUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                videoPath = getRealPathFromURI(MyApplication.getInstance(), uri);
                Log.e("VIDEOPATH", "" + videoPath);
            }
        }
        sendTitleDescToFileUploadDialog();
    }

    private void sendTitleDescToFileUploadDialog() {
        String fileExtension = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_send_title_desc_file_upload, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextTitle = dialogView.findViewById(R.id.edttxt_file_title);
        final EditText editTextDesc = dialogView.findViewById(R.id.edttxt_file_desc);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        Button btnUpload = dialogView.findViewById(R.id.btn_upload);


        if (Constants.isLivePhotoUpload) {
            Constants.isLivePhotoUpload = false;
            file = new File(livePhotoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isLiveVideoUpload) {
            Constants.isLiveVideoUpload = false;
            file = new File(liveVideoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isImageUpload) {
            Constants.isImageUpload = false;
            file = new File(imagePath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isAudioUpload) {
            Constants.isAudioUpload = false;
            file = new File(audioPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isVidioUpload) {
            Constants.isVidioUpload = false;
            file = new File(videoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }


        final String[] title = {file.getName()};
        editTextTitle.setText(title[0]);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        btnCancel.setOnClickListener(v -> alertDialog.dismiss());

        final String finalFileExtension = fileExtension;
        btnUpload.setOnClickListener(v -> {
            alertDialog.dismiss();
            if (!editTextTitle.getText().toString().contains(finalFileExtension)) {
                title[0] = editTextTitle.getText().toString() + finalFileExtension;
            } else {
                title[0] = editTextTitle.getText().toString();
            }

            String desc = "";
            desc = editTextDesc.getText().toString();
            uploadOnServer(title[0], desc);
        });
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (Constants.isAudioUpload) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private String getCaptureImageRealPathFromURI(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = mActivity.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(mActivity,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void uploadOnServer(String title, String desc) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        if (Constants.isLivePhotoUpload) {
            Constants.isLivePhotoUpload = false;
            file = new File(livePhotoPath);
        }
        if (Constants.isLiveVideoUpload) {
            Constants.isLiveVideoUpload = false;
            file = new File(liveVideoPath);
        }
        if (Constants.isImageUpload) {
            Constants.isImageUpload = false;
            file = new File(imagePath);
        }
        if (Constants.isAudioUpload) {
            Constants.isAudioUpload = false;
            file = new File(audioPath);
        }
        if (Constants.isVidioUpload) {
            Constants.isVidioUpload = false;
            file = new File(videoPath);
        }

        Log.e("subscriptionObject", "" + room);
        progressBar.setVisibility(View.GONE);
        pDialog = new ProgressDialog(mActivity, R.style.AlertDialogTheme);
        pDialog.setMessage("Uploading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);

        room.uploadFile(file, title, desc, new FileAdapter() {
                    @Override
                    public void onUploadStarted(String roomId, String fileName, String description) {
                        super.onUploadStarted(roomId, fileName, description);
                        Log.e("onUploadStarted", "" + fileName);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.show();
                            }
                        });
                    }

                    @Override
                    public void onUploadProgress(final int progress, String roomId, String fileName, String description) {
                        super.onUploadProgress(progress, roomId, fileName, description);
                        Log.e("onUploadProgress", "" + fileName);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.setProgress(progress);
                            }
                        });
                    }

                    @Override
                    public void onUploadComplete(int statusCode, FileObject file, String roomId, final String fileName, String description) {
                        super.onUploadComplete(statusCode, file, roomId, fileName, description);
                        Log.e("onUploadComplete", "" + fileName);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onUploadError(final ErrorObject error, IOException e) {
                        super.onUploadError(error, e);
                        if (error != null) {
                            Log.e("onUploadError", "" + error.getMessage());
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    pDialog.dismiss();
                                    Toast.makeText(mActivity, error.getReason(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onSendFile(RocketChatMessage message, ErrorObject error) {
                        super.onSendFile(message, error);
                        Log.e("onSendFile", "" + message.getMessage());
                    }
                }
        );
    }

    @Override
    public void onGetSubscriptions(List<SubscriptionObject> subscriptions, ErrorObject error) {
        if (Constants.client != null) {
            ChatRoomFactory factory = Constants.client.getChatRoomFactory();
            room = factory.createChatRooms(subscriptions).getChatRoomById(roomId);   //This should exist on server
            if (room == null) {
                room = factory.createChatRooms(subscriptions).getChatRoomByName(userName);
            }
            subscribeTyping();
        }
    }

    @Override
    public void onMessage(String roomId, RocketChatMessage message) {
        mActivity.runOnUiThread(() -> {
            if (msgList != null && chatNewAdapter != null && !prevMessageId.equals(message.getMessageId())) {
                prevMessageId = message.getMessageId();
                Message message1 = new Message();
                message1.setRid(roomId);
                message1.setAlias(message.getSenderAlias());
                message1.setId(message.getMessageId());
                message1.setMsg(message.getMessage());
                message1.setTs(String.valueOf(message.getMsgTimestamp()));
                List<Object> o = new ArrayList<>();
                o.add(message.getMentions());
                message1.setMentions(o);
                List<Object> channels = new ArrayList<>();
                channels.add(message.getChannels());
                message1.setChannels(channels);
                message1.setUpdatedAt(String.valueOf(message.getUpdatedAt()));
                message1.setT(message1.getT());
                message1.setGroupable(message.getGroupable());
                message1.setParseUrls(message.getParseUrls());
                message1.setParseUrls(message.getParseUrls());
                message1.setBot("");
                message1.setPinned(null);
                message1.setPinnedAt(null);
                message1.setPinnedBy(null);
                com.comlinkinc.medicus.retrofit.models.pojo.chathistory.File file = new com.comlinkinc.medicus.retrofit.models.pojo.chathistory.File();
                file.setId("");
                file.setName("");
                file.setType("");
                Starred st = new Starred();
                st.setId("");
                U u = new U();
                u.setUsername(message.getSender().getUserName());
                u.setId(message.getSender().getUserId());
                u.setName(message.getSender().getAvatarUrl());
                message1.setU(u);
                msgList.remove(0);
                msgList.add(message1);
                Log.i(TAG, "onMessage: " + msgList.size());
                chatNewAdapter.notifyDataSetChanged();
                lVChat.invalidateViews();
//                    onTouch();
            }
        });

    }

    private void subscribeTyping() {
        room.subscribeRoomTypingEvent((isSubscribed, subId) -> {
            if (isSubscribed) {
                System.out.println("subscribed to typing event successfylly");
            }
        }, this);
    }

    @Override
    public void onTyping(String roomId, final String user, final Boolean istyping) {

        mActivity.runOnUiThread(() -> {
            if (istyping) {
                txtIsTyping.setVisibility(View.VISIBLE);
                txtIsTyping.setText(user + " is typing....");
            } else {
                txtIsTyping.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setUnreadCountToZero(String roomId) {
        setUnreadCountZeroInterface.setUnreadCountZero(Constants.chatAuthToken, Constants.chatUserId, roomId).enqueue(new Callback<SetUnreadCountZeroResponse>() {
            @Override
            public void onResponse(@NonNull Call<SetUnreadCountZeroResponse> call, @NonNull Response<SetUnreadCountZeroResponse> response) {

            }

            @Override
            public void onFailure(@NonNull Call<SetUnreadCountZeroResponse> call, @NonNull Throwable t) {
                Log.d("", "");
            }
        });
    }

    private void searchChatMsg(String searchText) {
        chatSearchInterface.getChatSearchMessages(Constants.chatAuthToken, Constants.chatUserId, roomId, searchText).enqueue(new Callback<ChatHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChatHistoryResponse> call, @NonNull Response<ChatHistoryResponse> response) {
                CommonUtils.hideKeyboard(mActivity);
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessages() != null && response.body().getMessages().size() > 0) {
                            chatNewAdapter.clear();
                            layoutNoChatMsg.setVisibility(View.GONE);
                            List<Message> searchMsgList = new ArrayList<>(response.body().getMessages());
                            Collections.reverse(searchMsgList);
                            chatNewAdapter = new ChatApiAdapter(MyApplication.getInstance(), searchMsgList);
                            lVChat.setAdapter(chatNewAdapter);
                            chatNewAdapter.notifyDataSetChanged();
                            lVChat.bringToFront();
                        } else {
                            chatNewAdapter.clear();
                        }
                        setChatApiAdapter();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChatHistoryResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
