package com.comlinkinc.medicus.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.adapters.SubmittedQuesListAdapter;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.QuestionnairesInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnaries.Datum;
import com.comlinkinc.medicus.retrofit.models.pojo.questionnaries.QuestionnairesResponse;
import com.comlinkinc.medicus.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubmittedQuestionnaireFragment extends Fragment {

    private Context mContext;
    private ListView listviewQues;
    private List<Datum> listQuestionnarie;
    private String pid;

    private LinearLayout lyt_progress;

    public SubmittedQuestionnaireFragment() {
        this.pid = Constants.XUserId;
    }

    public SubmittedQuestionnaireFragment(String pid) {
        this.pid = pid;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_submitted_ques, container, false);
        mContext = getActivity();
        getIds(view);

        return view;
    }

    private void loadingAndDisplayContent(View view) {
        lyt_progress = view.findViewById(R.id.lyt_progress);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);
        listviewQues.setVisibility(View.GONE);


    }

    private void getIds(View view) {
        listviewQues = view.findViewById(R.id.listview_questionnaries);
        listQuestionnarie = new ArrayList<>();
        loadingAndDisplayContent(view);
        fetchQuestionnaireFromAPI();
    }

    private void fetchQuestionnaireFromAPI() {
        QuestionnairesInterface questionnairesInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(QuestionnairesInterface.class);
        questionnairesInterface.getQuestionnaire(Constants.XAuthToken, Constants.XUserId, pid).enqueue(new Callback<QuestionnairesResponse>() {
            @Override
            public void onResponse(@NonNull Call<QuestionnairesResponse> call, @NonNull Response<QuestionnairesResponse> response) {
                if (response.body() != null && response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                    listQuestionnarie.addAll(response.body().getData());
                    SubmittedQuesListAdapter customAdapter = new SubmittedQuesListAdapter(mContext, R.layout.row_appointment_history, listQuestionnarie);
                    listviewQues.setAdapter(customAdapter);
                    lyt_progress.setVisibility(View.GONE);
                    listviewQues.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(mContext, "Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<QuestionnairesResponse> call, @NonNull Throwable t) {
                CommonUtils.failureMessage(mContext, true);
            }
        });
    }
}
