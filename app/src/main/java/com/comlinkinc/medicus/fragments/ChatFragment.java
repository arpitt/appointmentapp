package com.comlinkinc.medicus.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DashboardActivity;
import com.comlinkinc.medicus.utils.MainActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.adapters.ChatApiAdapter;
import com.comlinkinc.medicus.progress.DotProgressBar;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.ChatHistoryInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.ChatSearchInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.SetUnreadCountZeroInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.ChatHistoryResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Message;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.Starred;
import com.comlinkinc.medicus.retrofit.models.pojo.chathistory.U;
import com.comlinkinc.medicus.retrofit.models.pojo.setunreadcountzero.SetUnreadCountZeroResponse;
import com.comlinkinc.medicus.utils.Constants;
import com.google.gson.Gson;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.common.listener.TypingListener;
import com.rocketchat.core.RocketChatAPI;
import com.rocketchat.core.adapter.FileAdapter;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.callback.MessageListener;
import com.rocketchat.core.factory.ChatRoomFactory;
import com.rocketchat.core.model.FileObject;
import com.rocketchat.core.model.RocketChatMessage;
import com.rocketchat.core.model.SubscriptionObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ChatFragment extends Fragment implements View.OnClickListener, MessageListener.SubscriptionListener, GetSubscriptionListener, TypingListener {
    private static final String IMAGE_DIRECTORY = "/medicus";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final String TAG = "ChatFragment";
    private FrameLayout mainLayout;
    private RocketChatAPI.ChatRoom room;
    private String prevMessageId = "";
    private ListView listviewChat;
    private List<Message> msgList = new ArrayList<>();
    private ChatApiAdapter chatNewAdapter;
    private EditText edttxtChat;
    private DotProgressBar progressBar;
    private File file;
    private String livePhotoPath, liveVideoPath, imagePath, audioPath, videoPath;
    private int GALLERY = 1;
    private int AUDIO = 2;
    private int VIDEO = 3;
    private TextView txtIsTyping;
    private ProgressDialog pDialog;
    private String rootUserName = "";
    private Uri mCapturedImageURI;
    private LinearLayout layoutNoChatMsg;
    private EditText edttxtSearchMsg;
    private Activity mActivity;
    private Context mContext;
    private String roomId = "";

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        if (mContext instanceof DashboardActivity) {
            mActivity = (DashboardActivity) mContext;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        mainLayout = view.findViewById(R.id.mainLayout);

        Bundle bundle = getArguments();
        if (bundle != null)
            roomId = Objects.requireNonNull(bundle.get("roomId")).toString();

        getIds(view);
        CommonUtils.hideKeyboard(mActivity);

        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        ((DashboardActivity) mActivity).showMoreOptionsButton(true);
        ((DashboardActivity) mActivity).showSearchMsgButton(true);

        if (Constants.client != null) {
            Constants.client.getSubscriptions(this);
        }

        edttxtChat.setOnEditorActionListener(
                (v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN
                            && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        if (room != null) {
                            if (!edttxtChat.getText().toString().trim().equals("")) {
                                room.sendMessage(edttxtChat.getText().toString());
                                edttxtChat.setText("");
                            } else {
                                Toast.makeText(MyApplication.getInstance(), "Please enter message", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (Constants.client != null) {
                                Constants.client.getSubscriptions((DashboardActivity) mActivity);
                            }
                        }
                        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
                        return true;
                    }
                    return false;
                });

        progressBar.setVisibility(View.VISIBLE);
        fetchChatHistory();

        setUnreadCountToZero(roomId);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getIds(View view) {
        progressBar = view.findViewById(R.id.progress_chat);
        listviewChat = view.findViewById(R.id.listview_chat);
        edttxtChat = view.findViewById(R.id.edttxt_chat);
        txtIsTyping = view.findViewById(R.id.txt_typing);
        ImageView btnSend = view.findViewById(R.id.btn_send);
        ImageView btnAttach = view.findViewById(R.id.btn_attach);
        btnAttach.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        layoutNoChatMsg = view.findViewById(R.id.layout_no_msg);

        edttxtSearchMsg = view.findViewById(R.id.search_message);
        edttxtSearchMsg.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() >= 2) {
                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        progressBar.setVisibility(View.VISIBLE);
                        searchChatMsg(cs.toString());
                    }, 1200);

                } else if (cs.length() == 0) {
                    fetchChatHistory();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        listviewChat.setOnItemLongClickListener((parent, view1, position, id) -> {
            Message message = msgList.get(position);
            if (message.getT() == null) {
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment(room, message);
                bottomSheetFragment.show(((DashboardActivity) mActivity).getSupportFragmentManager(), bottomSheetFragment.getTag());
            }
            return true;
        });
        edttxtChat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    if (room != null) {
                        room.sendIsTyping(true);
                    }
                } else {
                    if (room != null) {
                        room.sendIsTyping(false);
                    }
                }
            }
        });
    }

    private void fetchChatHistory() {
        ChatHistoryInterface chatHistoryInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(ChatHistoryInterface.class);
        chatHistoryInterface.getChatHistoryList(Constants.chatAuthToken, Constants.chatUserId, roomId).enqueue(new Callback<ChatHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChatHistoryResponse> call, @NonNull Response<ChatHistoryResponse> response) {
                progressBar.setVisibility(View.GONE);
                msgList = new ArrayList<>();
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessages() != null && response.body().getMessages().size() > 0) {
                            layoutNoChatMsg.setVisibility(View.GONE);
                            msgList.addAll(response.body().getMessages());
                            progressBar.setVisibility(View.GONE);
                            setChatApiAdapter();
                        } else {
                            layoutNoChatMsg.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(mActivity, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Oops... Something went wrong.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChatHistoryResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Timber.tag("FAILED").d("Failed_To_Load_Chat");
                Log.i("Failed: ", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }

    private void setChatApiAdapter() {
        mActivity.runOnUiThread(() -> {
            if (msgList != null && msgList.size() > 0) {
                Collections.reverse(msgList);
                chatNewAdapter = new ChatApiAdapter(MyApplication.getInstance(), msgList);
                listviewChat.setAdapter(chatNewAdapter);
                chatNewAdapter.notifyDataSetChanged();
                listviewChat.bringToFront();
                listviewChat.invalidateViews();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                if (room != null) {
                    if (!edttxtChat.getText().toString().trim().equals("")) {
                        room.sendMessage(edttxtChat.getText().toString());
                        edttxtChat.setText("");
                    } else {
                        Toast.makeText(MyApplication.getInstance(), "Please enter message", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (Constants.client != null) {
                        Constants.client.getSubscriptions(this);
                    }
                }
                CommonUtils.hideKeyboard(mActivity);
                break;
            case R.id.btn_attach:
                selectfile();
                break;
        }
    }

    private void selectfile() {
        showPictureDialog();
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Take photo", "Shoot video", "Select photo from gallery", "Select audio", "Select video"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            captureImage();
                            break;
                        case 1:
                            captureVideo();
                            break;
                        case 2:
                            choosePhotoFromGallery();
                            //openGallery();
                            break;
                        case 3:
                            chooseAudio();
                            break;
                        case 4:
                            chooseVideo();
                            break;
                    }
                });
        pictureDialog.show();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Image File name");
        mCapturedImageURI = mActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
        startActivityForResult(intentPicture, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Launching camera app to record video
     */
    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void chooseAudio() {
        Intent audioIntent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(audioIntent, AUDIO);
    }

    private void chooseVideo() {
        Intent videoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(videoIntent, VIDEO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == DashboardActivity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            Constants.isLivePhotoUpload = true;
            livePhotoPath = getCaptureImageRealPathFromURI(mCapturedImageURI);
        }

        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            Constants.isLiveVideoUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                liveVideoPath = getRealPathFromURI(MyApplication.getInstance(), uri);
            }
        }

        if (requestCode == GALLERY) {
            Constants.isImageUpload = true;
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), contentURI);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    //compress bitmap
                    byte[] byteArray = stream.toByteArray();
                    Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    imagePath = saveImage(compressedBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(mActivity, "Image uploading Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }
        if (requestCode == AUDIO) {
            Constants.isAudioUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                audioPath = getRealPathFromURI(MyApplication.getInstance(), uri);
                Log.e("AUDIOPATH", "" + audioPath);
            }
        }
        if (requestCode == VIDEO) {
            Constants.isVidioUpload = true;
            if (data != null) {
                Uri uri = data.getData();
                videoPath = getRealPathFromURI(MyApplication.getInstance(), uri);
                Log.e("VIDEOPATH", "" + videoPath);
            }
        }

        sendTitleDescToFileUploadDialog();
    }

    private void sendTitleDescToFileUploadDialog() {
        String fileExtension = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mActivity);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_send_title_desc_file_upload, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextTitle = dialogView.findViewById(R.id.edttxt_file_title);
        final EditText editTextDesc = dialogView.findViewById(R.id.edttxt_file_desc);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        Button btnUpload = dialogView.findViewById(R.id.btn_upload);


        if (Constants.isLivePhotoUpload) {
            Constants.isLivePhotoUpload = false;
            file = new File(livePhotoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isLiveVideoUpload) {
            Constants.isLiveVideoUpload = false;
            file = new File(liveVideoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isImageUpload) {
            Constants.isImageUpload = false;
            file = new File(imagePath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isAudioUpload) {
            Constants.isAudioUpload = false;
            file = new File(audioPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }
        if (Constants.isVidioUpload) {
            Constants.isVidioUpload = false;
            file = new File(videoPath);
            fileExtension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
        }


        final String[] title = {file.getName()};
        editTextTitle.setText(title[0]);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        btnCancel.setOnClickListener(v -> alertDialog.dismiss());

        final String finalFileExtension = fileExtension;
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (!editTextTitle.getText().toString().contains(finalFileExtension)) {
                    title[0] = editTextTitle.getText().toString() + finalFileExtension;
                } else {
                    title[0] = editTextTitle.getText().toString();
                }

                String desc = "";
                desc = editTextDesc.getText().toString();
                uploadOnServer(title[0], desc);
            }
        });
    }
    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            if (Constants.isAudioUpload) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private String getCaptureImageRealPathFromURI(Uri contentUri) {
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = mActivity.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            return contentUri.getPath();
        }
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(mActivity,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void uploadOnServer(String title, String desc) {

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        if (Constants.isLivePhotoUpload) {
            Constants.isLivePhotoUpload = false;
            file = new File(livePhotoPath);
        }
        if (Constants.isLiveVideoUpload) {
            Constants.isLiveVideoUpload = false;
            file = new File(liveVideoPath);
        }
        if (Constants.isImageUpload) {
            Constants.isImageUpload = false;
            file = new File(imagePath);
        }
        if (Constants.isAudioUpload) {
            Constants.isAudioUpload = false;
            file = new File(audioPath);
        }
        if (Constants.isVidioUpload) {
            Constants.isVidioUpload = false;
            file = new File(videoPath);
        }

        progressBar.setVisibility(View.GONE);
        pDialog = new ProgressDialog(mActivity, R.style.AlertDialogTheme);
        pDialog.setMessage("Uploading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);

        room.uploadFile(file, title, desc, new FileAdapter() {
                    @Override
                    public void onUploadStarted(String roomId, String fileName, String description) {
                        super.onUploadStarted(roomId, fileName, description);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.show();
                            }
                        });
                    }

                    @Override
                    public void onUploadProgress(final int progress, String roomId, String fileName, String description) {
                        super.onUploadProgress(progress, roomId, fileName, description);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.setProgress(progress);
                            }
                        });
                    }

                    @Override
                    public void onUploadComplete(int statusCode, FileObject file, String roomId, final String fileName, String description) {
                        super.onUploadComplete(statusCode, file, roomId, fileName, description);
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                pDialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onUploadError(final ErrorObject error, IOException e) {
                        super.onUploadError(error, e);
                        if (error != null) {
                            Log.e("onUploadError", "" + error.getMessage());
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    pDialog.dismiss();
                                    Toast.makeText(mActivity, error.getReason(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onSendFile(RocketChatMessage message, ErrorObject error) {
                        super.onSendFile(message, error);
                        Log.e("onSendFile", "" + message.getMessage());
                    }
                }
        );
    }

    @Override
    public void onGetSubscriptions(List<SubscriptionObject> subscriptions, ErrorObject error) {
        if (Constants.client != null) {
            ChatRoomFactory factory = Constants.client.getChatRoomFactory();
            room = factory.createChatRooms(subscriptions).getChatRoomById(roomId);   //This should exist on server
            if (room == null) {
                room = factory.createChatRooms(subscriptions).getChatRoomByName(Constants.userDetails.getUserName().replace("@", "_"));
            }
            subscribeTyping();
        }
    }

    @Override
    public void onMessage(String roomId, RocketChatMessage message) {
        mActivity.runOnUiThread(() -> {
            if (msgList != null && chatNewAdapter != null && !prevMessageId.equals(message.getMessageId())) {
                prevMessageId = message.getMessageId();
                Log.i(TAG, "onMessage: " + new Gson().toJson(message));
                Message message1 = new Message();
                message1.setRid(roomId);
                message1.setAlias(message.getSenderAlias());
                message1.setId(message.getMessageId());
                message1.setMsg(message.getMessage());
                message1.setTs(String.valueOf(message.getMsgTimestamp()));
                List<Object> o = new ArrayList<>();
                o.add(message.getMentions());
                message1.setMentions(o);
                List<Object> channels = new ArrayList<>();
                channels.add(message.getChannels());
                message1.setChannels(channels);
                message1.setUpdatedAt(String.valueOf(message.getUpdatedAt()));
                if(message.getRawJsonObject().has("t")) {
                    try {
                        message1.setT(message.getRawJsonObject().getString("t"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        message1.setT(null);
                    }
                } else {
                    message1.setT(null);
                }
                message1.setGroupable(message.getGroupable());
                message1.setParseUrls(message.getParseUrls());
                message1.setParseUrls(message.getParseUrls());
                message1.setBot("");
                message1.setPinned(null);
                message1.setPinnedAt(null);
                message1.setPinnedBy(null);
                com.comlinkinc.medicus.retrofit.models.pojo.chathistory.File file = new com.comlinkinc.medicus.retrofit.models.pojo.chathistory.File();
                file.setId("");
                file.setName("");
                file.setType("");
                Starred st = new Starred();
                st.setId("");
                U u = new U();
                u.setUsername(message.getSender().getUserName());
                u.setId(message.getSender().getUserId());
                u.setName(message.getSender().getAvatarUrl());
                message1.setU(u);
//                    msgList.clear();
                msgList.remove(0);
                msgList.add(message1);
                chatNewAdapter.notifyDataSetChanged();
                listviewChat.invalidateViews();
//                    onTouch();
            }
        });

    }

    private void subscribeTyping() {
        room.subscribeRoomMessageEvent((isSubscribed, subId) -> {
            Log.i(TAG, "subscribeMessage: Subscribed");
        }, this);
        room.subscribeRoomTypingEvent((isSubscribed, subId) -> {
            if (isSubscribed) {
                System.out.println("Subscribed to typing event successfully");
            }
        }, this);
    }

    @Override
    public void onTyping(String roomId, final String user, final Boolean istyping) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (istyping) {
                    txtIsTyping.setVisibility(View.VISIBLE);
                    txtIsTyping.setText(user + " is typing....");
                } else {
                    txtIsTyping.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void setUnreadCountToZero(String roomId) {
        SetUnreadCountZeroInterface setUnreadCountZeroInterface = RetrofitClient.getClient(Constants.ROCKET_CHAT_URL + "api/v1/").create(SetUnreadCountZeroInterface.class);
        setUnreadCountZeroInterface.setUnreadCountZero(Constants.chatAuthToken, Constants.chatUserId, roomId).enqueue(new Callback<SetUnreadCountZeroResponse>() {
            @Override
            public void onResponse(@NonNull Call<SetUnreadCountZeroResponse> call, @NonNull Response<SetUnreadCountZeroResponse> response) {
                Log.i(TAG, "onResponse: " + response.code());
            }

            @Override
            public void onFailure(@NonNull Call<SetUnreadCountZeroResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void searchChatMsg(String searchText) {
        ChatSearchInterface chatSearchInterface = RetrofitClient.getClient(Constants.BASE_URL).create(ChatSearchInterface.class);
        chatSearchInterface.getChatSearchMessages(Constants.XAuthToken, Constants.chatUserId, roomId, searchText).enqueue(new Callback<ChatHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<ChatHistoryResponse> call, @NonNull Response<ChatHistoryResponse> response) {
                CommonUtils.hideKeyboard(mActivity);
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getMessages() != null && response.body().getMessages().size() > 0) {
                            chatNewAdapter.clear();
                            layoutNoChatMsg.setVisibility(View.GONE);
                            List<Message> searchmsgList = new ArrayList<>(response.body().getMessages());

                            Collections.reverse(searchmsgList);
                            chatNewAdapter = new ChatApiAdapter(mActivity, searchmsgList);
                            listviewChat.setAdapter(chatNewAdapter);
                            chatNewAdapter.notifyDataSetChanged();
                            listviewChat.bringToFront();
                        } else {
                            chatNewAdapter.clear();
                        }
                        setChatApiAdapter();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChatHistoryResponse> call, @NonNull Throwable t) {
                Timber.e(t);
            }
        });
    }

    public void hideShowSearchView(boolean status) {
        if (edttxtSearchMsg != null) {
            if (status) {
                edttxtSearchMsg.setVisibility(View.VISIBLE);
            } else {
                edttxtSearchMsg.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
