package com.comlinkinc.medicus.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DashboardActivity;
import com.comlinkinc.medicus.adapters.LabTestListAdapter;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.interfaces.PatientInterface;
import com.comlinkinc.medicus.responses.labTests.LabTestListResponse;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientTestsFragment extends Fragment {

    private final static int LOADING_DURATION = 1000;
    private String pid;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rVLabTestList;
    private ProgressBar pBLoading;
    private TextView tVNoTests;
    private Context mContext;
    private Activity mActivity;
    private LabTestListAdapter adapter = null;
    private Handler handler = new Handler();
    private boolean isInternetConnected = true;

    public PatientTestsFragment() {
        pid = Constants.XUserId;
    }


    public PatientTestsFragment(String pid) {
        this.pid = pid;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        if(mContext instanceof DashboardActivity) {
            mActivity = (DashboardActivity) mContext;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_patient_tests, container, false);
        getIds(v);
        getLabTests();
        handler.postDelayed(new Runnable(){
            public void run(){
                getLabTests();
                handler.postDelayed(this, LOADING_DURATION);
            }
        }, LOADING_DURATION);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryLight), getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(this::getLabTests);

        return v;
    }

    private void getIds(View v) {
        swipeRefreshLayout = v.findViewById(R.id.swipeRefreshLayout);
        rVLabTestList = v.findViewById(R.id.rVLabTestList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rVLabTestList.setLayoutManager(layoutManager);
        pBLoading = v.findViewById(R.id.pBLoading);
        tVNoTests = v.findViewById(R.id.tVNoTests);
    }

    private void getLabTests(){
        if (adapter == null) pBLoading.setVisibility(View.VISIBLE);
        PatientInterface patientInterface = RetrofitClient.getClient(Constants.MAIN_URL).create(PatientInterface.class);
        patientInterface.getLabTests(Constants.XAuthToken, Constants.XUserId, pid).enqueue(new Callback<LabTestListResponse>() {
            @Override
            public void onResponse(@NonNull Call<LabTestListResponse> call, @NonNull Response<LabTestListResponse> response) {
                isInternetConnected = true;
                pBLoading.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase(Constants.TEXT_FOR_200)) {
                        tVNoTests.setVisibility(View.GONE);
                        adapter = new LabTestListAdapter(response.body().getLabTestList());
                        if (response.body().getLabTestList().size() > 0) {
                            rVLabTestList.setAdapter(adapter);
                        } else {
                            tVNoTests.setVisibility(View.VISIBLE);
                        }
                    } else {
                        adapter = null;
                        tVNoTests.setVisibility(View.VISIBLE);
                    }
                } else {
                    adapter = null;
                    Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LabTestListResponse> call, @NonNull Throwable t) {
                adapter = null;
                swipeRefreshLayout.setRefreshing(false);
                pBLoading.setVisibility(View.GONE);
                isInternetConnected = CommonUtils.failureMessage(mContext, isInternetConnected);
            }
        });
    }
}
