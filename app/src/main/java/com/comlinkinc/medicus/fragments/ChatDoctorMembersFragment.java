package com.comlinkinc.medicus.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.comlinkinc.medicus.R;
import com.comlinkinc.medicus.activities.DashboardActivity;
import com.comlinkinc.medicus.activities.DoctorDashboardActivity;
import com.comlinkinc.medicus.MyApplication;
import com.comlinkinc.medicus.adapters.ChatMembersAdapter;
import com.comlinkinc.medicus.progress.DotProgressBar;
import com.comlinkinc.medicus.retrofit.RetrofitClient;
import com.comlinkinc.medicus.retrofit.models.interfaces.SearchUserInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.UserInformationInterface;
import com.comlinkinc.medicus.retrofit.models.interfaces.UsersListSearchViewInterface;
import com.comlinkinc.medicus.retrofit.models.pojo.userinforesponse.UserInfoResponse;
import com.comlinkinc.medicus.retrofit.models.pojo.users.User;
import com.comlinkinc.medicus.retrofit.models.pojo.users.UsersListResponse;
import com.comlinkinc.medicus.utils.CommonUtils;
import com.comlinkinc.medicus.utils.Connectivity;
import com.comlinkinc.medicus.utils.Constants;
import com.comlinkinc.medicus.utils.OnBackPressed;
import com.comlinkinc.medicus.utils.Prefs;
import com.rocketchat.common.data.model.ErrorObject;
import com.rocketchat.core.callback.GetSubscriptionListener;
import com.rocketchat.core.model.SubscriptionObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatDoctorMembersFragment extends Fragment implements GetSubscriptionListener, SwipeRefreshLayout.OnRefreshListener,OnBackPressed {
    private List<User> userArrayList = new ArrayList<>();
    private List<User> refreshedUserArrayList;
    private ListView lvMembers;
    private SharedPreferences sharedpreferences;
    private ChatMembersAdapter chatMembersAdapter;
    private DotProgressBar dotProgressBar;
    private AutoCompleteTextView searchViewAutoComplete;
    private List<SubscriptionObject> subscribedMembers;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Integer> colorList;
    private Activity mActivity;

    public ChatDoctorMembersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mActivity = (Activity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_members, container, false);
        getIds(view);
        CommonUtils.hideKeyboard(mActivity);

        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        //((DoctorDashboardActivity) mActivity).showHideMoreOptionsButton(false);
        //((DoctorDashboardActivity) mActivity).showHideSearchMsgButton(false);
        ((DoctorDashboardActivity) mActivity).setToolbarBarTitle("Members");
        //((DoctorDashboardActivity) mActivity).setToolbarIcon();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.PREFS_READ_ONLY_GROUP, "false");
        editor.apply();


        swipeRefreshLayout.post(() -> {
                    swipeRefreshLayout.setRefreshing(true);
                    callMethod();
                }
        );

        Constants.isFromGroupsListFragment = false;

        if (Connectivity.isNetworkConnected(MyApplication.getInstance())) {
            if (Constants.client != null) {
                Constants.client.getSubscriptions(this);
            }

            JSONObject jsonObject = new JSONObject();
            JSONObject usernamejsonObject = new JSONObject();
            try {
                usernamejsonObject.put("$regex", searchViewAutoComplete.getText());
                jsonObject.put("username", usernamejsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String jsonString = jsonObject.toString();

            SearchUserInterface searchUserInterface = RetrofitClient.getClient(Constants.BASE_URL).create(SearchUserInterface.class);
            searchUserInterface.getUsersList(sharedpreferences.getString(Constants.PREFS_AUTH_TOKEN, ""), sharedpreferences.getString(Constants.PREFS_USER_ID, ""), jsonString).enqueue(new Callback<UsersListResponse>() {
                @Override
                public void onResponse(@NonNull Call<UsersListResponse> call, @NonNull Response<UsersListResponse> response) {
                    if (response.body() != null) {
                        userArrayList = response.body().getUsers();
                        setAutoCompleteAdapter();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UsersListResponse> call, @NonNull Throwable t) {
                    Log.e("ChatMembers", "SearchUser: " + t.getMessage());
                }
            });

            searchViewAutoComplete.setOnItemClickListener((parent, view1, position, id) -> {
                Gson userGson = new Gson();
                refreshedUserArrayList = new ArrayList<>();
                for (int i = 0; i < userArrayList.size(); i++) {
                    if (userArrayList.get(i).getUsername() != null) {
                        if (userArrayList.get(i).getUsername().equalsIgnoreCase(searchViewAutoComplete.getText().toString())) {
                            User newUser = userArrayList.get(i);
                            User user = userGson.fromJson(userGson.toJson(newUser), User.class);
                            if (user.getCustomFields() != null) {
                                String devicetoken = user.getCustomFields().getDevicetoken();
                                SharedPreferences.Editor editor1 = sharedpreferences.edit();
                                editor1.putString(Constants.PREFS_PEER_USER, devicetoken);
                                editor1.apply();
                            }
                            refreshedUserArrayList.add(user);
                        }
                    }
                }

                openChatScreen();

                searchViewAutoComplete.setText("");
            });

            searchViewAutoComplete.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }
                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() >= 3) {
                        JSONObject jsonObject = new JSONObject();
                        JSONObject usernamejsonObject = new JSONObject();
                        try {
                            usernamejsonObject.put("$regex", searchViewAutoComplete.getText());
                            jsonObject.put("username", usernamejsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String jsonString = jsonObject.toString();
                        UsersListSearchViewInterface usersListSearchViewInterface = RetrofitClient.getClient(Constants.BASE_URL).create(UsersListSearchViewInterface.class);
                        usersListSearchViewInterface.getUsersList(sharedpreferences.getString(Constants.PREFS_AUTH_TOKEN, ""),
                                sharedpreferences.getString(Constants.PREFS_USER_ID, ""), jsonString, 3000).enqueue(new Callback<UsersListResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<UsersListResponse> call, @NonNull Response<UsersListResponse> response) {

                                if (response.body() != null && response.body().getSuccess() != null && response.body().getTotal() != 0) {
                                    if (response.body().getUsers() != null && response.body().getUsers().size() > 0) {
                                        searchViewAutoComplete.setError(null);
                                        userArrayList = response.body().getUsers();

                                        Constants.isFromSearchView = true;
                                        String[] scripts = new String[userArrayList.size()];

                                        for (int i = 0; i < userArrayList.size(); i++) {
                                            if (userArrayList.get(i).getUsername() != null) {
                                                scripts[i] = userArrayList.get(i).getUsername();
                                            } else {
                                                scripts[i] = userArrayList.get(i).getName();
                                            }
                                        }

                                        ArrayAdapter<String> adapters = new ArrayAdapter<>(MyApplication.getInstance(), R.layout.autocomplete_item, scripts);
                                        searchViewAutoComplete.setThreshold(1);
                                        searchViewAutoComplete.setAdapter(adapters);
                                        adapters.notifyDataSetChanged();
                                    } else {
                                        searchViewAutoComplete.setError("User not found");
                                    }
                                } else {
                                    searchViewAutoComplete.setError("User not found");
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<UsersListResponse> call, @NonNull Throwable t) {
                                Log.d("API_FAIL", "" + t.getMessage());
                            }
                        });
                    }
                }
            });
        } else {
            CommonUtils.canReachServers(mActivity);
        }

        return view;
    }

    private void getIds(@NotNull View view) {
        refreshedUserArrayList = new ArrayList<>();
        sharedpreferences = mActivity.getSharedPreferences(Constants.COMLINK_PREFS, Context.MODE_PRIVATE);

        dotProgressBar = view.findViewById(R.id.dot_progress_bar);
        dotProgressBar.setVisibility(View.VISIBLE);
        searchViewAutoComplete = view.findViewById(R.id.search_autocomplete);
        lvMembers = view.findViewById(R.id.listview_members);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    private void openChatScreen() {
        Constants.isFromGroupChat = false;
        Constants.isFromPeerChat = true;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Constants.PREFS_ROOM_ID, refreshedUserArrayList.get(0).getId());
        editor.putString(Constants.PREFS_USER_NAME, refreshedUserArrayList.get(0).getUsername());
        editor.apply();

        if (refreshedUserArrayList.get(0).getUsername().equalsIgnoreCase(sharedpreferences.getString(Constants.PREFS_LOGIN_USER_NAME, ""))) {
            Toast.makeText(MyApplication.getInstance(), "You can't do this action", Toast.LENGTH_SHORT).show();
        } else {
            ((DoctorDashboardActivity) mActivity).setToolbarBarTitle(refreshedUserArrayList.get(0).getUsername());
            Class fragmentClass;
            fragmentClass = ChatDoctorFragment.class;
            try {
                Fragment fragment = (Fragment) fragmentClass.newInstance();
                Constants.isFromChatMembersFragment = true;
                FragmentManager fragmentManager = ((DashboardActivity) mActivity).getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, "ChatDoctorFragment").commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getUserInfoByName(String username) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        UserInformationInterface userInformationInterface = RetrofitClient.getClient(Constants.BASE_URL).create(UserInformationInterface.class);
        userInformationInterface.getUserInfoResponseCall(sharedpreferences.getString(Constants.PREFS_AUTH_TOKEN, ""),
                sharedpreferences.getString(Constants.PREFS_USER_ID, ""), username).enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserInfoResponse> call, @NonNull Response<UserInfoResponse> response) {
                if (response.body() != null) {
                    if (response.body().getUser() != null && response.body().getUser().getCustomFields() != null) {
                        String devicetoken = response.body().getUser().getCustomFields().getDevicetoken();
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Constants.PREFS_PEER_USER, devicetoken);
                        editor.apply();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserInfoResponse> call, @NonNull Throwable t) {
                Log.d("FAIL_USER_INFO", t.toString());
            }
        });
    }

    private void listViewOnClick() {
        lvMembers.setOnItemClickListener((parent, view, position, id) -> {
            if (Connectivity.isNetworkConnected(MyApplication.getInstance())) {
                if (Constants.isFromSearchView) {
                    ((DoctorDashboardActivity) mActivity).setToolbarBarTitle(subscribedMembers.get(position).getRoomName());

                    Constants.isFromGroupChat = false;
                    Constants.isFromPeerChat = true;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Constants.PREFS_ROOM_ID, subscribedMembers.get(position).getRoomId());
                    String userName = subscribedMembers.get(position).getRoomName();
                    editor.putString(Constants.PREFS_USER_NAME, subscribedMembers.get(position).getRoomName());
                    editor.putString(Constants.PREFS_USER_COLOR, colorList.get(position) + "");
                    getUserInfoByName(userName);
                    editor.apply();

                    // Call Chat Screen
                    Fragment fragment;
                    Class fragmentClass;
                    fragmentClass = ChatDoctorFragment.class;
                    try {
                        fragment = (Fragment) fragmentClass.newInstance();
                        Constants.isFromChatMembersFragment = true;
                        FragmentManager fragmentManager = ((DoctorDashboardActivity) mActivity).getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, "ChatDoctorFragment").commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ((DoctorDashboardActivity) mActivity).setToolbarBarTitle(subscribedMembers.get(position).getRoomName());
                    Constants.isFromGroupChat = false;
                    Constants.isFromPeerChat = true;
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Constants.PREFS_ROOM_ID, subscribedMembers.get(position).getRoomId());
                    String userName = subscribedMembers.get(position).getRoomName();
                    editor.putString(Constants.PREFS_USER_NAME, subscribedMembers.get(position).getRoomName());
                    editor.putString(Constants.PREFS_USER_COLOR, colorList.get(position) + "");
                    getUserInfoByName(userName);
                    editor.apply();

                    Class fragmentClass;
                    fragmentClass = ChatDoctorFragment.class;
                    try {
                        Fragment fragment = (Fragment) fragmentClass.newInstance();
                        Constants.isFromChatMembersFragment = true;
                        FragmentManager fragmentManager = ((DoctorDashboardActivity) mActivity).getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, "ChatDoctorFragment").commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                CommonUtils.canReachServers(mActivity);
            }
        });
    }

    private void setAutoCompleteAdapter() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        Constants.isFromSearchView = true;
        String[] scripts = new String[500];

        for (int i = 0; i < userArrayList.size(); i++) {
            if (userArrayList.get(i).getUsername() != null)
                scripts[i] = userArrayList.get(i).getUsername();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>
                (MyApplication.getInstance(), android.R.layout.select_dialog_item, scripts);

        searchViewAutoComplete.setThreshold(1);
        searchViewAutoComplete.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onGetSubscriptions(final List<SubscriptionObject> subscriptions, ErrorObject error) {
        Log.d("ChatMembersFragment", "Getting subscriptions");
        mActivity.runOnUiThread(() -> {
            subscribedMembers = new ArrayList<>();
            for (int i = 0; i < subscriptions.size(); i++) {
                if (("" + subscriptions.get(i).getRoomType()).equalsIgnoreCase("ONE_TO_ONE")) {
                    subscribedMembers.add(subscriptions.get(i));
                }
            }

            Constants.isFromSearchView = true;
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (subscribedMembers != null) {
                colorList = new ArrayList<>();
                Random rnd = new Random();
                for (int i = 0; i < subscribedMembers.size(); i++) {
                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    colorList.add(color);
                }

                Collections.reverse(subscribedMembers);

                Collections.sort(subscribedMembers, (o1, o2) -> o2.getUpdatedAt().compareTo(o1.getUpdatedAt()));
                Log.d("ChatMembersFragment", new Gson().toJson(subscribedMembers));
                chatMembersAdapter = new ChatMembersAdapter(mActivity, subscribedMembers, new Random());
                lvMembers.setAdapter(chatMembersAdapter);
                listViewOnClick();
                chatMembersAdapter.notifyDataSetChanged();
                dotProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void callMethod() {
        swipeRefreshLayout.setRefreshing(true);
        if (Constants.client != null) {
            Constants.client.getSubscriptions(this);
        }
    }

    @Override
    public void onRefresh() {
        callMethod();
    }


    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
